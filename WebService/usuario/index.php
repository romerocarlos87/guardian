<?php
header('Access-Control-Allow-Origin: *');
header('content-type: application/json;charset=utf-8');
//var_dump($_SERVER['REQUEST_METHOD']);
//var_dump($_POST);
//die;
switch ($_REQUEST['OPT']) {
	case 'auth':
	$service= new ServiceUser();
	$service->autenticar($_REQUEST['username'],$_REQUEST['password']);
	break;
	case 'nuevaUbicacion':
	$service= new ServiceUser();
	$service->nuevaUbicacion($_POST['data']['USUARIO'],$_POST['data']['LONGITUD'],$_POST['data']['LATITUD'],$_POST['data']['FECHA']);
	break;
	case 'nuevaEncuesta':
	$service=new ServiceUser();
	$service->nuevaEncuesta($_POST['data']);
	break;
	case 'getUltimasUbicaciones':
	$service=new ServiceUser();
	$service->getUltimasUbicaciones($_POST);
	break;
	case 'actualizarContrasena':
	$service=new ServiceUser();
	$service->actualizarContrasena($_POST['data']);
	break;
	case 'getUltimasUbicacionesGrupo':
	$service=new ServiceUser();
	$service->getUltimasUbicacionesGrupo($_POST);
	break;
	case 'obtenerBarrios':
	$service=new ServiceUser();
	$service->obtenerBarrios();
	break;
	case 'obtenerUsuarios':
	$service=new ServiceUser();
	$service->obtenerUsuarios($_POST['grupo']);
	break;
	case 'obtenerGrupos':
	$service=new ServiceUser();
	$service->obtenerGrupos();
	break;
	case 'getUbicacionesEncuesta':
	$service=new ServiceUser();
	$service->getUbicacionesEncuesta($_POST);
	break;
	case 'lastVersion':
	echo json_encode(['version'=>1.7]);
	break;
	default:
	echo json_encode(['estado'=>false,'mensaje'=>'se presentó un error al realizar la solicitud']);
	break;
}


/**
 * Clase servicios de usuarios
 */
class ServiceUser {
	
	function autenticar($username,$password){
		require("../../page/conectar.php");
		$conexion->query("select * from seguridad where seguridadusuario='".$username."' and seguridadclave='".$password."' and estadoregistro=1");	
		$rs_sql = $conexion->resultset();
		
		if($conexion->rowCount() > 0){
			foreach ($rs_sql as $value) {
				echo json_encode(['userName'=>$username,'userId'=>$value['SEGURIDADID'],'estado'=>true,'validado'=>$value['VALIDADO']]);
			}
		}else{
			echo json_encode(["estado"=>false,"mensaje"=>"Usuario o contraseña invalida"]);
		}
		unset($conexion);
	}

	function nuevaUbicacion($username,$longitud,$latitud,$fecha){

		require("../../page/conectar.php");
		$conexion->query("INSERT INTO `ubicaciones`(`USUARIO`, `LONGITUD`, `LATITUD`, `FECHA`) VALUES ('".$username."',".$longitud.",".$latitud.",".$fecha.");");	
		$conexion->execute();
		if($conexion->lastInsertId()){
			http_response_code(201);
			echo json_encode(['ubicaionId'=>$conexion->lastInsertId(),'estado'=>true]);
		}else{
			echo json_encode(['mensaje'=>'Error al intentar registar la ubicacion','estado'=>false]);
			http_response_code(400);
		}
	}

	function obtenerUsuarios($grupo){
		require("../../page/conectar.php");
		$where=" and GRUPOID={$grupo}";
		if($grupo=='all'){
			$where="";
		}
		$sql="SELECT SEGURIDADID,e.EMPLEADONOMBRE FROM `seguridad` s INNER JOIN empleado e on s.EMPLEADOID=e.EMPLEADOID where  e.registroestado=1 and s.estadoregistro=1 {$where} order by (e.EMPLEADONOMBRE)";
		//echo $sql;
		$conexion->query($sql);	
		$rs_sql = $conexion->resultset();
		if($conexion->rowCount() > 0){
			echo json_encode(['data'=>$rs_sql,'estado'=>true]);
		}
	}

	function obtenerGrupos(){
		require("../../page/conectar.php");
		$sql="SELECT * FROM `grupo` where REGISTROESTADO=1";
		$conexion->query($sql);	
		$rs_sql = $conexion->resultset();
		if($conexion->rowCount() > 0){
			echo json_encode(['data'=>$rs_sql,'estado'=>true]);
		}
	}
	

	function nuevaEncuesta($data){
		
		require("../../page/conectar.php");



		$where="SELECT * FROM persona WHERE `FECHAENCUESTA`='".$data['FECHAENCUESTA']."' ";
		

		if(isset($data['PERSONANUMERODOCUMENTO']) && $data['PERSONANUMERODOCUMENTO']!=''){
			$where .= " and `PERSONANUMERODOCUMENTO`='".$data['PERSONANUMERODOCUMENTO']."'";
		}

		if(isset($data['PERSONAPRIMERNOMBRE']) && $data['PERSONAPRIMERNOMBRE']!=''){
			$where .= " and `PERSONAPRIMERNOMBRE`='".$data['PERSONAPRIMERNOMBRE']."'  ";
		}

		if(isset($data['PERSONASEGUNDONOMBRE']) && $data['PERSONASEGUNDONOMBRE']!=''){
			$where .= " and `PERSONASEGUNDONOMBRE`='".$data['PERSONASEGUNDONOMBRE']."'  ";
		}

		if(isset($data['PERSONAPRIMERAPELLIDO']) && $data['PERSONAPRIMERAPELLIDO']!=''){
			$where .= " and `PERSONAPRIMERAPELLIDO`='".$data['PERSONAPRIMERAPELLIDO']."'  ";
		}

		if(isset($data['PERSONASEGUNDOAPELLIDO']) && $data['PERSONASEGUNDOAPELLIDO']!=''){
			$where .= " and `PERSONASEGUNDOAPELLIDO`='".$data['PERSONASEGUNDOAPELLIDO']."'  ";
		}

		if(isset($data['GENEROID']) && $data['GENEROID']!=''){
			$where .= " and `GENEROID`='".$data['GENEROID']."'  ";
		}

		if(isset($data['PERSONADIRECCION']) && $data['PERSONADIRECCION']!=''){
			$where .= " and `PERSONADIRECCION`='".$data['PERSONADIRECCION']."'  ";
		}

		if(isset($data['ENCUESTADORID']) && $data['ENCUESTADORID']!=''){
			$where .= " and `ENCUESTADORID`='".$data['ENCUESTADORID']."'  ";
		}

		if(isset($data['MOMENTOCREACION']) && $data['MOMENTOCREACION']!=''){
			$where .= " and `MOMENTOCREACION`='".$data['MOMENTOCREACION']."'  ";
		}

		if(isset($data['LONGITUD']) && $data['LONGITUD']!=''){
			$where .= " and `LONGITUD`='".$data['LONGITUD']."'  ";
		}

		if(isset($data['LATITUD']) && $data['LATITUD']!=''){
			$where .= " and `LATITUD`='".$data['LATITUD']."'  ";
		}
		if(isset($data['ENCUESTADOR']) && $data['ENCUESTADOR']!=''){
			$where .= " and `ENCUESTADOR`='".$data['ENCUESTADOR']."'";
		}

		$conexion->query($where);	
		$rs_sql = $conexion->resultset();
		if($conexion->rowCount() > 0){
			echo json_encode(['encuestaId'=>0,'estado'=>true]);
			die;
		}

		$checkPerosna="SELECT * FROM persona WHERE PERSONANUMERODOCUMENTO='".$data['PERSONANUMERODOCUMENTO']."'";
		$conexion->query($checkPerosna);	
		$rs_sql = $conexion->resultset();
		
		$tablaRegistro="persona";

		if($conexion->rowCount() > 0 || $data['PERSONANUMERODOCUMENTO']==''){
			$tablaRegistro="persona_log";
			echo json_encode(['encuestaId'=>0,'estado'=>true]);
			die;
		}


		$fechaInicio=0;
		if(isset($data['INICIOENCUESTA'])){
			$fechaInicio=$data['INICIOENCUESTA'];	
		}

		$sql="INSERT INTO `".$tablaRegistro."` (`PERSONANUMERODOCUMENTO`,`PERSONAPRIMERNOMBRE`,`PERSONASEGUNDONOMBRE`,`PERSONAPRIMERAPELLIDO`, `PERSONASEGUNDOAPELLIDO`, `GENEROID`, `PERSONAEDAD`, `PERSONADIRECCION`, `PERSONATIPOUBICACION`, `PERSONABARRIO`, `PERSONATELEFONO`,`PERSONACELULAR`,`PERSONACORREO`,`FECHAENCUESTA`,`ENCUESTADORID`,`QUENECESITA`,`DESEAOBRA`,`VOTARIA`, `PORQUEVOTARIA`,`PARTICIPARIA`, `PORQUEPARTICIPARIA`,`OTROPARTICIPARIA`, `ORIGENREGISTROID`, `USUARIOCREACION`,`MOMENTOCREACION`, `LONGITUD`, `LATITUD`,`ENCUESTADOR`,`CIUDAD`,`ENCUESTAINICIO`,`CONOCEUSTED`) VALUES ('"
		.$data['PERSONANUMERODOCUMENTO']."','"
		.$data['PERSONAPRIMERNOMBRE']."','"
		.$data['PERSONASEGUNDONOMBRE']."','"
		.$data['PERSONAPRIMERAPELLIDO']."','"
		.$data['PERSONASEGUNDOAPELLIDO']."',"
		.$data['GENEROID'].","
		.$data['PERSONAEDAD'].",'"
		.$data['PERSONADIRECCION']."','"
		.$data['PERSONATIPOUBICACION']."','"
		.$data['PERSONABARRIO']."','"
		.$data['PERSONATELEFONO']."','"
		.$data['PERSONACELULAR']."','"
		.$data['PERSONACORREO']."',"
		.$data['FECHAENCUESTA'].","
		.$data['ENCUESTADORID'].",'"
		.$data['QUENECESITA']."','"
		.$data['DESEAOBRA']."','"
		.$data['VOTARIA']."','"
		.$data['PORQUEVOTARIA']."','"
		.$data['PARTICIPARIA']."','"
		.$data['PORQUEPARTICIPARIA']."','"
		.$data['OTROPARTICIPARIA']."',"
		.$data['ORIGENREGISTROID'].",'"
		.$data['USUARIOCREACION']."',"
		.$data['MOMENTOCREACION'].","
		.$data['LONGITUD'].","
		.$data['LATITUD'].",'"
		.$data['ENCUESTADOR']."','"
		.$data['CIUDAD']."',"
		.$fechaInicio.",'"
		.$data['CONOCEUSTED']."')";

		$conexion->query($sql);
		
		$conexion->execute();
		if($conexion->lastInsertId()){
			http_response_code(201);
			echo json_encode(['encuestaId'=>$conexion->lastInsertId(),'estado'=>true]);
		}else{
			http_response_code(400);
			echo json_encode(['mensaje'=>'error al intentar registar la encuesta','estado'=>false]);
		}
		unset($conexion);

	}

	function getUltimasUbicaciones($data){
		require("../../page/conectar.php");
		$page=0;
		$limit="";
		$fecha=date('Ymd',time());

		if(isset($data['page'])){
			$page=$data['page'];
			$limit=" limit {$page},10";
		}
		$sql="SELECT * FROM `ubicaciones` WHERE USUARIO={$data['usuario']} and  fecha BETWEEN '".str_replace('-','',$data['fecha'])."000000' and '".str_replace('-','',$data['fecha'])."235900'  ORDER BY (FECHA) ASC ";
		
		if(!isset($data['usuario'])){
			$sql="
			SELECT * FROM `ubicaciones` U
			inner join seguridad s on s.SEGURIDADID=U.USUARIO
			inner join empleado e on s.empleadoid=e.empleadoid
			WHERE substr(U.FECHA,1,8) = ".$fecha." GROUP BY (U.USUARIO) ";
		}
		//echo $sql;die;
		$conexion->query($sql);
		$conexion->resultset();
		$count=$conexion->rowCount();
		

		$conexion->query($sql.$limit);	
		$rs_sql = $conexion->resultset();
		if($conexion->rowCount() > 0){
			foreach ($rs_sql as $value) {
				$dat['lat']=floatval($value['LATITUD']);
				$dat['lng']=floatval($value['LONGITUD']);
				$dat['nombre']=$value['SEGURIDADUSUARIO'].' - '.$value['EMPLEADONOMBRE'];
				$dat['fecha']=$value['FECHA'];
				$dat['usuarioId']=$value['USUARIO'];
				$datos[]=$dat;
			}
			echo json_encode(['data'=>$datos,'count'=>$count,'estado'=>true]);
		}else{
			echo json_encode(["estado"=>false,"mensaje"=>"No se encontraron registros"]);
		}
		unset($conexion);

	}


	function getUltimasUbicacionesGrupo($data){
		require("../../page/conectar.php");
		$page=0;
		$limit="";
		if(isset($data['page'])){
			$page=$data['page'];
			$limit=" limit {$page},10";
		}


		$fecha=date('Ymd',time());
		//$fecha='20190218';
		if(isset($data['grupo'])){

			$sql="SELECT * FROM `ubicaciones` U
			inner join seguridad s on s.SEGURIDADID=U.USUARIO 
			INNER JOIN empleado e on s.EMPLEADOID=e.EMPLEADOID
			WHERE U.FECHA =(SELECT MAX(UT.`FECHA`) FROM ubicaciones UT WHERE UT.USUARIO=U.USUARIO) 
			and GRUPOID={$data['grupo']}
			and  fecha BETWEEN '".$fecha."000000' and '".$fecha."235900' GROUP BY(U.USUARIO) ";
		}else{
			die(json_encode(["estado"=>false,"mensaje"=>"Se presentó un error al realizar la peticion"]));
		}
		
//		var_dump($sql);
//die;
		$conexion->query($sql);	
		$rs_sql = $conexion->resultset();
		if($conexion->rowCount() > 0){
			foreach ($rs_sql as $value) {
				$dat['lat']=floatval($value['LATITUD']);
				$dat['lng']=floatval($value['LONGITUD']);
				$dat['nombre']=$value['SEGURIDADUSUARIO'];
				$dat['fecha']=$value['FECHA'];
				$dat['usuarioId']=$value['USUARIO'];
				$datos[]=$dat;
			}
			echo json_encode(['data'=>$datos,'estado'=>true]);
		}else{
			echo json_encode(["estado"=>false,"mensaje"=>"No se encontraron registros"]);
		}
		unset($conexion);

	}

	function getUbicacionesEncuesta($data){
		$data['fecha']=str_replace('-', '', $data['fecha']);
		require("../../page/conectar.php");
		$page=0;
		$limit="";
		if(isset($data['page'])){
			$page=$data['page'];
			$limit=" limit {$page},10";
		}

		$fecha=date('Ymd',time());
		//$fecha='20190218';
		if(isset($data['usuario']) && isset($data['page']) && isset($data['fecha'])){
			$sql="select * from persona where substring(FECHAENCUESTA,1,8)={$data['fecha']} and ENCUESTADORID={$data['usuario']} {$limit}";
			$sql2="select * from persona where substring(FECHAENCUESTA,1,8)={$data['fecha']} and ENCUESTADORID={$data['usuario']}";
			$conexion->query($sql2);
			$conexion->resultset();
			$count=$conexion->rowCount();
		}else{
			die(json_encode(["estado"=>false,"mensaje"=>"Se presentó un error al realizar la peticion"]));
		}
		
		$conexion->query($sql);	
		$rs_sql = $conexion->resultset();
		




		if($conexion->rowCount() > 0){
			foreach ($rs_sql as $value) {
				$dat['lat']=floatval($value['LATITUD']);
				$dat['lng']=floatval($value['LONGITUD']);
				$dat['fecha']=$value['FECHAENCUESTA'];
				$datos[]=$dat;
			}
			echo json_encode(['data'=>$datos,'estado'=>true,'count'=>$count]);
		}else{
			echo json_encode(["estado"=>false,"mensaje"=>"No se encontraron registros"]);
		}
		unset($conexion);

	}

	function actualizarContrasena($data){
		require("../../page/conectar.php");


		$sql="update seguridad set SEGURIDADCLAVE='{$data['contrasena']}' , VALIDADO=1 where SEGURIDADUSUARIO='{$data['userId']}'";

		$conexion->query($sql);
		$conexion->execute();

		if($conexion->rowCount()){
			http_response_code(201);
			echo json_encode(['mensaje'=>'Contraseña actualizada','estado'=>true]);
		}else{
			http_response_code(400);
			echo json_encode(['mensaje'=>'error al intentar actualizar la contraseña','estado'=>false]);
		}
		unset($conexion);
	}



	function obtenerBarrios(){
		require("../../page/conectar.php");
		$sql="SELECT * FROM `barrio` where REGISTROESTADO=1";
		//ini_set('display_errors', 1);
		$conexion->query($sql);	
		$rs_sql = $conexion->resultset();
		if($conexion->rowCount() > 0){
			echo json_encode(array('data'=>$rs_sql,'estado'=>true));
		}else{
			echo json_encode(["estado"=>false,"mensaje"=>"No se encontraron registros"]);
		}
		unset($conexion);

	}




}