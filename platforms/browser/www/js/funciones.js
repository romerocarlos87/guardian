

/*
 * 
 * Funciones para enviar datos al servidor
 */

function enviarUbicacionServer(data,id) {

	var settings = {
		"async": true,
		"crossDomain": true,
		"url": "https://guardian.plussoluciones.com/WebServiceGuardian/usuario/",
		"method": "POST",
		"headers": {
		"content-type": "application/x-www-form-urlencoded",
		"authorization": "Basic cmVnaXN0cm86SW52MHQwKjIwMTg=",
		},
		"data": {
		"OPT":'nuevaUbicacion',
		"data":data
		},
		"beforeSend":function(){
		
		},"error":function () {

			return false;
		}
	}

	$.ajax(settings).done(function (response) {
		deleteUbicacion(id)
		
	});
	
}


function progress(progress,total,numero){

	$("#"+progress).css({width: (((numero)/(total))*100)+"%"});
	if(total==numero){
		console.log(progress)
		switch (progress) {
			case 'p-ubicaciones':
				sincronizarVivienda()
				break;
				
			case 'p-viviendas':
				sincronizarMascotas()
				break;
			case 'p-mascotas':
				sincronizarPersonas()
				break;
			case 'p-personas':
				sincronizarPersonaPrimeraInfancia()
				break;
			case 'p-personaPrimeraInfancia':
				sincronizarPersonaSegundaInfancia()
				break;
			case 'p-personaSegundaInfancia':
					sincronizarPersonaAdolescencia()
				break;
			case 'p-personaAdolescencia':
					sincronizarPersonaAdultez()
					break;
			case 'p-personaAdultez':
					sincronizarSaludOcupacional()
					break;
			case 'p-saludOcupacional':
				sincronizarImagenes()
				break;
			case 'p-fotos':
					sincronizarGestacion()
					break;
			case 'p-gestacion':
					M.toast({html: 'Sincronización terminada.'})
					$("#sincronizar").removeAttr("disabled")
				EliminaDatosSincronizados()
				eliminarImagenesSincronizadas()
				break;
			default:
				break;
		}
	}
 }

 function guardarGestacionServer(data,id,sync=false) {

	var settings = {
		"async": false,
		"crossDomain": true,
		"url": "https://guardian.plussoluciones.com/WebServiceGuardian/Guardian/",
		"method": "POST",
		"headers": {
		"content-type": "application/x-www-form-urlencoded",
		//"authorization": "Basic cmVnaXN0cm86SW52MHQwKjIwMTg=",
		},
		"data": {
		"OPT":'guardarGestacion',
		"data":data
		},
		"beforeSend":function(){
			if (!sync) {
				M.toast({html: 'Enviando registro...'})
			}
		},"error":function () {
			if(sync){
				localStorage.setItem("enviadoGestacion",(parseInt(localStorage.getItem("enviadoGestacion"))+1));
				progress("p-gestacion",localStorage.getItem("totalGestacion"),localStorage.getItem("enviadoGestacion"));
			}else{
				M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})

			}
	}};

	$.ajax(settings).done(function (response) {
		
		if (response.estado) {
				
				updateIdGestacionLocal(id,response.Id);
				if (!sync) {
					M.toast({html: 'Registro almacenado!'})
				}
		}
		if(!response.estado){
			if (!sync) {
				M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})
			}
		}

		if(sync){
			localStorage.setItem("enviadoGestacion",(parseInt(localStorage.getItem("enviadoGestacion"))+1));
			progress("p-gestacion",localStorage.getItem("totalGestacion"),localStorage.getItem("enviadoGestacion"));
		}
	});
	
}  

function guardarUbicacionGuardianServer(data,id,sync=false) {

	var settings = {
		"async": false,
		"crossDomain": true,
		"url": "https://guardian.plussoluciones.com/WebServiceGuardian/Guardian/",
		"method": "POST",
		"headers": {
		"content-type": "application/x-www-form-urlencoded",
		//"authorization": "Basic cmVnaXN0cm86SW52MHQwKjIwMTg=",
		},
		"data": {
		"OPT":'guardarUbicacionGuardian',
		"data":data
		},
		"beforeSend":function(){
			if (!sync) {
				M.toast({html: 'Enviando registro...'})
			}
		},"error":function () {
			if(sync){
				localStorage.setItem("enviadoFichaGuardian",(parseInt(localStorage.getItem("enviadoFichaGuardian"))+1));
				progress("p-ubicaciones",localStorage.getItem("totalFichaGuardian"),localStorage.getItem("enviadoFichaGuardian"));
			}else{
				M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})

			}
	}};

	$.ajax(settings).done(function (response) {
		
		if (response.estado) {
				
				updateIdUbicacionGuardianLocal(id,response.Id);
				if (!sync) {
					M.toast({html: 'Registro almacenado!'})
				}
		}
		if(!response.estado){
			if (!sync) {
				M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})
			}
		}

		if(sync){
			localStorage.setItem("enviadoFichaGuardian",(parseInt(localStorage.getItem("enviadoFichaGuardian"))+1));
			progress("p-ubicaciones",localStorage.getItem("totalFichaGuardian"),localStorage.getItem("enviadoFichaGuardian"));
		}
	});
	
}  
function loadImage(pathImage) {
    return new Promise(function(resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", pathImage, true);
        xhr.responseType = "blob";
        xhr.onload = function (e) {
            // console.log(this.response);
            // var reader = new FileReader();
            // reader.onload = function(event) {
            //     var res = event.target.result;
            //     // console.log(res)
            // };
            // var file = this.response;
            // reader.readAsDataURL(file);
            resolve(this.response);
        };
        xhr.send();
    });
};

function guardarImagenServer(data,id,sync=false) {

	loadImage(data.IMAGENRUTA).then(function(imageBlob){
		var dataSend = new FormData();
		dataSend.append("OPT","guardarImagen");
		dataSend.append("photo", imageBlob);
		dataSend.append("fileName",data.UBICACIONUUID);
		dataSend.append("usuario",data.USUARIO);
		dataSend.append("fecha",data.FECHA);
		dataSend.append("fichaguardianid",data.FICHAGUARDIANID);
	
		$.ajax({
		  method: "POST",
		  cache: false,
		  contentType: false,
		  processData: false,
		  url: 'https://guardian.plussoluciones.com/WebServiceGuardian/Guardian/',
		  data: dataSend,
	
		})
		.done(function(data) {
			if(data.estado==true){
				localStorage.setItem("enviadoFoto",(parseInt(localStorage.getItem("enviadoFoto"))+1));
				progress("p-fotos",localStorage.getItem("totaFoto"),localStorage.getItem("enviadoFoto"));
				updateIdImagen(id,data.Id)
			}else{
				localStorage.setItem("enviadoFoto",(parseInt(localStorage.getItem("enviadoFoto"))+1));
				progress("p-fotos",localStorage.getItem("totaFoto"),localStorage.getItem("enviadoFoto"));
			}
			
		})
		.fail(function( jqxhr, textStatus, error ) {
		  console.log(error)
		});
	  },function(){
		if(sync){
			localStorage.setItem("enviadoFoto",(parseInt(localStorage.getItem("enviadoFoto"))+1));
			progress("p-fotos",localStorage.getItem("totaFoto"),localStorage.getItem("enviadoFoto"));
		}else{
			M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})

		}
	  });
}

function guardarViviendaServer(data,id,sync=false) {

	var settings = {
		"async": false,
		"crossDomain": true,
		"url": "https://guardian.plussoluciones.com/WebServiceGuardian/Guardian/",
		"method": "POST",
		"headers": {
		"content-type": "application/x-www-form-urlencoded",
		//"authorization": "Basic cmVnaXN0cm86SW52MHQwKjIwMTg=",
		},
		"data": {
		"OPT":'guardarVivienda',
		"data":data
		},
		"beforeSend":function(){
			if (!sync) {
				M.toast({html: 'Enviando registro...'})
			}
		},"error":function () {
			if(sync){
				localStorage.setItem("enviadoVivienda",(parseInt(localStorage.getItem("enviadoVivienda"))+1));
				progress("p-viviendas",localStorage.getItem("totalVivienda"),localStorage.getItem("enviadoVivienda"));
			}else{
				M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})

			}
	}};

	$.ajax(settings).done(function (response) {
		if (response.estado) {
				updateIdViviendaLocal(id,response.Id);
				if (!sync) {
					M.toast({html: 'Registro almacenado!'})
				}
		}
		if(!response.estado){
			if (!sync) {
				M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})
			}
		}
		if(sync){
			localStorage.setItem("enviadoVivienda",(parseInt(localStorage.getItem("enviadoVivienda"))+1));
			progress("p-viviendas",localStorage.getItem("totalVivienda"),localStorage.getItem("enviadoVivienda"));
		}
	});
	
}

function guardarMascotaServer(data,id,sync=false) {

	var settings = {
		"async": false,
		"crossDomain": true,
		"url": "https://guardian.plussoluciones.com/WebServiceGuardian/Guardian/",
		"method": "POST",
		"headers": {
		"content-type": "application/x-www-form-urlencoded",
		//"authorization": "Basic cmVnaXN0cm86SW52MHQwKjIwMTg=",
		},
		"data": {
		"OPT":'guardarMascota',
		"data":data
		},
		"beforeSend":function(){
			if (!sync) {
				M.toast({html: 'Enviando registro...'})
			}
		},"error":function () {
			if(sync){
				localStorage.setItem("enviadoMascota",(parseInt(localStorage.getItem("enviadoMascota"))+1));
				progress("p-mascotas",localStorage.getItem("totalMascota"),localStorage.getItem("enviadoMascota"));
			}else{
				M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})
			}
	}};

	$.ajax(settings).done(function (response) {
		if (response.estado) {
			updateIdMascotaLocal(id,response.Id);
			if (!sync) {
				M.toast({html: 'Registro almacenado!'})
			}
		}
		if(!response.estado){
			if (!sync) {
				M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})
			}
		}
		if(sync){
			localStorage.setItem("enviadoMascota",(parseInt(localStorage.getItem("enviadoMascota"))+1));
			progress("p-mascotas",localStorage.getItem("totalMascota"),localStorage.getItem("enviadoMascota"));
		}
	});
	
}

function guardarPersonaServer(data,id,sync=false) {

	var settings = {
		"async": false,
		"crossDomain": true,
		"url": "https://guardian.plussoluciones.com/WebServiceGuardian/Guardian/",
		"method": "POST",
		"headers": {
		"content-type": "application/x-www-form-urlencoded",
		//"authorization": "Basic cmVnaXN0cm86SW52MHQwKjIwMTg=",
		},
		"data": {
		"OPT":'guardarPersona',
		"data":data
		},
		"beforeSend":function(){
			if (!sync) {
				M.toast({html: 'Enviando registro...'});
			}
		},"error":function () {
			if(sync){
				localStorage.setItem("enviadoPersona",(parseInt(localStorage.getItem("enviadoPersona"))+1));
				progress("p-personas",localStorage.getItem("totalPersona"),localStorage.getItem("enviadoPersona"));
			}else{
				M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'});
			}
	}};

	$.ajax(settings).done(function (response) {
		if (response.estado) {
			updateIdPersonaLocal(id,response.Id);
			if (!sync) {
				M.toast({html: 'Registro almacenado!'})
			}
		}
		if(!response.estado){
			if (!sync) {
				M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'});
			}
		}
		if(sync){
			localStorage.setItem("enviadoPersona",(parseInt(localStorage.getItem("enviadoPersona"))+1));
			progress("p-personas",localStorage.getItem("totalPersona"),localStorage.getItem("enviadoPersona"));
		}
	});
}
	function guardarPersonaPrimeraInfanciaServer(data,id,sync=false) {

		var settings = {
			"async": false,
			"crossDomain": true,
			"url": "https://guardian.plussoluciones.com/WebServiceGuardian/Guardian/",
			"method": "POST",
			"headers": {
			"content-type": "application/x-www-form-urlencoded",
			//"authorization": "Basic cmVnaXN0cm86SW52MHQwKjIwMTg=",
			},
			"data": {
			"OPT":'guardarPersonaPrimeraInfancia',
			"data":data
			},
			"beforeSend":function(){
				if (!sync) {
					M.toast({html: 'Enviando registro...'})
				}
			},"error":function () {
				if(sync){
					localStorage.setItem("enviadoPersonaPrimeraInfancia",(parseInt(localStorage.getItem("enviadoPersonaPrimeraInfancia"))+1));
					progress("p-personaPrimeraInfancia",localStorage.getItem("totalPersonaPrimeraInfancia"),localStorage.getItem("enviadoPersonaPrimeraInfancia"));
				}else{
					M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})
				}
		}};
	
		$.ajax(settings).done(function (response) {
			if (response.estado) {
				updateIdPersonaPrimeraInfanciaLocal(id,response.Id);
				if (!sync) {
					M.toast({html: 'Registro almacenado!'})
				}
			}
			if(!response.estado){
				if (!sync) {
					M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})
				}
			}
			if(sync){
				localStorage.setItem("enviadoPersonaPrimeraInfancia",(parseInt(localStorage.getItem("enviadoPersonaPrimeraInfancia"))+1));
				progress("p-personaPrimeraInfancia",localStorage.getItem("totalPersonaPrimeraInfancia"),localStorage.getItem("enviadoPersonaPrimeraInfancia"));
			}
		});

}

function guardarPersonaSegundaInfanciaServer(data,id,sync=false) {

	var settings = {
		"async": false,
		"crossDomain": true,
		"url": "https://guardian.plussoluciones.com/WebServiceGuardian/Guardian/",
		"method": "POST",
		"headers": {
		"content-type": "application/x-www-form-urlencoded",
		//"authorization": "Basic cmVnaXN0cm86SW52MHQwKjIwMTg=",
		},
		"data": {
		"OPT":'guardarPersonaSegundaInfancia',
		"data":data
		},
		"beforeSend":function(){
			if (!sync) {
				M.toast({html: 'Enviando registro...'})
			}
		},"error":function () {
			if(sync){
				localStorage.setItem("enviadoPersonaSegundaInfancia",(parseInt(localStorage.getItem("enviadoPersonaSegundaInfancia"))+1));
				progress("p-personaSegundaInfancia",localStorage.getItem("totalPersonaSegundaInfancia"),localStorage.getItem("enviadoPersonaSegundaInfancia"));
			}else{
				M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})
			}
	}};

	$.ajax(settings).done(function (response) {
		if (response.estado) {
			updateIdPersonaSegundaInfanciaLocal(id,response.Id);
			if (!sync) {
				M.toast({html: 'Registro almacenado!'})
			}
		}
		if(!response.estado){
			if (!sync) {
				M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})
			}
		}
		if(sync){
			localStorage.setItem("enviadoPersonaSegundaInfancia",(parseInt(localStorage.getItem("enviadoPersonaSegundaInfancia"))+1));
			progress("p-personaSegundaInfancia",localStorage.getItem("totalPersonaSegundaInfancia"),localStorage.getItem("enviadoPersonaSegundaInfancia"));
		}
	});

}


function guardarPersonaAdolescenciaServer(data,id,sync=false) {

	var settings = {
		"async": false,
		"crossDomain": true,
		"url": "https://guardian.plussoluciones.com/WebServiceGuardian/Guardian/",
		"method": "POST",
		"headers": {
		"content-type": "application/x-www-form-urlencoded",
		//"authorization": "Basic cmVnaXN0cm86SW52MHQwKjIwMTg=",
		},
		"data": {
		"OPT":'guardarPersonaAdolescencia',
		"data":data
		},
		"beforeSend":function(){
			if (!sync) {
				M.toast({html: 'Enviando registro...'})
			}
		},"error":function () {
			if(sync){
				localStorage.setItem("enviadoPersonaAdolescencia",(parseInt(localStorage.getItem("enviadoPersonaAdolescencia"))+1));
				progress("p-personaAdolescencia",localStorage.getItem("totalPersonaAdolescencia"),localStorage.getItem("enviadoPersonaAdolescencia"));
			}else{
				M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})
			}
	}};

	$.ajax(settings).done(function (response) {
		if (response.estado) {
			updateIdAdolescenciaLocal(id,response.Id);
			if (!sync) {
				M.toast({html: 'Registro almacenado!'})
			}
		}
		if(!response.estado){
			if (!sync) {
				M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})
			}
		}
		if(sync){
			localStorage.setItem("enviadoPersonaAdolescencia",(parseInt(localStorage.getItem("enviadoPersonaAdolescencia"))+1));
			progress("p-personaAdolescencia",localStorage.getItem("totalPersonaAdolescencia"),localStorage.getItem("enviadoPersonaAdolescencia"));
		}
	});

}


function guardarPersonaAdultezServer(data,id,sync=false) {

	var settings = {
		"async": false,
		"crossDomain": true,
		"url": "https://guardian.plussoluciones.com/WebServiceGuardian/Guardian/",
		"method": "POST",
		"headers": {
		"content-type": "application/x-www-form-urlencoded",
		//"authorization": "Basic cmVnaXN0cm86SW52MHQwKjIwMTg=",
		},
		"data": {
		"OPT":'guardarPersonaAdultez',
		"data":data
		},
		"beforeSend":function(){
			if (!sync) {
				M.toast({html: 'Enviando registro...'})
			}
		},"error":function () {
			if(sync){
				localStorage.setItem("enviadoPersonaAdultez",(parseInt(localStorage.getItem("enviadoPersonaAdultez"))+1));
				progress("p-personaAdultez",localStorage.getItem("totalPersonaAdultez"),localStorage.getItem("enviadoPersonaAdultez"));
			}else{
				M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})
			}
	}};

	$.ajax(settings).done(function (response) {
		if (response.estado) {
			updateIdPersonaAdultezLocal(id,response.Id);
			if (!sync) {
				M.toast({html: 'Registro almacenado!'})
			}
		}
		if(!response.estado){
			if (!sync) {
				M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})
			}
		}
		if(sync){
			localStorage.setItem("enviadoPersonaAdultez",(parseInt(localStorage.getItem("enviadoPersonaAdultez"))+1));
			progress("p-personaAdultez",localStorage.getItem("totalPersonaAdultez"),localStorage.getItem("enviadoPersonaAdultez"));
		}
	});

}


function guardarSaludOcupacionalServer(data,id,sync=false) {

	var settings = {
		"async": false,
		"crossDomain": true,
		"url": "https://guardian.plussoluciones.com/WebServiceGuardian/Guardian/",
		"method": "POST",
		"headers": {
		"content-type": "application/x-www-form-urlencoded",
		//"authorization": "Basic cmVnaXN0cm86SW52MHQwKjIwMTg=",
		},
		"data": {
		"OPT":'guardarSaludOcupacional',
		"data":data
		},
		"beforeSend":function(){
			if (!sync) {
				M.toast({html: 'Enviando registro...'})
			}
		},"error":function () {
			if(sync){
				localStorage.setItem("enviadoSaludOcupacional",(parseInt(localStorage.getItem("enviadoSaludOcupacional"))+1));
				progress("p-saludOcupacional",localStorage.getItem("totalSaludOcupacional"),localStorage.getItem("enviadoSaludOcupacional"));
			}else{
				M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})
			}
	}};

	$.ajax(settings).done(function (response) {
		if (response.estado) {
			updateIdSaludOcupacionalLocal(id,response.Id);
			if (!sync) {
				M.toast({html: 'Registro almacenado!'})
			}
		}
		if(!response.estado){
			if (!sync) {
				M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})
			}
		}
		if(sync){
			localStorage.setItem("enviadoSaludOcupacional",(parseInt(localStorage.getItem("enviadoSaludOcupacional"))+1));
			progress("p-saludOcupacional",localStorage.getItem("totalSaludOcupacional"),localStorage.getItem("enviadoSaludOcupacional"));
		}
	});

}

function actualizarContrasena(contrasena) {

	var settings = {
		"async": true,
		"crossDomain": true,
		"url": "https://guardian.plussoluciones.com/WebServiceGuardian/usuario/",
		"method": "POST",
		"headers": {
		"content-type": "application/x-www-form-urlencoded",
	//	"authorization": "Basic cmVnaXN0cm86SW52MHQwKjIwMTg=",
		},
		"data": {
		"OPT":'actualizarContrasena',
		"data":{'userId': localStorage.getItem('userName'),'contrasena':contrasena}
		},
		"beforeSend":function(){
			M.toast({html: 'Actualizando contraseña...'})
		},"error":function () {
			M.toast({html: 'Se presentó un error al intentar actualizar la contraseña.'})
		}
	}

	$.ajax(settings).done(function (response) {
		if (response.estado) {
			location.href ="index.html";
			M.toast({html: response.mensaje})
		} else {
			M.toast({html: 'Se presentó un error al intentar actualizar la contraseña'})
		}
	});
	
}


function obtenerUbicacion(){
	console.log('obteniendo ubicacion');
	
	var distance=null;

	navigator.geolocation.watchPosition(
		//Si todo va bien
		(position)=>{

		//	currentPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			 // Establecer la distancia si lastPosition existe (validacion de primera vez)
			 if (localStorage.getItem('latitud') !== null && localStorage.getItem('longitud') !== null) {
			//	last=new google.maps.LatLng(localStorage.getItem('latitude'), localStorage.getItem('longitude'))
			//	distance = google.maps.geometry.spherical.computeDistanceBetween(last, currentPosition);
			distance= getDistancia(localStorage.getItem('latitude'), localStorage.getItem('longitude'),position.coords.latitude, position.coords.longitude)
			

			  } else {
				localStorage.setItem('longitud',position.coords.longitude)
				localStorage.setItem('latitud',position.coords.latitude)
			  }
			  // Si la distancia supera los 20 mts actualizar la posicion de la marca
			  //console.log('Distancia ' + distance)
//			  console.log(localStorage.getItem('force'))
			  if (distance >= localStorage.getItem('minDistancia') || localStorage.getItem('force')=="true") {
				localStorage.setItem('longitud',position.coords.longitude)
				localStorage.setItem('latitud',position.coords.latitude)
			
				
				ubicacion={LONGITUD:position.coords.longitude,LATITUD:position.coords.latitude,USUARIO:localStorage.getItem('userId'),FECHA:getFechaActual()}
				now=Date.now()
				time = ( now - localStorage.getItem('lTime'))/1000
				console.log('Tiempo transcurrido: ' + parseInt(time)+ ' Segundos')
				if(time > localStorage.getItem('maxTime') || localStorage.getItem('force')=="true"){
				
					localStorage.setItem('lTime',now)
					localStorage.setItem('force',"false")
					addPosition(ubicacion)
				}
			  }
		},
		//Si hay error
		(error)=>{
			if (error.code==2) {
				M.toast({html: error.message})
			}
		},
		//Configuraciones
		{
			enableHighAccuracy: false,
			timeout: 30000,
			maximumAge: localStorage.getItem('maxTime')
		  });
}






function getDistancia(lat1,lon1,lat2,lon2){
 rad = function(x) {return x*Math.PI/180;}
	var R = 6378.137; //Radio de la tierra en km
 	var dLat = rad( lat2 - lat1 );
  var dLong = rad( lon2 - lon1 );
  var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var d = (R * c) * 1000;
	return parseInt(d); //Retorna tres decimales
 }

 function guardarEscolarServer(data,id,sync=false) {

	var settings = {
		"async": false,
		"crossDomain": true,
		"url": "https://guardian.plussoluciones.com/WebServiceGuardian/Guardian/",
		"method": "POST",
		"headers": {
		"content-type": "application/x-www-form-urlencoded",
		//"authorization": "Basic cmVnaXN0cm86SW52MHQwKjIwMTg=",
		},
		"data": {
		"OPT":'guardarEscolar',
		"data":data
		},
		"beforeSend":function(){
			if (!sync) {
				M.toast({html: 'Enviando registro...'})
			}
		},"error":function () {
			if(sync){
				localStorage.setItem("enviadoEscolar",(parseInt(localStorage.getItem("enviadoEscolar"))+1));
				progress("p-escolar",localStorage.getItem("totalEscolar"),localStorage.getItem("enviadoEscolar"));
			}else{
				M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})
			}
	}};

	$.ajax(settings).done(function (response) {
		if (response.estado) {
			updateIdEscolarLocal(id,response.Id);
			if (!sync) {
				M.toast({html: 'Registro almacenado!'})
			}
		}
		if(!response.estado){
			if (!sync) {
				M.toast({html: 'Se presentó un error al intentar enviar el registro, se almacena en el dispositivo.'})
			}
		}
		if(sync){
			localStorage.setItem("enviadoEscolar",(parseInt(localStorage.getItem("enviadoEscolar"))+1));
			progress("p-personaEscolar",localStorage.getItem("totalEscolar"),localStorage.getItem("enviadoEscolar"));
		}
	});

}