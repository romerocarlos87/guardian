function testInsertFichaGuardian() {
    for (let index = 1; index < 10; index++) {
        datos={
            "NUMERO":12,
            "CIUDADID":903,
            "CABECERA":"cabecera",
            "CORREGIMIENTO":"corregimiento",
            "VEREDA":"vereda",
            "MANZANA":"Manzana",
            "VIVIENDA":"vivienda",
            "FAMILIA":"Familia",
            "BARRIO":"barrio",
            "DIRECCION":"direccion",
            "TELEFONO":"TELEFONO",
            "EPSID":12,
            "USUARIOCREACION":'carlosromeropertuz',
            "MOMENTOCREACION":getFechaActual(),
            "ESTADOREGISTRO":1,
            "LONGITUD": localStorage.getItem('longitud'),
            "LATITUD": localStorage.getItem('latitud'),
            "UUID":uuid.v4(),
            "INICIOCAPTURA":getFechaActual()
        };
        guardarUbicacionGuardianLocal(datos);
    }
}

function testInsertMiembro() {
    for (let index = 1; index < 10; index++) {
        for (let index2 = 1; index2 < 1; index2++) {
        datos={
            DISCAPACIDAD: "2",
            DISCAPACIDADCUAL: "dISCAPACIDAD OTRA",
            EPSID: 2,
            EPSOTRO: "ASDSDASDA",
            ESTADOCIVILID: 2,
            ESTADOREGISTRO: 1,
            ETNIAID: 1,
            FECHANACIMIENTO: "2019-12-31",
            FICHAGUARDIANID: index,
            GENEROID: 1,
            GESTACION: "SI",
            GRUPOORGANIZADO: "2",
            GRUPOPOBLACIONALID: 1,
            MOMENTOCREACION: "20191006183723",
            NIVELDEESTUDIOID: 1,
            NOMBRE: "CARLOS",
            NUMERODOCUMENTO: "21314345649879",
            OCUPACIONID: 2,
            TIPOAFILIACIONID: 1,
            TIPODOCUMENTOID: 2,
            TIPOMIEMBROID: 1,
            TIPOSALUDID: 2,
            USUARIOCREACION: "asdasds",
            UUID: uuid.v4()
        }
        guardarPersonaLocal(datos)    
    }
    }
    
}

function testInsertVivienda() {
   for (let index = 1; index < 10; index++) {
    datos={
   AGUATRATADA: "0",
   ARROYO: "0",
   BASURERO: "0",
   DEPOSITOAGUACUAL: "0",
   DEPOSITOAGUAID: 1,
   DEPOSITOAGUAOTRO: "",
   ESTADOREGISTRO: 1,
   FICHAGUARDIANID: index,
   FUENTEAGUAID: 1,
   FUENTEAGUAOTRO: "",
   HUMO: "0",
   ILUMINACION: "0",
   INGRESO: "0",
   INGRESOCUAL: "",
   LARVAS: "0",
   MANEJOBASURA: "0",
   MOMENTOCREACION: "20191007131134",
   PERSONADORMITORIO: "",
   ROEDORES: "0",
   SERVICIOSANITARIOID: 1,
   TIPOALUMBRADOID: 1,
   TIPOALUMBRADOOTRO: "",
   TIPOPAREDID: 1,
   TIPOPISOID: 1,
   TIPOTECHOID: 2,
   TIPOVIVIENDAID: 1,
   TRATAMIENTO: "0",
   USUARIOCREACION: 'asdasd',
   UUID: uuid.v4(),
   VENTILACION: "0"}
   guardarViviendaLocal(datos)
   }
}

function testInsertMascota(){
        for (let index = 1; index < 10; index++) {
            for (let index = 1; index < 2; index++) {
            datos={
                ESTADOREGISTRO: 1,
                FICHAGUARDIANID: index,
                MOMENTOCREACION: "20191010194723",
                TIPOMASCOTAID: 5,
                TIPOMASCOTAOTRO: "ewrewr",
                USUARIOCREACION: "carlos.romero54",
                UUID: uuid.v4(),
                VACUANADA: "NO"
            }
            guardarMascotaLocal(datos)
        }
    }
}

function testInsertPersonaPrimeraInfancia(){
    for (let index = 1; index < 10; index++) {
        data = {
        CONTROLCYD: "SI",
        ESQUEMA: "SI",
        ESTADOREGISTRO: 1,
        LACTANCIA: "SI",
        LACTANCIATIEMPO: 0,
        MALTRATO: "SI",
        MOMENTOCREACION: 20191010215746,
        N_PESOACTUAL: 0,
        N_PESONACER: 0,
        N_TALLAACTUAL: 0,
        N_TALLANACER: 0,
        PERSONAID: index,
        USUARIOCREACION: 'carlos.romero54',
        UUID: uuid.v4(),
        V_BCG: "SI",
        V_CARNE: "SI",
        V_HEPB: "SI",
        V_INFLUENZA: "2",
        V_NEUMOCOCO: "2",
        V_PENTAVALENTE: "2",
        V_POLIO: "2",
        V_ROTAVIRUS: "2",
    }
 addPersonaPrimeraInfanciaLocal(data)
}
}




function testInsertPersonaSegundaInfancia(){
    for (let index = 1; index < 10; index++){
        
    data={
        CONTROLCYD: "SI",
        ESTADOREGISTRO: 1,
        MALTRATO: "SI",
        MOMENTOCREACION: "20191010232149",
        NUMERODESPARASITADA: "3",
        N_PESOACTUAL: "2",
        N_TALLAACTUAL: "2",
        PERSONAID: index,
        S_CONSULTAODONTOLOGO: "SI",
        S_FACTORCARIOGENICOID: 1,
        S_NUMEROCEPILLADA: "",
        USUARIOCREACION: "carlos.romero54",
        UUID: uuid.v4(),
        V_CARNE: "SI",
        V_ESQUEMA: "SI",
        V_TT: "SI",
        V_VPH: "SI",
        V_VPHDOSIS: 34
    }
    addPersonaSegundaInfanciaLocal(data)
}
}


function testInsertAdolescencia(){
    for (let index = 1; index < 10; index++) {
        data={
        CITOLOGIA: "SI",
        CITOLOGIADETALLE: "SI",
        ESTADOREGISTRO: 1,
        EXAMENMEDICO: "0",
        H_TOTALABORTADO: "0",
        H_TOTALVIVO: "0",
        LESIONES: "SI",
        MOMENTOCREACION: "20191011000008",
        PERSONAID: index,
        P_METODO: "asd",
        P_MOTIVO: "asd",
        P_PLANIFICA: "SI",
        SENO: "SI",
        SENOCUAL: "",
        TOS: "SI",
        USUARIOCREACION: "carlos.romero54",
        UUID: uuid.v4(),
        VIOLENCIA: "SI",
        V_TD: "SI",
        V_TDCUAL: "D1",
        V_VPH: "SI"
    }
    addPersonaAdolescensJuventudiaLocal(data)
}
}

function testInsertPersonaAdultez(){
    for (let index = 1; index < 10; index++) {
  
    datos={
        CITOLOGIA: "SI",
        CITOLOGIADETALLE: "",
        EJERCICIO: "SI",
        ESTADOREGISTRO: 1,
        E_CANCER: "SI",
        E_CANCERCONTROL: "SI",
        E_CANCERDETALLE: "",
        E_DIABETES: "SI",
        E_DIABETESCONTROL: "SI",
        E_EPOC: "SI",
        E_EPOCCONTROL: "SI",
        E_HIPERTENSION: "SI",
        E_HIPERTENSIONCONTROL: "SI",
        FUMA: "SI",
        LESIONES: "SI",
        MOMENTOCREACION: 20191011005358,
        N_CINTURA: 0,
        N_PESOACTUAL: 0,
        N_TALLAACTUAL: 0,
        PERSONAID: 7,
        PROSTATA: "SI",
        PROSTATADETALLE: "asdsad",
        SENO: "SI",
        SENODETALLE: "",
        TOMA: "SI",
        TOS: "SI",
        USUARIOCREACION: "carlos.romero54",
        UUID: uuid.v4(),
        VISION: "SI",
        VISIONDETALLE: ""
    }
    addPersonaAdultezLocal(datos)
}
}

function noSinc(){
    conn.transaction(function(tx) {
        tx.executeSql("delete from fichaguardian", [], function(tx, results) {
          console.log('depurada fichaguardian');
        });
        tx.executeSql("delete from persona", [], function(tx, results) {
            console.log('depurada persona');
        });
        tx.executeSql("delete from vivienda", [], function(tx, results) {
          console.log('depurada vivienda');
        });
        tx.executeSql("delete from mascota", [], function(tx, results) {
          console.log('depurada mascota');
        });
        tx.executeSql("delete from personaprimerainfancia", [], function(tx, results) {
          console.log('depurada personaprimerainfancia');
        });
        tx.executeSql("delete from personasegundainfancia", [], function(tx, results) {
          console.log('depurada personasegundainfancia');
        });
        tx.executeSql("delete from saludocupacional", [], function(tx, results) {
          console.log('depurada saludocupacional');
        });
        tx.executeSql("delete from personaadolescencia", [], function(tx, results) {
          console.log('depurada personaadolescencia');
        });
        tx.executeSql("delete from personaadultez", [], function(tx, results) {
          console.log('depurada personaadultez');
        });
      
      },(e)=>{console.log(e)});
}
function testDB() {
    noSinc()
    testInsertMascota()
    testInsertMiembro()
    testInsertPersonaAdultez()
    testInsertPersonaPrimeraInfancia()
    testInsertPersonaSegundaInfancia()
    testInsertVivienda()
    testInsertFichaGuardian()
        
}
