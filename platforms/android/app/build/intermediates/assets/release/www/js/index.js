/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
            this.receivedEvent('deviceready');
         
            
            crearDB()
            if (localStorage.getItem('userName')!=null && localStorage.getItem('userId')!=null) {
                
           // cordova.plugins.backgroundMode.excludeFromTaskList();
            cordova.plugins.backgroundMode.setEnabled(true);
            
            cordova.plugins.backgroundMode.on('disable', ()=>{
                cordova.plugins.backgroundMode.moveToBackground();
            });
            cordova.plugins.backgroundMode.overrideBackButton();

            cordova.plugins.backgroundMode.setDefaults({
                title: "ESoft",
                text: "Working...",
                icon: '/platforms/android/app/src/main/res/mipmap-hdpi/icon.png', // this will look for icon.png in platforms/android/res/drawable|mipmap
                color: '2196F3', // hex format like 'F14F4D'
                resume: true,
                hidden: true,
                bigText: true,
                silent: false
            })

            cordova.plugins.backgroundMode.on('activate', ()=>{
                cordova.plugins.backgroundMode.disableWebViewOptimizations(); 
                obtenerUbicacion()
            
            });
            setInterval(function () {
                //localStorage.setItem('force',true)
                //obtenerUbicacion()
            },30000)
        }else{
            getSesion()
            localStorage.setItem('force',"false")
            localStorage.setItem('maxTime',600) // tiempo en en segundos - (300 = 5')
            localStorage.setItem('minDistancia',0) //Distancia en metros
            localStorage.setItem('lTime',Date.now())
            localStorage.setItem('longitud',0)
            localStorage.setItem('latitud',0)
            cordova.plugins.backgroundMode.setEnabled(true);
        }

    },
   
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        console.log('Received Event: ' + id);
    }
};

app.initialize();

