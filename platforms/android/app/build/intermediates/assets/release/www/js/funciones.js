/*
 * 
 * Funciones para enviar datos al servidor
 */

function enviarUbicacion(data,id) {

	var settings = {
		"async": true,
		"crossDomain": true,
		"url": "https://encuesta.plussoluciones.com/WebService/usuario/",
		"method": "POST",
		"headers": {
		"content-type": "application/x-www-form-urlencoded",
		//"authorization": "Basic cmVnaXN0cm86SW52MHQwKjIwMTg=",
		},
		"data": {
		"OPT":'nuevaUbicacion',
		"data":data
		},
		"beforeSend":function(){
		
		},"error":function () {

			return false;
		}
	}

	$.ajax(settings).done(function (response) {
		deleteUbicacion(id)
		
	});
	
}


function progress(){
	console.log(parseInt(localStorage.getItem('te')),parseInt(localStorage.getItem('tne')))
	if(parseInt(localStorage.getItem('te'))==parseInt(localStorage.getItem('tne'))){
				if(localStorage.getItem('errorSync')=='1'){
					M.toast({html: 'Sincronización terminada, se presentaron errores en el proceso'})
					$("#sincronizar").text('Sincronizar')
					$("#sincronizar").attr('disabled',false)
				}else	if(localStorage.getItem('errorSync')=='0'){
					$("#sincronizar").text('Sincronizar')
			$("#sincronizar").attr('disabled',true)
					M.toast({html: 'Sincronización terminada exitosamente!!'})
				}
			
			
	}else{
			$("#sincronizar").text('Sincronizando...')
			$("#sincronizar").attr('disabled',true)
	}
	$("#progress").css({width: ((parseInt(localStorage.getItem('te'))/parseInt(localStorage.getItem('tne')))*100)+"%"})

 }

function enviarEncuesta(data,id,sync=false) {

	var settings = {
		"async": true,
		"crossDomain": true,
		"url": "https://encuesta.plussoluciones.com/WebService/usuario/",
		"method": "POST",
		"headers": {
		"content-type": "application/x-www-form-urlencoded",
	//	"authorization": "Basic cmVnaXN0cm86SW52MHQwKjIwMTg=",
		},
		"data": {
		"OPT":'nuevaEncuesta',
		"data":data
		},
		"beforeSend":function(){
			if (!sync) {
				M.toast({html: 'Enviando encuesta...'})
			}
		},"error":function () {
			if(sync){
				localStorage.setItem('te',parseInt(localStorage.getItem('te'))+1)
				localStorage.setItem('errorSync',1)
				progress()
			}else{
				M.toast({html: 'Se presentó un error al intentar registrar la encuesta, se almacena en el dispositivo.'})

			}
	}}

	$.ajax(settings).done(function (response) {
		if (response.estado) {
				updateStats(false,false)
				updateStats(true,true)
				deletePersona(id)
				if (!sync) {
					M.toast({html: 'Encuesta enviada!'})
				}
		}

		if(!response.estado){
			if (!sync) {
				M.toast({html: 'Se presentó un error al intentar registrar la encuesta, se almacena en el dispositivo.'})
			}
		}

		if(sync){
			localStorage.setItem('te',parseInt(localStorage.getItem('te'))+1)
			progress()
		}
	});
	
}




function actualizarContrasena(contrasena) {

	var settings = {
		"async": true,
		"crossDomain": true,
		"url": "https://encuesta.plussoluciones.com/WebService/usuario/",
		"method": "POST",
		"headers": {
		"content-type": "application/x-www-form-urlencoded",
	//	"authorization": "Basic cmVnaXN0cm86SW52MHQwKjIwMTg=",
		},
		"data": {
		"OPT":'actualizarContrasena',
		"data":{'userId': localStorage.getItem('userName'),'contrasena':contrasena}
		},
		"beforeSend":function(){
			M.toast({html: 'Actualizando contraseña...'})
		},"error":function () {
			M.toast({html: 'Se presentó un error al intentar actualizar la contraseña.'})
		}
	}

	$.ajax(settings).done(function (response) {
		if (response.estado) {
			location.href ="encuesta.html";
			M.toast({html: response.mensaje})
		} else {
			M.toast({html: 'Se presentó un error al intentar actualizar la contraseña'})
		}
	});
	
}


function obtenerUbicacion(){
	console.log('obteniendo ubicacion');
	
	var distance=null;

	navigator.geolocation.watchPosition(
		//Si todo va bien
		(position)=>{

		//	currentPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			 // Establecer la distancia si lastPosition existe (validacion de primera vez)
			 if (localStorage.getItem('latitud') !== null && localStorage.getItem('longitud') !== null) {
			//	last=new google.maps.LatLng(localStorage.getItem('latitude'), localStorage.getItem('longitude'))
			//	distance = google.maps.geometry.spherical.computeDistanceBetween(last, currentPosition);
			distance= getDistancia(localStorage.getItem('latitude'), localStorage.getItem('longitude'),position.coords.latitude, position.coords.longitude)
			

			  } else {
				localStorage.setItem('longitud',position.coords.longitude)
				localStorage.setItem('latitud',position.coords.latitude)
			  }
			  // Si la distancia supera los 20 mts actualizar la posicion de la marca
			  //console.log('Distancia ' + distance)
//			  console.log(localStorage.getItem('force'))
			  if (distance >= localStorage.getItem('minDistancia') || localStorage.getItem('force')=="true") {
				localStorage.setItem('longitud',position.coords.longitude)
				localStorage.setItem('latitud',position.coords.latitude)
				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth() + 1; //January is 0!
				var yyyy = today.getFullYear();
				var hh= today.getHours()
				var min=today.getMinutes()
				var sec=today.getSeconds()
				if (dd < 10) {
				  dd = '0' + dd;
				}
	  
				if (hh < 10) {
				  hh = '0' + hh;
				}
				if (mm < 10) {
					mm = '0' + mm;
				  }
				if (min < 10) {
					min = '0' + min;
				  }
				  if (sec < 10) {
					sec = '0' + sec;
				  }
				today = yyyy + "" + mm + "" + dd + "" + hh + "" + min + "" + sec;
				
				ubicacion={LONGITUD:position.coords.longitude,LATITUD:position.coords.latitude,USUARIO:localStorage.getItem('userId'),FECHA:today}
				now=Date.now()
				time = ( now - localStorage.getItem('lTime'))/1000
				console.log('Tiempo transcurrido: ' + parseInt(time)+ ' Segundos')
				if(time > localStorage.getItem('maxTime') || localStorage.getItem('force')=="true"){
					console.log('Ubicacion enviada');
					localStorage.setItem('lTime',now)
					localStorage.setItem('force',"false")
					addPosition(ubicacion)
				}
			  }
		},
		//Si hay error
		(error)=>{
			if (error.code==2) {
				M.toast({html: error.message})
			}
		},
		//Configuraciones
		{
			enableHighAccuracy: false,
			timeout: 30000,
			maximumAge: localStorage.getItem('maxTime')
		  })
}


function obtenerBarrios() {

	var settings = {
		"async": true,
		"crossDomain": true,
		"url": "https://encuesta.plussoluciones.com/WebService/usuario/",
		"method": "POST",
		"headers": {
		"content-type": "application/x-www-form-urlencoded",
	//	"authorization": "Basic cmVnaXN0cm86SW52MHQwKjIwMTg=",
		},
		"data": {
		"OPT":'obtenerBarrios',
		},
		"beforeSend":function(){
			M.toast({html: 'Actualizando barrios...'})
		},"error":function () {
			M.toast({html: 'Se presentó un error al intentar actualizar los barrios.'})
		}
	}

	$.ajax(settings).done(function (response) {
		if (response.estado) {
				addBarrio(response.data)
		}else{
			M.toast({html: 'Se presentó un error al intentar actualizar la contraseña'})
		}
	});
	
}


function registroEncuesta(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; //January is 0!
	var yyyy = today.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	}
	if (mm < 10) {
		mm = '0' + mm;
	}
	today = yyyy +""+ mm +""+ dd ;

	if(localStorage.getItem('fechaEncuesta')!=today){
		localStorage.setItem('fechaEncuesta',today)
		totalDia(false)
    }
}


function limpiar(){
    $("#cmbRsp1").val('')
    $("select").val(0)
	$('input').val('').removeClass('valid');
	M.updateTextFields();
	var instance = M.Tabs.getInstance($(".tabs"));
	instance.select('test1');
	$("#edtDir").val(' ')
	$("#encuestas").hide()
	$("#iniciarEncuesta").show()
}

function getDistancia(lat1,lon1,lat2,lon2){
 rad = function(x) {return x*Math.PI/180;}
	var R = 6378.137; //Radio de la tierra en km
 	var dLat = rad( lat2 - lat1 );
  var dLong = rad( lon2 - lon1 );
  var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var d = (R * c) * 1000;
	return parseInt(d); //Retorna tres decimales
 }


