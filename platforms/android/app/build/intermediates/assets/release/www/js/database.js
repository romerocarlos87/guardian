var conn = window.openDatabase("encuestas", "1.0", "app", 20000);

function crearTablas() {

    conn.transaction(function (tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS persona (PERSONAID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, PERSONANUMERODOCUMENTO varchar(50) NOT NULL, PERSONAPRIMERNOMBRE varchar(30)  NOT NULL, PERSONASEGUNDONOMBRE varchar(30)  NOT NULL, PERSONAPRIMERAPELLIDO varchar(30)  NOT NULL, PERSONASEGUNDOAPELLIDO varchar(30)  NOT NULL,GENEROID smallint(6) NOT NULL, PERSONAEDAD varchar(3) NOT NULL, PERSONADIRECCION varchar(250) NOT NULL, PERSONATIPOUBICACION varchar(50) NOT NULL, PERSONABARRIO varchar(150) NOT NULL, PERSONATELEFONO varchar(20) NOT NULL, PERSONACELULAR varchar(20) NOT NULL, PERSONACORREO varchar(250) NOT NULL, PERSONAESTADOID smallint(6) NOT NULL DEFAULT '1', FECHAENCUESTA bigint(20) NOT NULL, ENCUESTADORID smallint(6) NOT NULL, ENCUESTADOR varchar(150) NOT NULL, QUENECESITA varchar(250) NOT NULL, DESEAOBRA varchar(2) NOT NULL, VOTARIA varchar(2) NOT NULL, PORQUEVOTARIA varchar(250) NOT NULL, PARTICIPARIA varchar(2) NOT NULL, PORQUEPARTICIPARIA varchar(250) NOT NULL, OTROPARTICIPARIA varchar(250) NOT NULL, ORIGENREGISTROID smallint(6) NOT NULL, USUARIOCREACION varchar(50) NOT NULL, MOMENTOCREACION bigint(20) NOT NULL, ACTUALIZACION bigint(20) DEFAULT NULL, LONGITUD double , LATITUD double, CIUDAD varchar(50) NOT NULL,INICIOENCUESTA varchar(50) NOT NULL)");
    });

    conn.transaction(function (tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS ubicaciones (`ID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,`USUARIO` INTEGER NOT NULL,`LONGITUD` double, `LATITUD` double , `FECHA` bigint NOT NULL)");
    });

    conn.transaction(function (tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS barrios (`BARRIOID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,`BARRIONOMBRE` varchar(150) NOT NULL)");
    });

    conn.transaction(function (tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS stats (`ID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,`nosync` INTEGER NOT NULL,`sync` INTEGER NOT NULL,`totaldia` INTEGER NOT NULL)",[],function (tx,results) {
            tx.executeSql("INSERT INTO stats values (1,0,0,0)");
        });
    });


    conn.transaction(function (tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS sesion (`sesionid` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,`usuario` varchar(150) NOT NULL,`usuarioId` varchar(150) NOT NULL)");
    });

}

function addPosition(data) {
    conn.transaction(function (tx) {
            tx.executeSql('INSERT INTO ubicaciones (longitud ,latitud , usuario,fecha ) VALUES ('+data.LONGITUD+','+data.LATITUD+','+data.USUARIO+','+data.FECHA+')', [],function(tx,results){
                if(results.rowsAffected > 0){
                   
                    enviarUbicacion(data,results.insertId)
                }
            });
    });
}

function addSesion(data) {
    conn.transaction(function (tx) {
            tx.executeSql("INSERT INTO sesion (usuario ,usuarioId ) VALUES ('"+data.usuario+"','"+data.usuarioId+"')", [],function(tx,results){
                if(results.rowsAffected > 0){
                    localStorage.setItem('userName',data.usuario);
                    localStorage.setItem('userId',data.usuarioId);
                    location.href ="encuesta.html";
                }
            });
    });
}

function dropSesion() {
    conn.transaction(function (tx) {
            tx.executeSql("delete from sesion", [],function(tx,results){
                console.log(results);
                if(results.rowsAffected > 0){
                    localStorage.clear()
                    window.location.reload();
                }else{
                    window.location.reload();
                }
            });
    });
}

function getSesion() {
    conn.transaction(function (tx) {
        tx.executeSql("SELECT * FROM sesion", [], function (tx, results) {

            if (results.rows.length>0) {
                localStorage.setItem('userName',results.rows.item(0).usuario);
                localStorage.setItem('userId',results.rows.item(0).usuarioId);
                location.href ="encuesta.html";
            }else{
                if (window.location.pathname.split('/').slice(-1)[0]!='login.html') {
                    window.location.href="login.html"
                }
            }
        });
    });
}

function addBarrio(barrios) {
    $.each(barrios,function (i,data) {
        conn.transaction(function (tx) {
            tx.executeSql('INSERT INTO barrios (BARRIOID ,BARRIONOMBRE ) VALUES ('+data.BARRIOID+',\''+data.BARRIONOMBRE+'\')', [],function(tx,results){

            });
        });
       
    })
}

function addEncuesta() {
    conn.transaction(function (tx) {
       fecha=new Date().getFullYear()+''+("0"+(new Date().getMonth()+1)).slice(-2) +""+ ("0"+new Date().getDate()).slice(-2)+""+("0"+new Date().getHours()).slice(-2)+""+("0"+new Date().getMinutes()).slice(-2)+""+("0"+new Date().getSeconds()).slice(-2);
       edad=$("#edtEdad").val()
       if($("#edtEdad").val()==""){
            edad=0
       }
       if($("#cmbTpoUbc").val()=="BARRIO"){
        barrio=$("#edtBarrios").val()
       }else{
        barrio=$("#edtBarrio").val()
       }
       if($("#edtBarrios").val()=='otro'){
        barrio=$("#otro-barrio").val()
       }
       
        datos={
            "PERSONANUMERODOCUMENTO": $("#edtDoc").val(),
            "PERSONAPRIMERNOMBRE":$("#edtNmb1").val(),
            "PERSONASEGUNDONOMBRE":$("#EdtNmb2").val(),
            "PERSONAPRIMERAPELLIDO": $("#edtApl1").val(),
            "PERSONASEGUNDOAPELLIDO": $("#edtApl2").val(),
            "GENEROID": $("#cmbGnr").val(),
            "PERSONAEDAD": edad,
            "PERSONADIRECCION":$("#edtDir").val(),
            "PERSONATIPOUBICACION": $("#cmbTpoUbc").val(),
            "PERSONABARRIO":barrio,
            "PERSONATELEFONO": $("#EdtTel").val(),
            "PERSONACELULAR": $("#edtCel").val(),
            "PERSONACORREO":$("#edtMail").val(),
            "FECHAENCUESTA": fecha,
            "ENCUESTADORID": localStorage.getItem('userId'),
            "QUENECESITA":  M.FormSelect.getInstance($("#cmbRsp1")).getSelectedValues().toString(),
            "DESEAOBRA": $("#cmbRsp2").val(),
            "VOTARIA": $("#cmbRsp3").val(),
            "PORQUEVOTARIA": $("#cmbPqRsp3").val(),
            "PARTICIPARIA": $("#cmbRsp4").val(),
            "PORQUEPARTICIPARIA": $("#cmbPqRsp4").val(),
            "OTROPARTICIPARIA": $("#edtRsp4").val(),
            "ORIGENREGISTROID":1,
            "USUARIOCREACION":localStorage.getItem('userName'),
            "MOMENTOCREACION": fecha,
            "LONGITUD": localStorage.getItem('longitud'),
            "LATITUD": localStorage.getItem('latitud'),
            "ENCUESTADOR":localStorage.getItem('userName'),
            "CIUDAD":$("#cmbCiudad").val(),
            "INICIOENCUESTA":localStorage.getItem('inicioEncuesta')
        }
        
        tx.executeSql('INSERT INTO `persona`(`PERSONANUMERODOCUMENTO`,`PERSONAPRIMERNOMBRE`,`PERSONASEGUNDONOMBRE`,`PERSONAPRIMERAPELLIDO`, `PERSONASEGUNDOAPELLIDO`, `GENEROID`, `PERSONAEDAD`, `PERSONADIRECCION`, `PERSONATIPOUBICACION`, `PERSONABARRIO`, `PERSONATELEFONO`,`PERSONACELULAR`,`PERSONACORREO`,`FECHAENCUESTA`,`ENCUESTADORID`,`QUENECESITA`,`DESEAOBRA`,`VOTARIA`, `PORQUEVOTARIA`,`PARTICIPARIA`, `PORQUEPARTICIPARIA`,`OTROPARTICIPARIA`, `ORIGENREGISTROID`, `USUARIOCREACION`,`MOMENTOCREACION`, `LONGITUD`, `LATITUD`,`ENCUESTADOR`,`CIUDAD`,`INICIOENCUESTA`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', 
        [
            datos.PERSONANUMERODOCUMENTO,
            datos.PERSONAPRIMERNOMBRE,
            datos.PERSONASEGUNDOAPELLIDO,
            datos.PERSONAPRIMERAPELLIDO,
            datos.PERSONASEGUNDOAPELLIDO,
            datos.GENEROID,
            datos.PERSONAEDAD,
            datos.PERSONADIRECCION,
            datos.PERSONATIPOUBICACION,
            datos.PERSONABARRIO,
            datos.PERSONATELEFONO,
            datos.PERSONACELULAR,
            datos.PERSONACORREO,
            datos.FECHAENCUESTA,
            datos.ENCUESTADORID,
            datos.QUENECESITA,
            datos.DESEAOBRA,
            datos.VOTARIA,
            datos.PORQUEVOTARIA,
            datos.PARTICIPARIA,
            datos.PORQUEPARTICIPARIA,
            datos.OTROPARTICIPARIA,
            datos.ORIGENREGISTROID,
            datos.USUARIOCREACION,
            datos.MOMENTOCREACION,
            datos.LONGITUD,
            datos.LATITUD,
            datos.ENCUESTADOR,
            datos.CIUDAD,
            datos.INICIOENCUESTA
        ],function(tx,results){
            if(results.rowsAffected > 0){
                updateStats(false,true)
                totalDia(true)
                enviarEncuesta(datos,results.insertId)
                limpiar()
            }else{
				M.toast({html: 'Se presentó un error al intentar registrar la encuesta'})
            }

        });
    });
}

function totalDia(tipo) {
    if(tipo){
        conn.transaction(function (tx) {
            tx.executeSql("SELECT * FROM stats WHERE id=1", [], function (tx, results) {
                    conn.transaction(function (tx) {
                        num=results.rows.item(0).totaldia + 1;
                        tx.executeSql("UPDATE stats set totaldia=? where id=1", [num]);
                    });
            });
        });
    }else{
        conn.transaction(function (tx) {
            tx.executeSql("SELECT * FROM stats WHERE id=1", [], function (tx, results) {
                    conn.transaction(function (tx) {
                        tx.executeSql("UPDATE stats set totaldia=? where id=1", [0]);
                    });
            });
        });
    }
        
}

//sync {true:'sync',false:'nosync'} actualiza la columna sync o no sync dependiendo del valor de la variable
//tipo {true:'+',false:.'-'} suma o resta detemdiendo el valor de la variable
function updateStats(sync,tipo) {
    if(!sync){
        conn.transaction(function (tx) {
            tx.executeSql("SELECT * FROM stats WHERE id=1", [], function (tx, results) {
                    conn.transaction(function (tx) {
                        num=results.rows.item(0).nosync - 1;
                        if(tipo){
                            num=results.rows.item(0).nosync + 1;
                        }
                        tx.executeSql("UPDATE stats set nosync=? where id=1", [num]);
                    });
            });
        });

    }else{
        conn.transaction(function (tx) {
            tx.executeSql("SELECT * FROM stats WHERE id=1", [], function (tx, results) {
                    conn.transaction(function (tx) {
                        num=results.rows.item(0).sync - 1;
                        if(tipo){
                            num=results.rows.item(0).sync + 1;
                        }
                        tx.executeSql("UPDATE stats set sync=? where id=1", [num]);
                    });
            });
        });
    }
    
}

function getStats(){
    $("#total").html(0);
    conn.transaction(function (tx) {
        tx.executeSql("SELECT * FROM stats WHERE id=1", [], function (tx, results) {
            $("#nosync").html(0);
            $("#total").html(results.rows.item(0).totaldia);
        });
        tx.executeSql("SELECT count(*) nosync FROM persona", [], function (tx, results) {
            if(results.rows.item(0).nosync){
                $("#sincronizar").show()
                $(".progress").show()
            }
            $("#nosync").html(results.rows.item(0).nosync);
        });
       
    });
}

function getBarrios(){
    conn.transaction(function (tx) {
        tx.executeSql("SELECT * FROM barrios order by(BARRIONOMBRE) asc", [], function (tx, results) {
            $("#edtBarrios").html('<option value="0">...</option>')
            $.each(results.rows,function (i,barrio) {
                $("#edtBarrios").append('<option value="'+results.rows.item(i).BARRIONOMBRE+'">'+results.rows.item(i).BARRIONOMBRE+'</option>');
            })
            $("#edtBarrios").append('<option value="otro">Otro</option>')
        });
    });
}

function sincronizarEncuestas(){
    //total no enviadas
	$("#progress").css({width: "0%"})

    localStorage.setItem('tne',0)
    //total enviadas
    localStorage.setItem('te',0)
    // error en el envio (1-0)
    localStorage.setItem('errorSync',0)
    conn.transaction(function (tx) {
        tx.executeSql("SELECT * FROM persona", [], function (tx, results) {
            //Total no enviadas
            localStorage.setItem('tne',results.rows.length)
            
                   
            $.each(results.rows,function(i,element){
                enviarEncuesta(element,element.PERSONAID,true)
               
                getStats()
            });
        });
    });
}


function sincronizarUbicaciones(){
    conn.transaction(function (tx) {
        tx.executeSql("SELECT * FROM ubicaciones", [], function (tx, results) {
            $.each(results.rows,function(i,element){
                enviarUbicacion(element,element.ID)
                getStats()
            });
        });
    });
}


function getAllUbicaciones(){
    conn.transaction(function (tx) {
            tx.executeSql("SELECT * FROM ubicaciones", [], function (tx, results) {
                return results.rows;
            });
        });
        return false;
}


function deleteUbicacion(id){
    conn.transaction(function (tx) {
            tx.executeSql("delete FROM ubicaciones where ID=?", [id], function (tx, results) {
                
            });
        });
}

function deletePersona(id){
    conn.transaction(function (tx) {
            tx.executeSql("delete FROM persona where PERSONAID=?", [id], function (tx, results) {
                console.log(results);
            });
        });
}




function failDB(err) {
    console.log("Error processing SQL: " + err);
}

function successDB() {
    console.log("Se creo la base de datos");
}

function crearDB() {
    conn.transaction(crearTablas, failDB, successDB);
}