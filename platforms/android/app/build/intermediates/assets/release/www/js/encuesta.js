function votaria(){
	if ($('#cmbRsp3').val() == '0'){
        M.toast({html: 'Seleccione si votaria por el candidato!'})
		return false;
    }	
    var rsp = $('#cmbRsp3').val();
	if(rsp == "NO"){
		$("#cmbPqRsp3").prop( "disabled", false );
        $("#cmbPqRsp3")[0].selectedIndex = 0;
        $('#cmbPqRsp3').formSelect();
        $(".cmbPqRsp3").show()
	}else{
        $(".cmbPqRsp3").hide()
		$( "#cmbPqRsp3" ).prop( "disabled", true );
        $('#cmbPqRsp3').val('0');
        $('#cmbPqRsp3').formSelect();
	}
}

function maxLongitud(input){
	
	if($(input).val().length < $(input).prop('maxlength')){
		$(input).addClass('invalid')
	}else{
		$(input).removeClass('invalid')
	}
	if($(input).val().length > $(input).prop('maxlength')){
		$(input).val($(input).val().slice(0,$(input).prop('maxlength')))
		
	}
}

function perteneceria(){
	if ($('#cmbRsp4').val() == '0'){
        M.toast({html: 'Seleccione si perteneceria a la campaña!'})
		return false;
	}	
	var rsp = $('#cmbRsp4').val(); 
	if(rsp == "SI"){
		$("#cmbPqRsp4").prop( "disabled", false );			
        $("#cmbPqRsp4")[0].selectedIndex = 0;	
        $('#cmbPqRsp4').formSelect();
        $(".cmbPqRsp4").show()
	}else{
		$( "#cmbPqRsp4" ).prop( "disabled", true );
        $('#cmbPqRsp4').val('0');
        $('#cmbPqRsp4').formSelect();
        $('#edtRsp4').val('');
        $(".cmbPqRsp4").hide()
	}
}

function otro_perteneceria(){
	if ($('#cmbPqRsp4').val() == '0'){
        M.toast({html: 'Seleccione porque perteneceria!'})
		return false;
	}	
	var rsp = $('#cmbPqRsp4').val(); 
	if(rsp == "Otro"){
        $("#edtRsp4").prop( "disabled", false );
        $('.edtRsp4').show()
	}else{
		$("#edtRsp4").prop( "disabled", true );
        $('#edtRsp4').val('');
        $('.edtRsp4').hide()
	}
}

function validarNum(e) {
	tecla = (document.all) ? e.keyCode : e.which;
	if (tecla == 8 || tecla == 0) return true;
	patron = /(^[0-9]$)/;
	te = String.fromCharCode(tecla);
	return patron.test(te);
}


function validar_tel(){
	var tel = $('#EdtTel').val();
	var pos = tel.substring(0, 1);
	if (pos != 3){
        M.toast({html: 'Debe comenzar por 3!'})
		$('#EdtTel').val('');
		return false;
	}
}

function validar_cel(){
	var tel = $('#edtCel').val();
	var pos = tel.substring(0, 1);
	if (pos != 3){
        M.toast({html: 'Debe comenzar por 3!'})
		$('#edtCel').val('');
		return false;
	}
}

function validarPestanaEncuesta(){

	if ($('#cmbRsp1').val() == '0'){
        M.toast({html: 'Seleccione que necesita su barrio !'})
		return false;
	}

	if ($('#cmbRsp2').val() == '0'){
        M.toast({html:  'Seleccione si desea obras  !'})
		return false;
	}

	if ($('#cmbRsp3').val() == '0'){
        M.toast({html:  'Seleccione si votaria por el candidato !'})
		return false;
	}

	if ($('#cmbRsp3').val() == 'NO'){
		if ($('#cmbPqRsp3').val() == '0'){
            M.toast({html:  'Seleccione por que no votaria!'})
			return false;
		}
	}	

	if ($('#cmbRsp4').val() == '0'){
        M.toast({html:  'Seleccione si le gustaría pertencer a la campaña !'})
		return false;
	}	

	if ($('#cmbRsp4').val() == 'SI'){

		if ($('#cmbPqRsp4').val() == '0'){
            M.toast({html:  'Seleccione por que gustaría pertencer!'})
			return false;
		}		

		if ($('#cmbPqRsp4').val() == 'Otro'){

			if ($('#edtRsp4').val() == ''){
                M.toast({html:  'Escriba el otro motivo!'})
				return false;
			}	
		}			
	}		

	if ($('#edtNmb1').val() == ''){
        M.toast({html: 'Escriba el primer nombre!'})
		return false;
	}
	if ($('#edtApl1').val() == ''){
        M.toast({html: 'Escriba el primer apellido!'})
		return false;
	}

	if ($('#cmbGnr').val() == '0'){
        M.toast({html: 'Selecione un genero!'})
		return false;
	}

	if($("#cmbCiudad").val()=='' || $("#cmbCiudad").val()==null){
		M.toast({html:  'Seleccione una ciudad!'})
		return false;
	}

	if($("#barrio").val()==""){
		M.toast({html:  'Seleccione un barrio!'})
		return false;
	}

	if($("#EdtTel").val()=="" && $("#edtCel").val()==""){
		M.toast({html:  'Escriba un telefono o un celular!'});
		return false;
	} 

	if ($("#EdtTel").val().length>0 && $("#EdtTel").val().length < $("#EdtTel").prop('maxlength')) {
		M.toast({html:  'Escriba un telefono  valido!'});
		return false;
	}

	if ($("#edtCel").val().length>0 && $("#edtCel").val().length < $("#edtCel").prop('maxlength')) {
		M.toast({html:  'Escriba un celular valido!'});
		return false;	
	}
	if ($("#edtMail").val()!='') {
			var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			if(!regex.test($("#edtMail").val())){
				M.toast({html:  'Escriba un correo valido!'});
				return false;
			}
			/*if($("#edtMail").val().substr($("#edtMail").val().length-3)!='.co' && $("#edtMail").val().substr($("#edtMail").val().length-4)!='.com'){
				M.toast({html:  'Escriba un correo valido!'});
				return false;
			}*/
	}
	
	return true;
}