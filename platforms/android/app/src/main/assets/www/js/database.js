var conn = window.openDatabase(
  "encuestas",
  "1.0",
  "encuestas guardian",
  200000
);
function errorHandler(transaction, error) {
  console.error("Error : " + error.message);
}
function crearTablas() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "CREATE TABLE IF NOT EXISTS `fichaguardian` (`FICHAGUARDIANID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,`NUMERO` varchar(10) NOT NULL,        `CIUDADID` INTEGER NOT NULL,`CABECERA` varchar(50) NOT NULL,`CORREGIMIENTO` varchar(150) NOT NULL,`VEREDA` varchar(150) NOT NULL, `MANZANA` varchar(50) NOT NULL,`VIVIENDA` varchar(100) NOT NULL,`FAMILIA` varchar(50) NOT NULL,`BARRIO` varchar(250) NOT NULL,`DIRECCION` varchar(250) NOT NULL,`TELEFONO` varchar(150) NOT NULL,`EPSID` int(11) NOT NULL,`USUARIOCREACION` varchar(50) ,`MOMENTOCREACION` INTEGER NOT NULL,`ESTADOREGISTRO` smallint ,LONGITUD double , LATITUD double,`UUID` VARCHAR(150) NOT NULL,INICIOCAPTURA INTEGER,DBSERVERID INTEGER UNIQUE,SEGUIMIENTO INTEGER(1))",
      [],
      () => {
        console.log("tabla fichaguardian creada");
      },
      errorHandler
    );
  });
  conn.transaction(function(tx) {
    tx.executeSql(
      "CREATE TABLE IF NOT EXISTS `ESCOLAR` (`ESCOLARID`  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,PERSONAID int(11) NOT NULL, `ANNO` int(11) NOT NULL,  `ULTIMOANO` int(11) NOT NULL,  `NOMBREESTABLECIMIENTO` varchar(150) NOT NULL,  `APROBADO` varchar(2) NOT NULL,  `HAREPETIDO` varchar(2) NOT NULL,  `CUALGRADO` int(11) NOT NULL,  `PROBLEMASCONVIVENCIA` varchar(2) NOT NULL,  `DIFICULTADSISTEMAEDUCATIVO` int(11) NOT NULL,  `DESEAINTEGRARALSISTEMAEDUCATIVO` varchar(2) NOT NULL,  `RAZONPARANOINTEGRAR` int(11) NOT NULL,  `ESTABLECIMIENTOCERCA` varchar(2) NOT NULL,  `RAZONESPARANOELEGIRESTABLECIMIENTO` varchar(300) NOT NULL,  `ESTABLECIMIENTOPARAHIJO` varchar(150) NOT NULL,  `HAINTENTADOMATRICULARANTES` varchar(2) NOT NULL,  `PORQUENOMATRICULO` varchar(300) NOT NULL,USUARIOCREACION VARCHAR(20),UUID VARCHAR(150),DBSERVERID INTEGER UNIQUE,`MOMENTOCREACION` INTEGER NOT NULL,`ESTADOREGISTRO` smallint )",
      [],
      () => {
        console.log("tabla ESCOLAR creada");
      },
      errorHandler
    );
  });


  conn.transaction(function(tx) {
    tx.executeSql(
      "CREATE TABLE IF NOT EXISTS `eps` (`EPSID` integer INTEGER PRIMARY KEY NOT NULL,`EPSDESCRIPCION` varchar(150) NOT NULL)",
      [],
      () => {
        console.log("tabla Eps creada");
      },
      errorHandler
    );
  });

  conn.transaction(function(tx) {
    //        tx.executeSql("CREATE TABLE IF NOT EXISTS barrios (`BARRIOID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,`BARRIONOMBRE` varchar(150) NOT NULL)");
  });

  conn.transaction(function(tx) {
    tx.executeSql(
      "CREATE TABLE IF NOT EXISTS municipios (`MUNICIPIOID` INTEGER PRIMARY KEY NOT NULL,`MUNICIPIONOMBRE` varchar(150) NOT NULL)",
      [],
      () => {
        console.log("tabla municipios creada");
      },
      errorHandler
    );
  });


  conn.transaction(function(tx) {
    tx.executeSql(
      "CREATE TABLE IF NOT EXISTS sesion (`sesionid` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,`usuario` varchar(150) NOT NULL,`usuarioId` varchar(150) NOT NULL)"
    );
  });
  conn.transaction(function(tx) {
    tx.executeSql(
      "CREATE TABLE IF NOT EXISTS ubicaciones (`ID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,`USUARIO` INTEGER ,`LONGITUD` double, `LATITUD` double , `FECHA` bigint NOT NULL)",
      [],
      () => {
        console.log("tabla Ubicaciones creada");
      },
      errorHandler
    );
  });
  conn.transaction(function(tx) {
    tx.executeSql(
      "CREATE TABLE IF NOT EXISTS tipomiembro (TIPOMIEMBROID integer PRIMARY KEY NOT NULL,TIPOMIEMBRODESCRIPCION varchar(50) NOT NULL)",
      [],
      () => {
        console.log("tabla tipomiembro creada");
      },
      errorHandler
    );
  });
  conn.transaction(function(tx) {
    tx.executeSql(
      "CREATE TABLE IF NOT EXISTS  tipodocumento (`TIPODOCUMENTOID` INTEGER PRIMARY KEY NOT NULL,`TIPODOCUMENTODESCRIPCION` varchar(50) NOT NULL)",
      [],
      () => {
        console.log("tabla tipodocumento creada");
      },
      errorHandler
    );
  });
  conn.transaction(function(tx) {
    tx.executeSql(
      "CREATE TABLE IF NOT EXISTS `persona` (`PERSONAID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,`FICHAGUARDIANID` bigint(20) NOT NULL,`TIPOMIEMBROID` int(11) ,`TIPODOCUMENTOID` smallint(6) NOT NULL,`NUMERODOCUMENTO` varchar(50) NOT NULL,`NOMBRE` varchar(100) NOT NULL,`GENEROID` smallint(6) NOT NULL,`ESTADOCIVILID` smallint(6) NOT NULL,`FECHANACIMIENTO` bigint(20) ,`NIVELDEESTUDIOID` smallint(6) ,`OCUPACIONID` int(11),`TIPOSALUDID` int(11) ,`TIPOAFILIACIONID` int(11),`EPSID` int(20) ,`EPSOTRO` varchar(50) ,`DISCAPACIDAD` varchar(2) ,`DISCAPACIDADCUAL` varchar(50) ,`ETNIAID` int(11) ,`GRUPOPOBLACIONALID` int(11) ,`GRUPOORGANIZADO` varchar(2) ,`GESTACION` varchar(2) NOT NULL,`USUARIOCREACION` varchar(50) ,`MOMENTOCREACION` bigint(20) NOT NULL,`ESTADOREGISTRO` int(11) NOT NULL,`UUID` VARCHAR(150) NOT NULL,DBSERVERID integer UNIQUE,F_PERTENECE integer,PRESTAMO VARCHAR(2),PRESTAMOVALORID INTEGER, SECTORNEGOCIOID INTEGER)",
      [],
      () => {
        console.log("tabla persona creada");
      },
      errorHandler
    );
  });

  conn.transaction(function(tx) {
    tx.executeSql(
      "CREATE TABLE IF NOT EXISTS `saludocupacional` (`SALUDOCUPACIONALID` integer PRIMARY KEY AUTOINCREMENT NOT NULL,`FICHAGUARDIAN` bigint(20) NOT NULL,`TIPOFAMILIAID` int(11) NOT NULL,`TIPOCORRECCIONID` int(11) NOT NULL,`CIGARRILLO` varchar(10) NOT NULL,`ALCOHOL` varchar(10) NOT NULL,`PASTILLAS` varchar(10) NOT NULL,`PSA` varchar(20) NOT NULL,`COMPARTEN` varchar(10) NOT NULL,`RELACIONES` varchar(10) NOT NULL,`USUARIOCREACION` varchar(50) ,`MOMENTOCREACION` bigint(20) NOT NULL,`ESTADOREGISTRO` int(11) NOT NULL,`UUID` varchar(150) NOT NULL,`DBSERVERID` integer UNIQUE)",
        [],() => {
        console.log("tabla saludocupacional creada");
      },
      errorHandler
    );
  });
  conn.transaction(function(tx) {
    tx.executeSql(
      "CREATE TABLE IF NOT EXISTS `vivienda` (`VIVIENDAID` integer PRIMARY KEY AUTOINCREMENT NOT NULL ,`FICHAGUARDIANID` bigint(20) NOT NULL,`TIPOVIVIENDAID` int(11) NOT NULL,`TIPOPISOID` int(11) NOT NULL,`TIPOPAREDID` int(11) NOT NULL,`TIPOTECHOID` int(11) NOT NULL ,`ILUMINACION` varchar(2) NOT NULL,`VENTILACION` varchar(2) NOT NULL,`INGRESO` varchar(2) NOT NULL,`INGRESOCUAL` varchar(150) NOT NULL,`PERSONADORMITORIO` int(11) NOT NULL,`ARROYO` varchar(2) NOT NULL,`BASURERO` varchar(2) NOT NULL,`HUMO` varchar(2) NOT NULL,`FUENTEAGUAID` int(11) NOT NULL,`FUENTEAGUAOTRO` varchar(100) NOT NULL,`AGUATRATADA` varchar(2) NOT NULL,`TRATAMIENTO` varchar(80) NOT NULL,`DEPOSITOAGUAID` int(11) NOT NULL,`DEPOSITOAGUACUAL` varchar(50) NOT NULL,`DEPOSITOAGUAOTRO` varchar(100) NOT NULL,`LARVAS` varchar(2) NOT NULL,`ROEDORES` varchar(2) NOT NULL,`TIPOALUMBRADOID` int(11) NOT NULL,`TIPOALUMBRADOOTRO` varchar(100) NOT NULL,`SERVICIOSANITARIOID` int(11) NOT NULL,`MANEJOBASURA` varchar(2) NOT NULL,`USUARIOCREACION` varchar(50),`MOMENTOCREACION` bigint(20) NOT NULL,`ESTADOREGISTRO` int(11) NOT NULL,UUID varchar(150),DBSERVERID integer UNIQUE)",
        [],() => {
        console.log("tabla vivienda creada");
      },
      errorHandler
    );
  });
  conn.transaction(function(tx) {
    tx.executeSql(
        "CREATE TABLE IF NOT EXISTS `mascota` (`MASCOTAID` integer PRIMARY KEY AUTOINCREMENT NOT NULL,`FICHAGUARDIANID` bigint(20) NOT NULL,`TIPOMASCOTAID` int(11) NOT NULL,`TIPOMASCOTAOTRO` varchar(50),`VACUANADA` varchar(2) NOT NULL,`USUARIOCREACION` varchar(50) ,`MOMENTOCREACION` bigint(20) NOT NULL,`ESTADOREGISTRO` int(11) NOT NULL,UUID varchar(150),DBSERVERID integer UNIQUE)",
        [],() => {
        console.log("tabla mascota creada");
      },
      errorHandler
    );
  });
 
  conn.transaction(function(tx) {
    tx.executeSql(
        "CREATE TABLE IF NOT EXISTS `tipomascota` (`TIPOMASCOTAID` INTEGER  PRIMARY KEY  NOT NULL,`TIPOMASCOTADESCRIPCION` varchar(50) NOT NULL)",
        [],() => {
        console.log("tabla tipomascota creada");
      },
      errorHandler
    );
  });

  conn.transaction(function(tx) {
    tx.executeSql(
        "CREATE TABLE IF NOT EXISTS `niveldeestudio` (  `NIVELDEESTUDIOID`  INTEGER  PRIMARY KEY  NOT NULL,  `NIVELDEESTUDIODESCRIPCION` varchar(150) NOT NULL);",
        [],() => {
        console.log("tabla niveldeestudio creada");
      },
      errorHandler
    );
  });
  
  conn.transaction(function(tx) {
    tx.executeSql(
        "CREATE TABLE IF NOT EXISTS `ocupacion` (`OCUPACIONID`  INTEGER  PRIMARY KEY  NOT NULL , `OCUPACIONDESCRIPCION` varchar(50) NOT NULL)",
        [],() => {
        console.log("tabla ocupacion creada");
      },
      errorHandler
    );
  });

  conn.transaction(function(tx) {
    tx.executeSql(
        "CREATE TABLE IF NOT EXISTS `estadocivil` (`ESTADOCIVILID`  INTEGER  PRIMARY KEY  NOT NULL, `ESTADOCIVILDESCRIPCION` varchar(50) NOT NULL);",
        [],() => {
        console.log("tabla estadocivil creada");
      },
      errorHandler
    );
  });
  conn.transaction(function(tx) {
    tx.executeSql(
        "CREATE TABLE IF NOT EXISTS `tipovivienda` (`TIPOVIVIENDAID`  INTEGER  PRIMARY KEY  NOT NULL,`TIPOVIVIENDADESCRIPCION` varchar(100) NOT NULL);",
        [],() => {
        console.log("tabla tipovivienda creada");
      },
      errorHandler
    );
  });

  conn.transaction(function(tx) {
    tx.executeSql(
        "CREATE TABLE IF NOT EXISTS `tipopiso` (`TIPOPISOID`  INTEGER  PRIMARY KEY  NOT NULL,`TIPOPISODESCRIPCION` varchar(100) NOT NULL);",
        [],() => {
        console.log("tabla tipopiso creada");
      },
      errorHandler
    );
  });

  conn.transaction(function(tx) {
    tx.executeSql(
        "CREATE TABLE IF NOT EXISTS `fuenteagua` (`FUENTEAGUAID`  INTEGER  PRIMARY KEY  NOT NULL,`FUENTEAGUADECRIPCION` varchar(100) NOT NULL);",
        [],() => {
        console.log("tabla fuenteagua creada");
      },
      errorHandler
    );
  });

  conn.transaction(function(tx) {
    tx.executeSql(
        "CREATE TABLE IF NOT EXISTS `tipoalumbrado` (`TIPOALUMBRADOID`  INTEGER  PRIMARY KEY  NOT NULL,`TIPOALUMBRADODESCRIPCION` varchar(100) NOT NULL);",
        [],() => {
        console.log("tabla tipoalumbrado creada");
      },
      errorHandler
    );
  });

  conn.transaction(function(tx) {
    tx.executeSql(
        "CREATE TABLE IF NOT EXISTS `tipopared` (`TIPOPAREDID`  INTEGER  PRIMARY KEY  NOT NULL,  `TIPOPAREDDESCRIPCION` varchar(100) NOT NULL);",
        [],() => {
        console.log("tabla tipopared creada");
      },
      errorHandler
    );
  });

  conn.transaction(function(tx) {
    tx.executeSql(
        "CREATE TABLE IF NOT EXISTS `serviciosanitario` (`SERVICIOSANITARIOID`  INTEGER  PRIMARY KEY  NOT NULL,`SERVICIOSANITARIODESCRIPCION` varchar(100) NOT NULL);",
        [],() => {
        console.log("tabla serviciosanitario creada");
      },
      errorHandler
    );
  });

  conn.transaction(function(tx) {
    tx.executeSql(
        "CREATE TABLE IF NOT EXISTS `tipofamilia` (`TIPOFAMILIAID`  INTEGER  PRIMARY KEY  NOT NULL,`TIPOFAMILIADESCRIPCION` varchar(100) NOT NULL);",
        [],() => {
        console.log("tabla tipofamilia creada");
      },
      errorHandler
    );
  });

  conn.transaction(function(tx) {
    tx.executeSql(
        "CREATE TABLE IF NOT EXISTS `tipocorreccion` (`TIPOCORRECCIONID`  INTEGER  PRIMARY KEY NOT NULL,`TIPOCORRECCIONDESCRIPCION` varchar(100) NOT NULL);",
        [],() => {
        console.log("tabla tipocorreccion creada");
      },
      errorHandler
    );
  });


  conn.transaction(function(tx) {
    tx.executeSql(
        "CREATE TABLE IF NOT EXISTS `personaprimerainfancia` (`PERSONAPRIMERAINFANCIAID` integer PRIMARY KEY AUTOINCREMENT NOT NULL,`PERSONAID` bigint(20) NOT NULL,`CONTROLCYD` varchar(2) NOT NULL,`LACTANCIA` varchar(2) NOT NULL,`LACTANCIATIEMPO` int(11) NOT NULL,`N_PESONACER` int(11) DEFAULT NULL,`N_PESOACTUAL` int(11) NOT NULL,`N_TALLANACER` int(11) DEFAULT NULL,`N_TALLAACTUAL` int(11) NOT NULL,`V_CARNE` varchar(2) NOT NULL,`V_BCG` varchar(2) DEFAULT NULL,`V_HEPB` varchar(2) DEFAULT NULL,`V_POLIO` int(11) DEFAULT NULL,`V_PENTAVALENTE` int(11) DEFAULT NULL,`V_ROTAVIRUS` int(11) DEFAULT NULL,`V_NEUMOCOCO` int(11) DEFAULT NULL,`V_INFLUENZA` int(11) DEFAULT NULL,`ESQUEMA` varchar(2) NOT NULL,`MALTRATO` varchar(2) NOT NULL,`USUARIOCREACION` varchar(50),`MOMENTOCREACION` bigint(20) NOT NULL,`ESTADOREGISTRO` int(11) NOT NULL,`UUID` varchar(150),DBSERVERID integer UNIQUE,ODONTOLOGO varchar(2),CARIOGENICOS varchar(20))",
        [],() => {
        console.log("tabla personaprimerainfancia creada");
      },
      errorHandler
    );
  });
  
  conn.transaction(function(tx) {
    tx.executeSql(
        "CREATE TABLE IF NOT EXISTS `personasegundainfancia` (`PERSONASEGUNDAINFANCIAID` integer PRIMARY KEY AUTOINCREMENT NOT NULL ,`PERSONAID` bigint(20) NOT NULL,`CONTROLCYD` varchar(2) NOT NULL,`N_PESOACTUAL` int(11) NOT NULL,`N_TALLAACTUAL` int(11) NOT NULL,`S_FACTORCARIOGENICOID` int(11) NOT NULL,`S_NUMEROCEPILLADA` int(11) NOT NULL,`S_CONSULTAODONTOLOGO` varchar(2) NOT NULL,`V_CARNE` varchar(2) NOT NULL,`V_VPH` varchar(2) DEFAULT NULL,`V_VPHDOSIS` int(11) DEFAULT NULL,`V_TT` varchar(2) DEFAULT NULL,`V_ESQUEMA` varchar(2) NOT NULL,`MALTRATO` varchar(2) NOT NULL,`NUMERODESPARASITADA` int(11) DEFAULT NULL,`USUARIOCREACION` varchar(50) ,`MOMENTOCREACION` bigint(20) NOT NULL,`ESTADOREGISTRO` int(11) NOT NULL,`UUID` varchar(150),DBSERVERID integer UNIQUE,ESCOLARIZADO varchar(2));",
        [],() => {
        console.log("tabla personasegundainfancia creada");
      },
      errorHandler
    );
  });
  
  conn.transaction(function(tx) {
    tx.executeSql(
        "CREATE TABLE IF NOT EXISTS `personaadolescencia` (`PERSONAADOLESCENCIAID` integer PRIMARY KEY AUTOINCREMENT NOT NULL,`PERSONAID` bigint(20) NOT NULL,`P_PLANIFICA` varchar(2) NOT NULL,`P_METODO` varchar(50) NOT NULL,`P_MOTIVO` varchar(50) NOT NULL,`EXAMENMEDICO` varchar(2) NOT NULL,`CITOLOGIA` varchar(2) NOT NULL,`CITOLOGIADETALLE` varchar(10) NOT NULL,`H_TOTALVIVO` int(11) NOT NULL,`H_TOTALABORTADO` int(11) NOT NULL,`TOS` varchar(2) NOT NULL,`LESIONES` varchar(2) NOT NULL,`V_TD` varchar(2) NOT NULL,`V_TDCUAL` varchar(2) DEFAULT NULL,`V_VPH` varchar(2) DEFAULT NULL,`SENO` varchar(2) DEFAULT NULL,`SENOCUAL` varchar(50) DEFAULT NULL,`VIOLENCIA` varchar(2) NOT NULL,`USUARIOCREACION` varchar(50),`MOMENTOCREACION` bigint(20) NOT NULL,`ESTADOREGISTRO` int(11) NOT NULL,`UUID` varchar(150),DBSERVERID integer UNIQUE)",
        [],() => {
        console.log("tabla personaadolescencia creada");
      },
      errorHandler
    );
  });

    conn.transaction(function(tx) {
    tx.executeSql(
        "CREATE TABLE IF NOT EXISTS `personaadultez` (`PERSONAADULTEZID`  integer PRIMARY KEY AUTOINCREMENT NOT NULL,`PERSONAID` bigint(20) NOT NULL,`N_PESOACTUAL` int(11) NOT NULL,`N_TALLAACTUAL` int(11) NOT NULL,`N_CINTURA` int(11) NOT NULL,`FUMA` varchar(2) NOT NULL,`TOMA` varchar(2) NOT NULL,`EJERCICIO` varchar(2) NOT NULL,`TOS` varchar(2) NOT NULL,`LESIONES` varchar(2) NOT NULL,`SENO` varchar(2) NOT NULL,`SENODETALLE` varchar(50) NOT NULL,`CITOLOGIA` varchar(2) NOT NULL,`CITOLOGIADETALLE` varchar(50) NOT NULL,`PROSTATA` varchar(2) NOT NULL,`PROSTATADETALLE` varchar(50) NOT NULL,`VISION` varchar(2) NOT NULL,`VISIONDETALLE` varchar(50) NOT NULL,`E_HIPERTENSION` varchar(2) NOT NULL,`E_HIPERTENSIONCONTROL` varchar(2) NOT NULL,`E_DIABETES` varchar(2) NOT NULL,`E_DIABETESCONTROL` varchar(2) NOT NULL,`E_EPOC` varchar(2) NOT NULL,`E_EPOCCONTROL` varchar(2) NOT NULL,`E_CANCER` varchar(2) NOT NULL,`E_CANCERDETALLE` varchar(50) NOT NULL,`E_CANCERCONTROL` varchar(2) NOT NULL,`USUARIOCREACION` varchar(50),`MOMENTOCREACION` bigint(20) NOT NULL,`ESTADOREGISTRO` int(11) NOT NULL,`UUID` varchar(150) NOT NULL,DBSERVERID integer UNIQUE)",
        [],() => {
        console.log("tabla personaadultez creada");
      },
      errorHandler
    );
  });
    conn.transaction(function(tx) {
      tx.executeSql(
          "CREATE TABLE IF NOT EXISTS `imagenes` (`IMAGENID`  integer PRIMARY KEY AUTOINCREMENT NOT NULL,`IMAGENRUTA` varchar(2000) NOT NULL,`USUARIO` varchar(30) , `UBICACIONUUID` varchar(150) NOT NULL,FECHA integer,DBSERVERID integer)",
          [],() => {
          console.log("tabla imagenes creada");
        },
        errorHandler
      );
  });

  conn.transaction(function(tx) {
    tx.executeSql(
        "  CREATE TABLE IF NOT EXISTS `tipodiscapacidad` (`TIPODISCACACIDADID`  INTEGER  PRIMARY KEY  NOT NULL ,`TIPODISCAPACIDADNOMBRE` varchar(50) NOT NULL,`TIPODISCAPACIDADDESCRICPION` varchar(200) NOT NULL,`TIPODISCACACIDADABREVIATURA` varchar(3) NOT NULL)",
        [],() => {
        console.log("tabla tipodiscapacidad creada");
      },
      errorHandler
    );
});

conn.transaction(function(tx) {
  tx.executeSql(
      "CREATE TABLE IF NOT EXISTS `etnia` (`ETNIAID`  INTEGER  PRIMARY KEY  NOT NULL,`ETNIADESCRIPCION` varchar(50) NOT NULL)",
      [],() => {
      console.log("tabla etnia creada");
    },
    errorHandler
  );
});

conn.transaction(function(tx) {
  tx.executeSql(
      "CREATE TABLE IF NOT EXISTS `grupopoblacional` (`GRUPOPOBLACIONALID`  INTEGER  PRIMARY KEY  NOT NULL, `GRUPOPOBLACIONALDESCRIPCION` varchar(50) NOT NULL)",
      [],() => {
      console.log("tabla grupopoblacional creada");
    },
    errorHandler
  );
});

conn.transaction(function(tx) {
  tx.executeSql(
      "CREATE TABLE IF NOT EXISTS `gestacion` (`GESTACIONID` integer PRIMARY KEY AUTOINCREMENT NOT NULL,`PERSONAID` bigint(20) NOT NULL,`P_CONTROL` varchar(2) NOT NULL,`P_CARNE` varchar(2) NOT NULL,`RIESGO` varchar(4) NOT NULL,`FUM` bigint(20) NOT NULL,`FPP` bigint(20) NOT NULL,`SEMANAS` int(11) NOT NULL,`A_OBSTRETICO` varchar(2) NOT NULL,`A_PARTO` int(2) NOT NULL,`A_CESAREA` int(2) NOT NULL,`A_ABORTO` int(2) NOT NULL,`CONTROLES` int(11) NOT NULL,`P_GESTANTE` int(11) NOT NULL,`VIH` varchar(2) NOT NULL,`VIHDETALLE` varchar(4) NOT NULL,`VDRL` varchar(2) NOT NULL,`VDRLDETALLE` varchar(4) NOT NULL,`ODONTOLOGIA` varchar(2) NOT NULL,`TDTT` int(11) NOT NULL,`MULTIVITAMINICO` varchar(2) NOT NULL,`P_TIPO` varchar(10) NOT NULL,`P_ATENCION` varchar(2) NOT NULL,`POSPARTO` varchar(2) NOT NULL,`M_GEST` varchar(2) NOT NULL,`M_GESTCUAL` varchar(50) NOT NULL,`M_PARTO` varchar(2) NOT NULL,`M_PARTOCUAL` varchar(50) NOT NULL,`M_PUERP` varchar(2) NOT NULL,`M_PUERPCUAL` varchar(50) NOT NULL,USUARIOCREACION varchar(30) ,`UUID` varchar(150),DBSERVERID integer UNIQUE)",
      [],() => {
      console.log("tabla gestacion creada");
    },
    errorHandler
  );
});

conn.transaction(function(tx) {
  tx.executeSql(
      "CREATE TABLE IF NOT EXISTS `grupoorganizado` (`GRUPOORGANIZADOID`  INTEGER  PRIMARY KEY  NOT NULL ,`GRUPODESCRIPCION` varchar(50) NOT NULL)",
      [],() => {
      console.log("tabla grupoorganizado creada");
    },
    errorHandler
  );
});
restoreDB()
}

function addEscolar(data) {

  conn.transaction(function(tx) {
    tx.executeSql("INSERT INTO `ESCOLAR`(`PERSONAID`, `ANNO`, `ULTIMOANO`, `NOMBREESTABLECIMIENTO`, `APROBADO`, `HAREPETIDO`, `CUALGRADO`, `PROBLEMASCONVIVENCIA`, `DIFICULTADSISTEMAEDUCATIVO`, `DESEAINTEGRARALSISTEMAEDUCATIVO`, `RAZONPARANOINTEGRAR`, `ESTABLECIMIENTOCERCA`, `RAZONESPARANOELEGIRESTABLECIMIENTO`, `ESTABLECIMIENTOPARAHIJO`, `HAINTENTADOMATRICULARANTES`, `PORQUENOMATRICULO`, `USUARIOCREACION`, `UUID`, `MOMENTOCREACION`, `ESTADOREGISTRO`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);", 
    [
      data.PERSONAID,
      data.ANNO,
      data.ULTIMOANO,
      data.NOMBREESTABLECIMIENTO,
      data.APROBADO,
      data.HAREPETIDO,
      data.CUALGRADO,
      data.PROBLEMASCONVIVENCIA,
      data.DIFICULTADSISTEMAEDUCATIVO,
      data.DESEAINTEGRARALSISTEMAEDUCATIVO,
      data.RAZONPARANOINTEGRAR,
      data.ESTABLECIMIENTOCERCA,
      data.RAZONESPARANOELEGIRESTABLECIMIENTO,
      data.ESTABLECIMIENTOPARAHIJO,
      data.HAINTENTADOMATRICULARANTES,
      data.PORQUENOMATRICULO,
      data.USUARIOCREACION,
      data.UUID,
      data.MOMENTOCREACION,
      data.ESTADOREGISTRO,
    ], function(tx, results) {
      console.log
      M.toast({
        html: "Registro almacenado"
      });
    },(error,error2)=>{
      console.log(error);
      console.log(error2);
      
      M.toast({
        html: "Error al almacenar el registro"
      });
    });
  });
}


function addImagen(data) {

  conn.transaction(function(tx) {
    tx.executeSql("INSERT INTO `imagenes`(`IMAGENRUTA`,`UBICACIONUUID`,`USUARIO`,`FECHA`) VALUES (?,?,?,?);", 
    [
      data.IMAGENRUTA,
      data.UUID,
      data.USUARIO,
      data.FECHA
    ], function(tx, results) {
      M.toast({
        html: "Fotografía tomada"
      });
    },()=>{
      M.toast({
        html: "Error al almacenar la Fotografía"
      });
    });
  });
}



function eliminarImagenesSincronizadas(){

  conn.transaction(function(tx) {
    tx.executeSql("select substr(IMAGENRUTA,-17) IMAGENRUTA from imagenes where DBSERVERID is not null", 
    [], function(tx, results) {
      
        for (let index = 0; index < results.rows.length; index++) {
       
        window.resolveLocalFileSystemURL(cordova.file.externalCacheDirectory, function (fileSystem) {
          
              fileSystem.getFile(results.rows[index].IMAGENRUTA, {create:false}, function(fileEntry){
                fileEntry.remove(function(){
                  console.log("File removed!");
                },function(){
                  console.log("error deleting the file ");
                  });
                },function(e){
                  console.log(e);
                });
              },function(evt){
                console.log(evt.target.error.code);
            });
    };
  });
})
}
function addPersonaPrimeraInfanciaLocal(data) {

  conn.transaction(function(tx) {
    tx.executeSql("INSERT INTO `personaprimerainfancia`(`PERSONAID`,`CONTROLCYD`, `LACTANCIA`, `LACTANCIATIEMPO`, `N_PESONACER`, `N_PESOACTUAL`, `N_TALLANACER`, `N_TALLAACTUAL`, `V_CARNE`, `V_BCG`, `V_HEPB`, `V_POLIO`, `V_PENTAVALENTE`,`V_ROTAVIRUS`, `V_NEUMOCOCO`, `V_INFLUENZA`, `ESQUEMA`, `MALTRATO`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`,`UUID`,`CARIOGENICOS`,`ODONTOLOGO`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);", 
    [
      data.PERSONAID,
      data.CONTROLCYD,
      data.LACTANCIA,
      data.LACTANCIATIEMPO,
      data.N_PESONACER,
      data.N_PESOACTUAL,
      data.N_TALLANACER,
      data.N_TALLAACTUAL,
      data.V_CARNE,
      data.V_BCG,
      data.V_HEPB,
      data.V_POLIO,
      data.V_PENTAVALENTE,
      data.V_ROTAVIRUS,
      data.V_NEUMOCOCO,
      data.V_INFLUENZA,
      data.ESQUEMA,
      data.MALTRATO,
      data.USUARIOCREACION,
      data.MOMENTOCREACION,
      data.ESTADOREGISTRO,
      data.UUID,
      data.CARIOGENICOS,
      data.ODONTOLOGO
    ], function(tx, results) {
      M.toast({
        html: "Datos registrados"
      });
    },errorHandler);
  });
}

function addPersonaSegundaInfanciaLocal(data) {

  conn.transaction(function(tx) {
    tx.executeSql("INSERT INTO `personasegundainfancia`( `PERSONAID`, `CONTROLCYD`, `N_PESOACTUAL`, `N_TALLAACTUAL`, `S_FACTORCARIOGENICOID`, `S_NUMEROCEPILLADA`, `S_CONSULTAODONTOLOGO`, `V_CARNE`,`V_VPH`, `V_VPHDOSIS`, `V_TT`, `V_ESQUEMA`, `MALTRATO`, `NUMERODESPARASITADA`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`,`UUID`,`ESCOLARIZADO`) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", 
    [
      data.PERSONAID,
      data.CONTROLCYD,
      data.N_PESOACTUAL,
      data.N_TALLAACTUAL,
      data.S_FACTORCARIOGENICOID,
      data.S_NUMEROCEPILLADA,
      data.S_CONSULTAODONTOLOGO,
      data.V_CARNE,
      data.V_VPH,
      data.V_VPHDOSIS,
      data.V_TT,
      data.V_ESQUEMA,
      data.MALTRATO,
      data.NUMERODESPARASITADA,
      data.USUARIOCREACION,
      data.MOMENTOCREACION,
      data.ESTADOREGISTRO,
      data.UUID,
      data.ESCOLARIZADO
    ], function(tx, results) {
      M.toast({
        html: "Datos registrados"
      });
    },errorHandler);
  });
}

function addPersonaAdolescensJuventudiaLocal(datos) {

  conn.transaction(function(tx) {
    tx.executeSql("INSERT INTO `personaadolescencia`(`PERSONAID`, `P_PLANIFICA`, `P_METODO`, `P_MOTIVO`, `EXAMENMEDICO`, `CITOLOGIA`, `CITOLOGIADETALLE`, `H_TOTALVIVO`,`H_TOTALABORTADO`, `TOS`, `LESIONES`, `V_TD`, `V_TDCUAL`, `V_VPH`, `SENO`, `SENOCUAL`, `VIOLENCIA`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`, `UUID`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", 
    [
      datos.PERSONAID,
      datos.P_PLANIFICA,
      datos.P_METODO,
      datos.P_MOTIVO,
      datos.EXAMENMEDICO,
      datos.CITOLOGIA,
      datos.CITOLOGIADETALLE,
      datos.H_TOTALVIVO,
      datos.H_TOTALABORTADO,
      datos.TOS,
      datos.LESIONES,
      datos.V_TD,
      datos.V_TDCUAL,
      datos.V_VPH,
      datos.SENO,
      datos.SENOCUAL,
      datos.VIOLENCIA,
      datos.USUARIOCREACION,
      datos.MOMENTOCREACION,
      datos.ESTADOREGISTRO,
      datos.UUID
    ], function(tx, results) {
      M.toast({
        html: "Datos registrados"
      });
    },errorHandler);
  });
}

function addPersonaAdultezLocal(datos) {

  conn.transaction(function(tx) {
    tx.executeSql("INSERT INTO `personaadultez`( `PERSONAID`, `N_PESOACTUAL`, `N_TALLAACTUAL`, `N_CINTURA`, `FUMA`, `TOMA`, `EJERCICIO`, `TOS`, `LESIONES`, `SENO`, `SENODETALLE`, `CITOLOGIA`,`CITOLOGIADETALLE`, `PROSTATA`, `PROSTATADETALLE`, `VISION`, `VISIONDETALLE`, `E_HIPERTENSION`, `E_HIPERTENSIONCONTROL`, `E_DIABETES`, `E_DIABETESCONTROL`, `E_EPOC`,`E_EPOCCONTROL`, `E_CANCER`, `E_CANCERDETALLE`, `E_CANCERCONTROL`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`,`UUID`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", 
    [
      datos.PERSONAID,
datos.N_PESOACTUAL,
datos.N_TALLAACTUAL,
datos.N_CINTURA,
datos.FUMA,
datos.TOMA,
datos.EJERCICIO,
datos.TOS,
datos.LESIONES,
datos.SENO,
datos.SENODETALLE,
datos.CITOLOGIA,
datos.CITOLOGIADETALLE,
datos.PROSTATA,
datos.PROSTATADETALLE,
datos.VISION,
datos.VISIONDETALLE,
datos.E_HIPERTENSION,
datos.E_HIPERTENSIONCONTROL,
datos.E_DIABETES,
datos.E_DIABETESCONTROL,
datos.E_EPOC,
datos.E_EPOCCONTROL,
datos.E_CANCER,
datos.E_CANCERDETALLE,
datos.E_CANCERCONTROL,
datos.USUARIOCREACION,
datos.MOMENTOCREACION,
datos.ESTADOREGISTRO,
      datos.UUID
    ], function(tx, results) {
      M.toast({
        html: "Datos registrados"
      });
    },errorHandler);
  });
}

/** INICIO grupopoblacional */
function addTipoGrupoPoblacionalLocal(grupopoblacional) {
  conn.transaction(function(tx) {
    tx.executeSql("delete from grupopoblacional", [], function(tx, results) {});
  });
  $.each(grupopoblacional, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO grupopoblacional (GRUPOPOBLACIONALID,GRUPOPOBLACIONALDESCRIPCION) VALUES(?,?)",
        [data.GRUPOPOBLACIONALID,data.GRUPOPOBLACIONALDESCRIPCION],
        function(tx, results) {},
        errorHandler
      );
    });
  });
}
function getTipoGrupoPoblacionalLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT * FROM grupopoblacional order by(GRUPOPOBLACIONALID) asc",
      [],
      function(tx, results) {
        $("#GRUPOPOBLACIONALID").html('<option value="0">...</option>');
        $.each(results.rows, function(i, barrio) {
          $("#GRUPOPOBLACIONALID").append('<option value="' + results.rows.item(i).GRUPOPOBLACIONALID + '">' + results.rows.item(i).GRUPOPOBLACIONALDESCRIPCION + "</option>");
        });
        
        $("#GRUPOPOBLACIONALID").val(6)
        $("#GRUPOPOBLACIONALID").formSelect();
      },
      errorHandler
    );
  });
}
function addGrupoOrganizadoLocal(grupoorganizado) {
  conn.transaction(function(tx) {
    tx.executeSql("delete from grupoorganizado", [], function(tx, results) {});
  });
  $.each(grupoorganizado, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO grupoorganizado (GRUPOORGANIZADOID,GRUPODESCRIPCION) VALUES(?,?)",
        [data.GRUPOORGANIZADOID,data.GRUPODESCRIPCION],
        function(tx, results) {},
        errorHandler
      );
    });
  });
}
function getGrupoOrganizadoLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT * FROM grupoorganizado order by(GRUPOORGANIZADOID) asc",
      [],
      function(tx, results) {
        $("#GRUPOORGANIZADO").html('<option value="0">...</option>');
        $.each(results.rows, function(i, barrio) {
          $("#GRUPOORGANIZADO").append('<option value="' + results.rows.item(i).GRUPOORGANIZADOID + '">' + results.rows.item(i).GRUPODESCRIPCION + "</option>");
        });
        $("#GRUPOORGANIZADO").formSelect();

      },
      errorHandler
    );
  });
}
/** FIN tipodiscapacidad */

/** INICIO etnia */
function addTipoEtniaLocal(etnia) {
  conn.transaction(function(tx) {
    tx.executeSql("delete from etnia", [], function(tx, results) {});
  });
  $.each(etnia, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO etnia (ETNIAID,ETNIADESCRIPCION) VALUES(?,?)",
        [data.ETNIAID,data.ETNIADESCRIPCION],
        function(tx, results) {},
        errorHandler
      );
    });
  });
}
function getTipoEtniaLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT * FROM etnia order by(ETNIAID) asc",
      [],
      function(tx, results) {
        $("#ETNIAID").html('<option value="0">...</option>');
        $.each(results.rows, function(i, barrio) {
          $("#ETNIAID").append('<option value="' + results.rows.item(i).ETNIAID + '">' + results.rows.item(i).ETNIADESCRIPCION + "</option>");
        });
        $("#ETNIAID").val(7);
        $("#ETNIAID").formSelect();

      },
      errorHandler
    );
  });
}
/** FIN tipodiscapacidad */

/** INICIO tipodiscapacidad */
function addTipoDiscapacidadLocal(tipodiscapacidad) {
  conn.transaction(function(tx) {
    tx.executeSql("delete from tipodiscapacidad", [], function(tx, results) {});
  });
  $.each(tipodiscapacidad, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO tipodiscapacidad (TIPODISCACACIDADID,TIPODISCAPACIDADNOMBRE, TIPODISCAPACIDADDESCRICPION, TIPODISCACACIDADABREVIATURA) VALUES(?,?,?,?)",
        [data.TIPODISCACACIDADID,data.TIPODISCAPACIDADNOMBRE, data.TIPODISCAPACIDADDESCRICPION,data.TIPODISCACACIDADABREVIATURA],
        function(tx, results) {},
        errorHandler
      );
    });
  });
}
function geTipoDiscapacidadLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT * FROM tipodiscapacidad order by(TIPODISCACACIDADID) asc",
      [],
      function(tx, results) {
        $("#TIPODISCACACIDADID").html('<option value="0">...</option>');
        $.each(results.rows, function(i, barrio) {
          $("#TIPODISCACACIDADID").append(
            '<option value="' +
              results.rows.item(i).TIPODISCACACIDADID +
              '">' +
              results.rows.item(i).TIPODISCAPACIDADNOMBRE +
              "</option>"
          );
        });
        $("#TIPODISCACACIDADID").formSelect();

      },
      errorHandler
    );
  });
}
/** FIN tipodiscapacidad */

/** INICIO tipocorreccion */
function addTipoCorreccionLocal(tipocorreccion) {
  conn.transaction(function(tx) {
    tx.executeSql("delete from tipocorreccion", [], function(tx, results) {});
  });
  $.each(tipocorreccion, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO `tipocorreccion`(`TIPOCORRECCIONID`, `TIPOCORRECCIONDESCRIPCION`) VALUES (?,?)",
        [data.TIPOCORRECCIONID, data.TIPOCORRECCIONDESCRIPCION],
        function(tx, results) {},
        errorHandler
      );
    });
  });
}
function geTipoCorreccionLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT * FROM tipocorreccion order by(TIPOCORRECCIONDESCRIPCION) asc",
      [],
      function(tx, results) {
        $("#TIPOCORRECCIONID").html('<option value="0">...</option>');
        $.each(results.rows, function(i, barrio) {
          $("#TIPOCORRECCIONID").append(
            '<option value="' +
              results.rows.item(i).TIPOCORRECCIONID +
              '">' +
              results.rows.item(i).TIPOCORRECCIONDESCRIPCION +
              "</option>"
          );
        });
      },
      errorHandler
    );
  });
}
/** Fin tipocorreccion */

/** INICIO tipofamilia */
function addTipoFamiliaLocal(tipofamilia) {
  conn.transaction(function(tx) {
    tx.executeSql("delete from tipofamilia", [], function(tx, results) {});
  });
  $.each(tipofamilia, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO `tipofamilia`(`TIPOFAMILIAID`, `TIPOFAMILIADESCRIPCION`) VALUES (?,?)",
        [data.TIPOFAMILIAID, data.TIPOFAMILIADESCRIPCION],
        function(tx, results) {},
        errorHandler
      );
    });
  });
}
function geTipoFamiliaLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT * FROM tipofamilia order by(TIPOFAMILIADESCRIPCION) asc",
      [],
      function(tx, results) {
        $("#TIPOFAMILIAID").html('<option value="0">...</option>');
        $.each(results.rows, function(i, barrio) {
          $("#TIPOFAMILIAID").append(
            '<option value="' +
              results.rows.item(i).TIPOFAMILIAID +
              '">' +
              results.rows.item(i).TIPOFAMILIADESCRIPCION +
              "</option>"
          );
        });
      },
      errorHandler
    );
  });
}
/** Fin tipofamilia */

/** INICIO serviciosanitario */
function addServicioSanitarioLocal(serviciosanitario) {
  conn.transaction(function(tx) {
    tx.executeSql("delete from serviciosanitario", [], function(tx, results) {});
  });
  $.each(serviciosanitario, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO `serviciosanitario`(`SERVICIOSANITARIOID`, `SERVICIOSANITARIODESCRIPCION`) VALUES (?,?)",
        [data.SERVICIOSANITARIOID, data.SERVICIOSANITARIODESCRIPCION],
        function(tx, results) {},
        errorHandler
      );
    });
  });
}
function getServicioSanitarioLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT * FROM serviciosanitario order by(SERVICIOSANITARIODESCRIPCION) asc",
      [],
      function(tx, results) {
        $("#SERVICIOSANITARIOID").html('<option value="0">...</option>');
        $.each(results.rows, function(i, barrio) {
          $("#SERVICIOSANITARIOID").append(
            '<option value="' +
              results.rows.item(i).SERVICIOSANITARIOID +
              '">' +
              results.rows.item(i).SERVICIOSANITARIODESCRIPCION +
              "</option>"
          );
        });
      },
      errorHandler
    );
  });
}
/** Fin serviciosanitario */

/** INICIO tipoalumbrado */
function addTipoAlumbradoLocal(tipoalumbrado) {
  conn.transaction(function(tx) {
    tx.executeSql("delete from tipoalumbrado", [], function(tx, results) {});
  });
  $.each(tipoalumbrado, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO `tipoalumbrado`(`TIPOALUMBRADOID`, `TIPOALUMBRADODESCRIPCION`) VALUES (?,?)",
        [data.TIPOALUMBRADOID, data.TIPOALUMBRADODESCRIPCION],
        function(tx, results) {},
        errorHandler
      );
    });
  });
}
function getTipoAlumbradoLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT * FROM tipoalumbrado order by(TIPOALUMBRADODESCRIPCION) asc",
      [],
      function(tx, results) {
        $("#TIPOALUMBRADOID").html('<option value="0">...</option>');
        $.each(results.rows, function(i, barrio) {
          $("#TIPOALUMBRADOID").append(
            '<option value="' +
              results.rows.item(i).TIPOALUMBRADOID +
              '">' +
              results.rows.item(i).TIPOALUMBRADODESCRIPCION +
              "</option>"
          );
        });
      },
      errorHandler
    );
  });
}
/** Fin fuenteagua */

/** INICIO fuenteagua */
function addFuenteAguaLocal(fuenteagua) {
  conn.transaction(function(tx) {
    tx.executeSql("delete from fuenteagua", [], function(tx, results) {});
  });
  $.each(fuenteagua, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO `fuenteagua`(`FUENTEAGUAID`, `FUENTEAGUADECRIPCION`) VALUES (?,?)",
        [data.FUENTEAGUAID, data.FUENTEAGUADECRIPCION],
        function(tx, results) {},
        errorHandler
      );
    });
  });
}
function getFuenteAguaLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT * FROM fuenteagua order by(FUENTEAGUADECRIPCION) asc",
      [],
      function(tx, results) {
        $("#FUENTEAGUAID").html('<option value="0">...</option>');
        $.each(results.rows, function(i, barrio) {
          $("#FUENTEAGUAID").append(
            '<option value="' +
              results.rows.item(i).FUENTEAGUAID +
              '">' +
              results.rows.item(i).FUENTEAGUADECRIPCION +
              "</option>"
          );
        });
      },
      errorHandler
    );
  });
}
/** Fin fuenteagua */

/** INICIO tipopared */
function addTipoParedLocal(tipopared) {
  conn.transaction(function(tx) {
    tx.executeSql("delete from tipopared", [], function(tx, results) {});
  });
  $.each(tipopared, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO `tipopared`(`TIPOPAREDID`, `TIPOPAREDDESCRIPCION`) VALUES (?,?)",
        [data.TIPOPAREDID, data.TIPOPAREDDESCRIPCION],
        function(tx, results) {},
        errorHandler
      );
    });
  });
}
function getTipoParedLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT * FROM tipopared order by(TIPOPAREDDESCRIPCION) asc",
      [],
      function(tx, results) {
        $("#TIPOPAREDID").html('<option value="0">...</option>');
        $.each(results.rows, function(i, barrio) {
          $("#TIPOPAREDID").append(
            '<option value="' +
              results.rows.item(i).TIPOPAREDID +
              '">' +
              results.rows.item(i).TIPOPAREDDESCRIPCION +
              "</option>"
          );
        });
      },
      errorHandler
    );
  });
}
/** Fin tipopared */

/** INICIO tipopiso */
function addTipoPisoLocal(tipopiso) {
  conn.transaction(function(tx) {
    tx.executeSql("delete from tipopiso", [], function(tx, results) {});
  });
  $.each(tipopiso, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO `tipopiso`(`TIPOPISOID`, `TIPOPISODESCRIPCION`) VALUES (?,?)",
        [data.TIPOPISOID, data.TIPOPISODESCRIPCION],
        function(tx, results) {},
        errorHandler
      );
    });
  });
}
function getTipoPisoLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT * FROM tipopiso order by(TIPOPISODESCRIPCION) asc",
      [],
      function(tx, results) {
        $("#TIPOPISOID").html('<option value="0">...</option>');
        $.each(results.rows, function(i, barrio) {
          $("#TIPOPISOID").append(
            '<option value="' +
              results.rows.item(i).TIPOPISOID +
              '">' +
              results.rows.item(i).TIPOPISODESCRIPCION +
              "</option>"
          );
        });
      },
      errorHandler
    );
  });
}
/** Fin tipovivienda */

/** INICIO tipovivienda */
function addTipoViviendaLocal(tipovivienda) {
  
  
  conn.transaction(function(tx) {
    tx.executeSql("delete from tipovivienda", [], function(tx, results) {});
  });
  $.each(tipovivienda, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO `tipovivienda`(`TIPOVIVIENDAID`, `TIPOVIVIENDADESCRIPCION`) VALUES (?,?)",
        [data.TIPOVIVIENDAID, data.TIPOVIVIENDADESCRIPCION],
        function(tx, results) {},
        errorHandler
      );
    });
  });
}
function getTipoViviendaLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT * FROM tipovivienda order by(TIPOVIVIENDADESCRIPCION) asc",
      [],
      function(tx, results) {
        $("#TIPOVIVIENDAID").html('<option value="0">...</option>');
        $.each(results.rows, function(i, barrio) {
          $("#TIPOVIVIENDAID").append(
            '<option value="' +
              results.rows.item(i).TIPOVIVIENDAID +
              '">' +
              results.rows.item(i).TIPOVIVIENDADESCRIPCION +
              "</option>"
          );
        });
      },
      errorHandler
    );
  });
}
/** Fin tipovivienda */


/** INICIO estadocivil */
function addEstadoCivilLocal(estadocivil) {
  conn.transaction(function(tx) {
    tx.executeSql("delete from estadocivil", [], function(tx, results) {});
  });
  $.each(estadocivil, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO `estadocivil`(`ESTADOCIVILID`, `ESTADOCIVILDESCRIPCION`) VALUES (?,?)",
        [data.ESTADOCIVILID, data.ESTADOCIVILDESCRIPCION],
        function(tx, results) {},
        errorHandler
      );
    });
  });
}
function getEstadoCivilLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT * FROM estadocivil order by(ESTADOCIVILDESCRIPCION) asc",
      [],
      function(tx, results) {
        $("#ESTADOCIVILID").html('<option value="0">...</option>');
        $.each(results.rows, function(i, barrio) {
          $("#ESTADOCIVILID").append(
            '<option value="' +
              results.rows.item(i).ESTADOCIVILID +
              '">' +
              results.rows.item(i).ESTADOCIVILDESCRIPCION +
              "</option>"
          );
        });
      },
      errorHandler
    );
  });
}
/** Fin estadocivil */

function addPosition(data) {
  conn.transaction(function(tx) {
    tx.executeSql(
      "INSERT INTO ubicaciones (longitud ,latitud , usuario,fecha ) VALUES (" +
        data.LONGITUD +
        "," +
        data.LATITUD +
        "," +
        data.USUARIO +
        "," +
        data.FECHA +
        ")",
      [],
      function(tx, results) {
        console.log("Ubicacion guardada");
        if (results.rowsAffected > 0) {
          if (data.USUARIO) {
            enviarUbicacionServer(data, results.insertId);
          }
        }
      },
      errorHandler
    );
  });
}

function addSesion(data) {
  conn.transaction(function(tx) {
    tx.executeSql(
      "INSERT INTO sesion (usuario ,usuarioId ) VALUES ('" +
        data.usuario +
        "','" +
        data.usuarioId +
        "')",
      [],
      function(tx, results) {
        if (results.rowsAffected > 0) {
          localStorage.setItem("userName", data.usuario);
          localStorage.setItem("userId", data.usuarioId);
          location.href = "ubicacion.html";
        }
      }
    );
  });
}


function addMunicipioLocal(municipios) {
  conn.transaction(function(tx) {
    tx.executeSql("delete from municipios", [], function(tx, results) {});
  });
  $.each(municipios, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO municipios (MUNICIPIOID ,MUNICIPIONOMBRE ) VALUES (" +
          data.MUNICIPIOID +
          ",'" +
          data.MUNICIPIONOMBRE +
          "')",
        [],
        function(tx, results) {},
        errorHandler
      );
    });
  });
}

/** INICIO tipoMienbro */
function addTipoMiembroLocal(tipomiembro) {
  conn.transaction(function(tx) {
    tx.executeSql("delete from tipomiembro", [], function(tx, results) {});
  });
  $.each(tipomiembro, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO `tipomiembro`(`TIPOMIEMBROID`, `TIPOMIEMBRODESCRIPCION`) VALUES (?,?)",
        [data.TIPOMIEMBROID, data.TIPOMIEMBRODESCRIPCION],
        function(tx, results) {},
        errorHandler
      );
    });
  });
}
function getTipoMiembroLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT * FROM tipomiembro order by(TIPOMIEMBRODESCRIPCION) asc",
      [],
      function(tx, results) {
        $("#TIPOMIEMBROID").html('<option value="0">...</option>');
        $.each(results.rows, function(i, barrio) {
          $("#TIPOMIEMBROID").append(
            '<option value="' +
              results.rows.item(i).TIPOMIEMBROID +
              '">' +
              results.rows.item(i).TIPOMIEMBRODESCRIPCION +
              "</option>"
          );
        });
        $("#TIPOMIEMBROID").append('<option value="otro">Otro</option>');
      },
      errorHandler
    );
  });
}
/** Fin tipoMienbro */

/** INICIO tipoDocumento */
function addTipoDocumentoLocal(tipodocumento) {
  conn.transaction(function(tx) {
    tx.executeSql("delete from tipodocumento", [], function(tx, results) {});
  });
  $.each(tipodocumento, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO `tipodocumento`(`TIPODOCUMENTOID`, `TIPODOCUMENTODESCRIPCION`) VALUES (?,?)",
        [data.TIPODOCUMENTOID, data.TIPODOCUMENTODESCRIPCION],
        function(tx, results) {},
        errorHandler
      );
    });
  });
}
function getTipoDocumentoLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT * FROM tipodocumento order by(TIPODOCUMENTODESCRIPCION) asc",
      [],
      function(tx, results) {
        $("#TIPODOCUMENTOID").html('<option value="0">...</option>');
        $.each(results.rows, function(i, barrio) {
          $("#TIPODOCUMENTOID").append(
            '<option value="' +
              results.rows.item(i).TIPODOCUMENTOID +
              '">' +
              results.rows.item(i).TIPODOCUMENTODESCRIPCION +
              "</option>"
          );
        });
        $("#TIPODOCUMENTOID").append('<option value="otro">Otro</option>');
      },
      errorHandler
    );
  });
}
/** Fin tipoDocumento */

/** INICIO ocupacion */
function addOcupacionesLocal(ocupacion) {
  conn.transaction(function(tx) {
    tx.executeSql("delete from ocupacion", [], function(tx, results) {});
  });
  $.each(ocupacion, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO `ocupacion`(`OCUPACIONID`, `OCUPACIONDESCRIPCION`) VALUES (?,?)",
        [data.OCUPACIONID, data.OCUPACIONDESCRIPCION],
        function(tx, results) {},
        errorHandler
      );
    });
  });
}
function getOcupacionesLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT * FROM ocupacion order by(OCUPACIONDESCRIPCION) asc",
      [],
      function(tx, results) {
        $("#OCUPACIONID").html('<option value="0">...</option>');
        $.each(results.rows, function(i, barrio) {
          $("#OCUPACIONID").append(
            '<option value="' +
              results.rows.item(i).OCUPACIONID +
              '">' +
              results.rows.item(i).OCUPACIONDESCRIPCION +
              "</option>"
          );
        });
        
      },
      errorHandler
    );
  });
}
/** Fin ocupacion */

/** INICIO niveldeestudio */
function addNivelEstudioLocal(niveldeestudio) {
  conn.transaction(function(tx) {
    tx.executeSql("delete from niveldeestudio", [], function(tx, results) {});
  });
  $.each(niveldeestudio, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO `niveldeestudio`(`NIVELDEESTUDIOID`, `NIVELDEESTUDIODESCRIPCION`) VALUES (?,?)",
        [data.NIVELDEESTUDIOID, data.NIVELDEESTUDIODESCRIPCION],
        function(tx, results) {},
        errorHandler
      );
    });
  });
}

function getNivelEstudioLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT * FROM niveldeestudio order by(NIVELDEESTUDIODESCRIPCION) asc",
      [],
      function(tx, results) {
        $("#NIVELDEESTUDIOID").html('<option value="0">...</option>');
        $.each(results.rows, function(i, niveldeestudio) {
          $("#NIVELDEESTUDIOID").append(
            '<option value="' + niveldeestudio.NIVELDEESTUDIOID + '">' + niveldeestudio.NIVELDEESTUDIODESCRIPCION + "</option>"
          );
        });
      },
      errorHandler
    );
  });
}

/** Fin niveldeestudio */
function addEpsLocal(Eps) {
  conn.transaction(function(tx) {
    tx.executeSql("delete from eps", [], function(tx, results) {});
  });
  $.each(Eps, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO eps (EPSID,EPSDESCRIPCION) VALUES (" +
          data.EPSID +
          ",'" +
          data.EPSDESCRIPCION +
          "')",
        [],
        function(tx, results) {},
        errorHandler
      );
    });
  });
}

function addBarrio(barrios) {
  $.each(barrios, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO barrios (BARRIOID ,BARRIONOMBRE ) VALUES (" +
          data.BARRIOID +
          ",'" +
          data.BARRIONOMBRE +
          "')",
        [],
        function(tx, results) {}
      );
    });
  });
}
/**
 * Inicio UbicacionGuardian
 */
function guardarUbicacionGuardianLocal(datos) {

  conn.transaction(function(tx) {
    tx.executeSql(
      "INSERT INTO fichaguardian (NUMERO,CIUDADID,CABECERA,CORREGIMIENTO,VEREDA,MANZANA,VIVIENDA,FAMILIA,BARRIO,DIRECCION,TELEFONO,EPSID,USUARIOCREACION,MOMENTOCREACION,ESTADOREGISTRO,LONGITUD, LATITUD,UUID,INICIOCAPTURA) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
      [
        datos.NUMERO,
        datos.CIUDADID,
        datos.CABECERA,
        datos.CORREGIMIENTO,
        datos.VEREDA,
        datos.MANZANA,
        datos.VIVIENDA,
        datos.FAMILIA,
        datos.BARRIO,
        datos.DIRECCION,
        datos.TELEFONO,
        datos.EPSID,
        datos.USUARIOCREACION,
        datos.MOMENTOCREACION,
        datos.ESTADOREGISTRO,
        datos.LONGITUD,
        datos.LATITUD,
        datos.UUID,
        datos.INICIOCAPTURA
      ],
      function(tx, results) {
        if (results.rowsAffected > 0) {
         
          if (datos.USUARIOCREACION) {
              guardarUbicacionGuardianServer(datos,results.insertId)
            }
          /*hace referencia al ultimo registro de la tabla 
            fichaguardian que sirve de llave foranea en las 
            otras tablas que van a llenar con el valor de ese localStorage (FICHAGUARDIANID)*/
          localStorage.setItem("FICHAGUARDIANID", results.insertId);
          M.toast({html: "Datos registrados"});
          redirect('vivienda')
        } else {
          M.toast({
            html: "Se presentó un error al intentar registrar la información"
          });
        }
      },
      errorHandler
    );
  });
}

function updateIdUbicacionGuardianLocal(id,dbServerId) {
  conn.transaction(function(tx) {
    tx.executeSql(
      "update fichaguardian set DBSERVERID=? where FICHAGUARDIANID=?",
      [dbServerId,id],
      function(tx, results) {
        console.log(results);
      }
    );
  });
}
function updateIdGestacionLocal(id,dbServerId) {
  conn.transaction(function(tx) {
    tx.executeSql(
      "update GESTACION set DBSERVERID=? where GESTACIONID=?",
      [dbServerId,id],
      function(tx, results) {
        console.log(results);
      }
    );
  });
}
function updateIdViviendaLocal(id,dbServerId) {
  conn.transaction(function(tx) {
    tx.executeSql(
      "update vivienda set DBSERVERID=? where VIVIENDAID=?",
      [dbServerId,id],
      function(tx, results) {
        console.log(results);
      }
    );
  });
}
function updateIdImagen(id,dbServerId) {
  conn.transaction(function(tx) {
    tx.executeSql(
      "update imagenes set DBSERVERID=? where UBICACIONUUID=?",
      [id,dbServerId],
      function(tx, results) {
        console.log(results);
      }
    );
  });
}

function updateIdMascotaLocal(id,dbServerId) {
  conn.transaction(function(tx) {
    tx.executeSql(
      "update mascota set DBSERVERID=? where MASCOTAID=?",
      [dbServerId,id],
      function(tx, results) {
        console.log(results);
      }
    );
  });
}

function updateIdPersonaLocal(id,dbServerId) {
  conn.transaction(function(tx) {
    tx.executeSql(
      "update persona set DBSERVERID=? where PERSONAID=?",
      [dbServerId,id],
      function(tx, results) {
        console.log(results);
      }
    );
  });
}

function updateIdPersonaPrimeraInfanciaLocal(id,dbServerId) {
  conn.transaction(function(tx) {
    tx.executeSql(
      "update personaprimerainfancia set DBSERVERID=? where PERSONAPRIMERAINFANCIAID=?",
      [dbServerId,id],
      function(tx, results) {
        console.log(results);
      }
    );
  });
}

function updateIdPersonaSegundaInfanciaLocal(id,dbServerId) {
  conn.transaction(function(tx) {
    tx.executeSql(
      "update personasegundainfancia set DBSERVERID=? where PERSONASEGUNDAINFANCIAID=?",
      [dbServerId,id],
      function(tx, results) {
        console.log(results);
      }
    );
  });
}

function updateIdAdolescenciaLocal(id,dbServerId) {
  conn.transaction(function(tx) {
    tx.executeSql(
      "update personaadolescencia set DBSERVERID=? where PERSONAADOLESCENCIAID=?",
      [dbServerId,id],
      function(tx, results) {
        console.log(results);
      }
    );
  });
}

function updateIdPersonaAdultezLocal(id,dbServerId) {
  conn.transaction(function(tx) {
    tx.executeSql(
      "update personaadultez set DBSERVERID=? where PERSONAADULTEZID=?",
      [dbServerId,id],
      function(tx, results) {
        console.log(results);
      }
    );
  });
}

function updateIdEscolarLocal(id,dbServerId) {
  conn.transaction(function(tx) {
    tx.executeSql(
      "update ESCOLAR set DBSERVERID=? where ESCOLARID=?",
      [dbServerId,id],
      function(tx, results) {
        console.log(results);
      }
    );
  });
}


function updateIdSaludOcupacionalLocal(id,dbServerId) {
  conn.transaction(function(tx) {
    tx.executeSql(
      "update saludocupacional set DBSERVERID=? where SALUDOCUPACIONALID=?",
      [dbServerId,id],
      function(tx, results) {
        console.log(results);
      }
    );
  });
}


/**
 * FIN UbicacionGuardian
 */
/**
 * INicio GESTACION
 */
function addGestacionLocal(datos) {
  conn.transaction(function(tx) {
    tx.executeSql(
      "INSERT INTO gestacion (PERSONAID, P_CONTROL, P_CARNE, RIESGO, FUM, FPP, SEMANAS, A_OBSTRETICO, A_PARTO, A_CESAREA, A_ABORTO, CONTROLES, VIH, VIHDETALLE, VDRL, VDRLDETALLE, ODONTOLOGIA, MULTIVITAMINICO, P_TIPO, P_ATENCION, POSPARTO, M_GEST, M_GESTCUAL, M_PARTO, M_PARTOCUAL, M_PUERP, M_PUERPCUAL,UUID,USUARIOCREACION,P_GESTANTE,TDTT) VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?)",
      [
        datos.PERSONAID,
        datos.P_CONTROL,
        datos.P_CARNE,
        datos.RIESGO,
        datos.FUM,
        datos.FPP,
        datos.SEMANAS,
        datos.A_OBSTRETICO,
        datos.A_PARTO,
        datos.A_CESAREA,
        datos.A_ABORTO,
        datos.CONTROLES,
        datos.VIH,
        datos.VIHDETALLE,
        datos.VDRL,
        datos.VDRLDETALLE,
        datos.ODONTOLOGIA,
        datos.MULTIVITAMINICO,
        datos.P_TIPO,
        datos.P_ATENCION,
        datos.POSPARTO,
        datos.M_GEST,
        datos.M_GESTCUAL,
        datos.M_PARTO,
        datos.M_PARTOCUAL,
        datos.M_PUERP,
        datos.M_PUERPCUAL,
        datos.UUID,
        datos.USUARIOCREACION,
        datos.P_GESTANTE,
        datos.TDTT
      ],
      function(tx, results) {
        
        M.toast({html: 'Datos Registrados'});
   
      },
      errorHandler
    );
  });
}
/**
 * FIN personagestacion
 */
/**
 * INicio PERSONA
 */
function guardarPersonaLocal(datos) {
  conn.transaction(function(tx) {
    tx.executeSql(
      "INSERT INTO `persona`(`FICHAGUARDIANID`, `TIPOMIEMBROID`, `TIPODOCUMENTOID`, `NUMERODOCUMENTO`, `NOMBRE`, `GENEROID`, `ESTADOCIVILID`, `FECHANACIMIENTO`, `NIVELDEESTUDIOID`, `OCUPACIONID`, `TIPOSALUDID`, `TIPOAFILIACIONID`, `EPSID`, `EPSOTRO`, `DISCAPACIDAD`, `DISCAPACIDADCUAL`, `ETNIAID`, `GRUPOPOBLACIONALID`, `GRUPOORGANIZADO`, `GESTACION`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`,UUID,F_PERTENECE,PRESTAMO,PRESTAMOVALORID,SECTORNEGOCIOID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
      [
        datos.FICHAGUARDIANID,
        datos.TIPOMIEMBROID,
        datos.TIPODOCUMENTOID,
        datos.NUMERODOCUMENTO,
        datos.NOMBRE,
        datos.GENEROID,
        datos.ESTADOCIVILID,
        datos.FECHANACIMIENTO,
        datos.NIVELDEESTUDIOID,
        datos.OCUPACIONID,
        datos.TIPOSALUDID,
        datos.TIPOAFILIACIONID,
        datos.EPSID,
        datos.EPSOTRO,
        datos.DISCAPACIDAD,
        datos.DISCAPACIDADCUAL,
        datos.ETNIAID,
        datos.GRUPOPOBLACIONALID,
        datos.GRUPOORGANIZADO,
        datos.GESTACION,
        datos.USUARIOCREACION,
        datos.MOMENTOCREACION,
        datos.ESTADOREGISTRO,
        datos.UUID,
        datos.F_PERTENECE,
        datos.PRESTAMO,
        datos.PRESTAMOVALORID,
        datos.SECTORNEGOCIOID
      ],
      function(tx, results) {
        localStorage.setItem('PERSONAID',results.insertId);
        M.toast({html: 'Datos Registrados'});
        redirect('miembro',false);
      },
      errorHandler
    );
  });
}

function listarPersonaLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT f1.`PERSONAID`,NOMBRE,TIPOMIEMBRODESCRIPCION,FECHANACIMIENTO,GENEROID,GESTACION FROM "+
      "`persona` f1 "+ 
      "INNER JOIN tipomiembro f2 on f1.`TIPOMIEMBROID`=f2.TIPOMIEMBROID  WHERE FICHAGUARDIANID=?",
      [localStorage.getItem('FICHAGUARDIANID')],
      function(tx, results) {
        $("#tablaMiembros").html("");
        $.each(results.rows, function(i, persona) {
         data=getEdadPersona(persona.FECHANACIMIENTO);
          i++;
          optMenu="";
          if (persona.GESTACION=='SI') {
						optMenu += '<li class="tipoPersona" data-tipo="gestacion" data-genero="'+persona.GENEROID+'" data-id="' + persona.PERSONAID + '" ><a href="#!">Gestación</a></li>';
           }
     
           if (data.unidad=='Años') {
						if (data.edad >= 1 && data.edad < 6) {
							optMenu += '<li class="tipoPersona" data-tipo="primeraInfancia16" data-genero="'+persona.GENEROID+'" data-id="' + persona.PERSONAID + '" ><a href="#!">Primera Infancia</a></li>';
						}
						if (data.edad >= 6 && data.edad < 12) {
							optMenu += '<li class="tipoPersona" data-tipo="segundainfancia" data-genero="'+persona.GENEROID+'" data-id="' + persona.PERSONAID + '" ><a href="#!">Segunda Infancia</a></li>';
						}
						if (data.edad >= 12 && data.edad < 18) {
							optMenu += '<li class="tipoPersona" data-tipo="adolesenciajuventud" data-genero="'+persona.GENEROID+'" data-id="' + persona.PERSONAID + '" ><a href="#!">Adolescencia Juventud</a></li>';

						}
						if (data.edad >= 18 && data.edad < 29) {
							optMenu += '<li class="tipoPersona" data-tipo="adolesenciajuventud" data-genero="'+persona.GENEROID+'" data-id="' + persona.PERSONAID + '" ><a href="#!">Adolescencia Juventud</a></li>';

						}
						if (data.edad >= 29 && data.edad < 60) {
							optMenu += '<li class="tipoPersona" data-tipo="adultezvejez" data-genero="'+persona.GENEROID+'" data-id="' + persona.PERSONAID + '" ><a href="#!">Adultez Vejéz</a></li>';
						}
						if (data.edad >= 60) {
							optMenu += '<li class="tipoPersona" data-tipo="adultezvejez" data-genero="'+persona.GENEROID+'" data-id="' + persona.PERSONAID + '" ><a href="#!">Adultez Vejéz</a></li>';
						}
					}else{
						if(data.meses >=0 && data.meses <= 11){
							optMenu += '<li class="tipoPersona" data-tipo="primeraInfancia01" data-genero="'+persona.GENEROID+'" data-id="' + persona.PERSONAID + '" ><a href="#!">Primera Infancia</a></li>';
						 }
					}

					/*if (data.unidad=='Años') {
						if((data.edad>=1 && data.edad<=5) || (data.edad>=6 && data.edad<=11)){
							optMenu += '<li class="tipoPersona" data-tipo="segundainfancia"  data-genero="'+persona.GENEROID+'"  data-id="' + persona.PERSONAID + '" ><a href="#!">Segunda Infancia</a></li>'
						}else if((data.edad>=12 && data.edad<=17) || (data.edad>=18 && data.edad<=28) ){
							optMenu = '<li class="tipoPersona" data-tipo="adolesenciajuventud"  data-genero="'+persona.GENEROID+'"  data-id="' + persona.PERSONAID + '" ><a href="#!">Adolescencia Juventud</a></li>'
						}else if(data.edad>=29 ){
							optMenu += '<li class="tipoPersona" data-tipo="adultezvejez"  data-genero="'+persona.GENEROID+'"  data-id="' + persona.PERSONAID + '" ><a href="#!">Adultez Vejéz</a></li>'
						}
					}else{
						if(data.meses >=0 && data.meses <= 11){
							optMenu += '<li class="tipoPersona" data-tipo="primeraInfancia"  data-genero="'+persona.GENEROID+'"  data-id="' + persona.PERSONAID + '" ><a href="#!">Primera Infancia</a></li>'
						}   
					}*/
          $("#tablaMiembros").append(
            "<tr><td>" +
              i +
              "</td><td>" +
              persona.TIPOMIEMBRODESCRIPCION +
              "</td><td>" +
              persona.NOMBRE +
              "</td><td>" +
              data.edad + " "+data.unidad+
              "</td>" +
              "<td>" +
              '<a class="dropdown-trigger btn-floating grey" href="#" data-target="dropdown' +
              i +
              '"><i class="material-icons">build</i></a>' +
              '<ul id="dropdown' +
              i +
              '" class="dropdown-content">' 
								+optMenu+
              '<li class="eliminarPersona" data-id="' +
              persona.PERSONAID +
              '" ><a href="#!">Eliminar</a></li>' +
              "</ul>" +
              "</td>" +
              "</tr>"
          );
          $(".dropdown-trigger").dropdown();
        });
        $(".tipoPersona").click(function(){
          console.log($(this).data());
          
          localStorage.setItem("genero",$(this).data("genero"));
          switch ($(this).data("tipo")) {
            case 'primeraInfancia01':
                $.get("primerainfancia01.html",(html)=>{
                  $("#modal-complemento-persona").html(html);
                 
                });
              break;
              case 'primeraInfancia16':
                $.get("primerainfancia16.html",(html)=>{
                  $("#modal-complemento-persona").html(html);
                 
                });
              break;
              case 'adultezvejez':
                $.get("adultez-vejez.html",(html)=>{
                  $("#modal-complemento-persona").html(html);
                });
              break;
              case 'adolesenciajuventud':
                $.get("adolesencia-juventud.html",(html)=>{
                  $("#modal-complemento-persona").html(html);
                });
              break;
              case 'segundainfancia':
                $.get("segundainfancia.html",(html)=>{
                  $("#modal-complemento-persona").html(html);
                });
              break;
              case 'gestacion':
                $.get("gestacion.html",(html)=>{
                  $("#modal-complemento-persona").html(html);
                });
              break;
          }
        
      
        });
        $(".eliminarPersona > a").click(function() {
          console.log(this);
          deletePersonaLocal(
            $(this)
              .parent()
              .data("id")
          );
        });

        $(".InformacionComplementoPersona > a").click(function() {
          //  deletePersonaLocal($(this).parent().data("id"))
        });
      },
      errorHandler
    );
  });
}

function deletePersonaLocal(id) {
  conn.transaction(function(tx) {
    if (confirm("¿Está seguro que desea eliminar el registro?")) {
      tx.executeSql("delete from persona where personaid=?", [id], function(
        tx,
        results
      ) {
        listarPersonaLocal();
      });
    }
  });
}

/**
 * FIN PERSONA
 */

/**
 * INICIO SALUD OCUPACIONAL
 */
function addSaludOcupacionalLocal(data) {
  conn.transaction(function(tx) {
    tx.executeSql("INSERT INTO `saludocupacional`(`FICHAGUARDIAN`, `TIPOFAMILIAID`, `TIPOCORRECCIONID`, `CIGARRILLO`, `ALCOHOL`, `PASTILLAS`, `PSA`, `COMPARTEN`, `RELACIONES`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`,`UUID`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",
      [
        data.FICHAGUARDIAN,
        data.TIPOFAMILIAID,
        data.TIPOCORRECCIONID,
        data.CIGARRILLO,
        data.ALCOHOL,
        data.PASTILLAS,
        data.PSA,
        data.COMPARTEN,
        data.RELACIONES,
        data.USUARIOCREACION,
        data.MOMENTOCREACION,
        data.ESTADOREGISTRO,
        data.UUID
      ],
      function(tx, results) {
        M.toast({html: 'Datos Registrados'});
        M.toast({html: 'Encuesta Terminada'});
        redirect('ubicacion',false);
      },
      errorHandler
    );
  });
}

/**
 * FIN SALUD OCUPACIONAL
 */

/**
 * INICIO VIVIENDA
 */
function guardarViviendaLocal(data) {
  conn.transaction(function(tx) {
    tx.executeSql("INSERT INTO `vivienda`(`FICHAGUARDIANID`, `TIPOVIVIENDAID`, `TIPOPISOID`, `TIPOPAREDID`, `TIPOTECHOID`, `ILUMINACION`, `VENTILACION`, `INGRESO`, `INGRESOCUAL`, `PERSONADORMITORIO`, `ARROYO`, `BASURERO`, `HUMO`, `FUENTEAGUAID`, `FUENTEAGUAOTRO`, `AGUATRATADA`, `TRATAMIENTO`, `DEPOSITOAGUAID`, `DEPOSITOAGUACUAL`, `DEPOSITOAGUAOTRO`, `LARVAS`, `ROEDORES`, `TIPOALUMBRADOID`, `TIPOALUMBRADOOTRO`, `SERVICIOSANITARIOID`, `MANEJOBASURA`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`, `UUID`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
      [
        data.FICHAGUARDIANID,
        data.TIPOVIVIENDAID,
        data.TIPOPISOID,
        data.TIPOPAREDID,
        data.TIPOTECHOID,
        data.ILUMINACION,
        data.VENTILACION,
        data.INGRESO,
        data.INGRESOCUAL,
        data.PERSONADORMITORIO,
        data.ARROYO,
        data.BASURERO,
        data.HUMO,
        data.FUENTEAGUAID,
        data.FUENTEAGUAOTRO,
        data.AGUATRATADA,
        data.TRATAMIENTO,
        data.DEPOSITOAGUAID,
        data.DEPOSITOAGUACUAL,
        data.DEPOSITOAGUAOTRO,
        data.LARVAS,
        data.ROEDORES,
        data.TIPOALUMBRADOID,
        data.TIPOALUMBRADOOTRO,
        data.SERVICIOSANITARIOID,
        data.MANEJOBASURA,
        data.USUARIOCREACION,
        data.MOMENTOCREACION,
        data.ESTADOREGISTRO,
        data.UUID
      ],
      function(tx, results) {
        M.toast({html: 'Datos Registrados'});
      },
      errorHandler
    );
  });
}


/**
 * FIN VIVIENDA
 */

/*
* INICIO MASCOTA
*/
function guardarMascotaLocal(data) {
  conn.transaction(function(tx) {
    tx.executeSql("INSERT INTO `mascota` (`FICHAGUARDIANID`, `TIPOMASCOTAID`, `TIPOMASCOTAOTRO`, `VACUANADA`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`,`UUID`) VALUES (?,?,?,?,?,?,?,?)",
      [
        data.FICHAGUARDIANID,
        data.TIPOMASCOTAID,
        data.TIPOMASCOTAOTRO,
        data.VACUANADA,
        data.USUARIOCREACION,
        data.MOMENTOCREACION,
        data.ESTADOREGISTRO,
        data.UUID
      ],
      function(tx, results) {
        M.toast({html: 'Datos Registrados'});
        listarMascotas();
      },
      errorHandler
    );
  });
}

function addTipoMascotaLocal(tipomascota) {
  conn.transaction(function(tx) {
    tx.executeSql("delete from tipomascota", [], function(tx, results) {});
  });
  $.each(tipomascota, function(i, data) {
    conn.transaction(function(tx) {
      tx.executeSql(
        "INSERT OR IGNORE INTO tipomascota (TIPOMASCOTAID,TIPOMASCOTADESCRIPCION) VALUES (" +
          data.TIPOMASCOTAID +
          ",'" +
          data.TIPOMASCOTADESCRIPCION +
          "')",
        [],
        function(tx, results) {},
        errorHandler
      );
    });
  });
}

function listarMascotas() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "select m.MASCOTAID, tm.TIPOMASCOTADESCRIPCION,m.TIPOMASCOTAOTRO,m.VACUANADA from mascota m inner join tipomascota tm on m.TIPOMASCOTAID=tm.TIPOMASCOTAID where m.FICHAGUARDIANID=?",
      [localStorage.getItem("FICHAGUARDIANID")],
      function(tx, results) {
        $("#listaMascotas").html("");
        $.each(results.rows,(i,mascota)=>{
            $("#listaMascotas").append('<tr><td>'+(i+1)+'</td><td>'+mascota.TIPOMASCOTADESCRIPCION+'</td><td>'+mascota.TIPOMASCOTAOTRO+'</td><td>'+mascota.VACUANADA+'</td><td><a class="eliminarMascota" onclick="eliminarMascota('+mascota.MASCOTAID+')">Eliminar</a></td></tr>')
          })
      }
    );
  });
}
function deleteMascota(id) {
  conn.transaction(function(tx) {
    tx.executeSql(
      "delete from mascota where MASCOTAID=?",
      [id],
      function(tx, results) {
        listarMascotas()
      }
    );
  });
}
function getTipoMascotasLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT * FROM tipomascota order by(TIPOMASCOTADESCRIPCION) asc",
      [],
      function(tx, results) {
        $("#TIPOMASCOTAID").html('<option value="0">...</option>');
        $.each(results.rows, function(i, barrio) {
          $("#TIPOMASCOTAID").append(
            '<option value="' +
              results.rows.item(i).TIPOMASCOTAID +
              '">' +
              results.rows.item(i).TIPOMASCOTADESCRIPCION +
              "</option>"
          );
        });
        
      },
      errorHandler
    );
  });
}

/*
*FIN MASCOTA
*/

function totalDia(tipo) {
  if (tipo) {
    conn.transaction(function(tx) {
      tx.executeSql("SELECT * FROM stats WHERE id=1", [], function(
        tx,
        results
      ) {
        conn.transaction(function(tx) {
          num = results.rows.item(0).totaldia + 1;
          tx.executeSql("UPDATE stats set totaldia=? where id=1", [num]);
        });
      });
    });
  } else {
    conn.transaction(function(tx) {
      tx.executeSql("SELECT * FROM stats WHERE id=1", [], function(
        tx,
        results
      ) {
        conn.transaction(function(tx) {
          tx.executeSql("UPDATE stats set totaldia=? where id=1", [0]);
        });
      });
    });
  }
}

function getMunicipiosLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT * FROM municipios order by(MUNICIPIONOMBRE) asc",
      [],
      function(tx, results) {
        $("#CIUDADID").html('<option value="0">...</option>');
        $.each(results.rows, function(i, barrio) {
          $("#CIUDADID").append(
            '<option value="' +
              results.rows.item(i).MUNICIPIOID +
              '">' +
              results.rows.item(i).MUNICIPIONOMBRE +
              "</option>"
          );
        });
        $("#CIUDADID").append('<option value="otro">Otro</option>');
      },
      errorHandler
    );
  });
}
function getEpsLocal() {
  conn.transaction(function(tx) {
    tx.executeSql(
      "SELECT * FROM eps order by(EPSDESCRIPCION) asc",
      [],
      function(tx, results) {
        $("#EPSID").html('<option value="0">...</option>');
        $.each(results.rows, function(i, barrio) {
          $("#EPSID").append(
            '<option value="' +
              results.rows.item(i).EPSID +
              '">' +
              results.rows.item(i).EPSDESCRIPCION +
              "</option>"
          );
        });
        $("#EPSID").append('<option value="otro">Otro</option>');
      },
      errorHandler
    );
  });
}


function getAllUbicaciones() {
  conn.transaction(function(tx) {
    tx.executeSql("SELECT * FROM ubicaciones", [], function(tx, results) {
      return results.rows;
    });
  });
  return false;
}

function deleteUbicacion(id) {
  conn.transaction(function(tx) {
    tx.executeSql("delete FROM ubicaciones where ID=?", [id], function(
      tx,
      results
    ) {});
  });
}

function updateUserName() {
  if(isLogin()){
    conn.transaction(function(tx) {
      username=localStorage.getItem("userName")
      userid=localStorage.getItem("userId")
      tx.executeSql("update fichaguardian set USUARIOCREACION=? where USUARIOCREACION is null", [username], function(tx,results) {console.log(results)});
      tx.executeSql("update ubicaciones set USUARIO=? where USUARIO is null", [userid], function(tx,results) {console.log(results)});
      tx.executeSql("update persona set USUARIOCREACION=? where USUARIOCREACION is null", [username], function(tx,results) {console.log(results)});
      tx.executeSql("update saludocupacional set USUARIOCREACION=? where USUARIOCREACION is null", [username], function(tx,results) {console.log(results)});
      tx.executeSql("update vivienda set USUARIOCREACION=? where USUARIOCREACION is null", [username], function(tx,results) {console.log(results)});
      tx.executeSql("update mascota set USUARIOCREACION=? where USUARIOCREACION is null", [username], function(tx,results) {console.log(results)});
      tx.executeSql("update personaprimerainfancia set USUARIOCREACION=? where USUARIOCREACION is null", [username], function(tx,results) {console.log(results)});
      tx.executeSql("update personasegundainfancia set USUARIOCREACION=? where USUARIOCREACION is null", [username], function(tx,results) {console.log(results)});
      tx.executeSql("update personaadolescencia set USUARIOCREACION=? where USUARIOCREACION is null", [username], function(tx,results) {console.log(results)});
      tx.executeSql("update personaadultez set USUARIOCREACION=? where USUARIOCREACION is null", [username], function(tx,results) {console.log(results)});
      tx.executeSql("update imagenes set USUARIO=? where USUARIO is null", [username], function(tx,results) {console.log(results)});
      tx.executeSql("update gestacion set USUARIOCREACION=? where USUARIOCREACION is null", [username], function(tx,results) {console.log(results)});
      tx.executeSql("update ESCOLAR set USUARIOCREACION=? where USUARIOCREACION is null", [username], function(tx,results) {console.log(results)});

    });
  }else{
    if(confirm("Para sincronizar debe autenticarse, ¿desea autenticarse?")){
      window.location.href="login.html"
    }
  }
}


function EliminaDatosSincronizados() {
  if(isLogin()){
    conn.transaction(function(tx) {
      username=localStorage.getItem("userName")
      userid=localStorage.getItem("userId")
      tx.executeSql("DELETE FROM fichaguardian where DBSERVERID is not null", [], function(tx,results) {console.log(results)});
      tx.executeSql("DELETE FROM ubicaciones where DBSERVERID is not null", [], function(tx,results) {console.log(results)});
      tx.executeSql("DELETE FROM persona where DBSERVERID is not null", [], function(tx,results) {console.log(results)});
      tx.executeSql("DELETE FROM saludocupacional where DBSERVERID is not null", [], function(tx,results) {console.log(results)});
      tx.executeSql("DELETE FROM vivienda where DBSERVERID is not null", [], function(tx,results) {console.log(results)});
      tx.executeSql("DELETE FROM mascota where DBSERVERID is not null", [], function(tx,results) {console.log(results)});
      tx.executeSql("DELETE FROM personaprimerainfancia where DBSERVERID is not null", [], function(tx,results) {console.log(results)});
      tx.executeSql("DELETE FROM personasegundainfancia where DBSERVERID is not null", [], function(tx,results) {console.log(results)});
      tx.executeSql("DELETE FROM personaadolescencia where DBSERVERID is not null", [], function(tx,results) {console.log(results)});
      tx.executeSql("DELETE FROM personaadultez where DBSERVERID is not null", [], function(tx,results) {console.log(results)});
      tx.executeSql("DELETE FROM imagenes where DBSERVERID is not null", [], function(tx,results) {console.log(results)});
      tx.executeSql("DELETE FROM gestacion where DBSERVERID is not null", [], function(tx,results) {console.log(results)});
      tx.executeSql("DELETE FROM ESCOLAR where DBSERVERID is not null", [], function(tx,results) {console.log(results)});

      
    });
  }else{
    if(confirm("Para sincronizar debe autenticarse, ¿desea autenticarse?")){
      window.location.href="login.html"
    }
  }
}

function failDB(err) {
  console.log("Error processing SQL: " + err);
}

function successDB() {
  console.log("Se creo la base de datos");
}

function crearDB() {
  conn.transaction(crearTablas, failDB, successDB);
}
function dropSesion() {
  localStorage.clear()
  window.location.replace("login.html");
}