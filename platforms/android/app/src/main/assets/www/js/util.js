var fileName = "app-debug.apk";
function isLogin(){
  if((localStorage.getItem('userName')==null || localStorage.getItem('userName')==undefined)  && (localStorage.getItem('userId')==null || localStorage.getItem('userId')==undefined)){
     return false;
  }
  return true;
}
function checkVersionApp() {
 
  $("#modal-update").modal();
  $("#modal-update").modal("open");

  cordova.getAppVersion.getVersionNumber(function (version) {
    $("#mensaje-update").html("Version actual: " + version);
    var settings = {
      async: true,
      crossDomain: true,
      url: "https://guardian.plussoluciones.com/WebServiceGuardian/usuario/",
      method: "POST",
      headers: {
        "content-type": "application/x-www-form-urlencoded"
        //"authorization": "Basic cmVnaXN0cm86SW52MHQwKjIwMTg=",
      },
      data: {
        OPT: "lastVersion"
      },
      beforeSend: function () { 
        $("#mensaje-update").append("<p>Buscando Actualizaciones...</p>");
      },
      error: function () {
        return false;
      }
    };

    $.ajax(settings).done(function (response) {
      obtenerDatosServer()
      $("#loader-update").hide();
      setTimeout(()=>{
        if (response.version == version) {
          $("#mensaje-update").append("<p>No hay actualizaciones disponibles</p>");
          $("#modal-update-cancelar").text("Aceptar");
        } else {
          $("#mensaje-update").append(
            "<p>Hay una actualización disponible</p><p>Nueva versión: " +
            response.version +
            "</p>"
          );
          $("#modal-update-cancelar").text("Cancelar");
          downloadUpdate()
        }
        $("#loader-update").hide();
      },15000);
    });
  });
}
function applyUpdate(){
  $("#mensaje-update").append("<p>Aplicando actualización</p>")
  cordova.plugins.fileOpener2.showOpenWithDialog(cordova.file.externalCacheDirectory + fileName, 'application/vnd.android.package-archive', {
    error: function (e) {
      M.toast({ html: 'Se presentó un error al intentar aplicar la actualizacion.' });
    },
    success: function () {
      //$("#modal-update-actualizar").show();
    }
  })
}
function downloadUpdate() {
  $("#mensaje-update").append("<p>Descargando actualización</p>");

  var downloadUrl = "https://guardian.plussoluciones.com/WebServiceGuardian/app-update/app-debug.apk";

  window.requestFileSystem(LocalFileSystem.TEMPORARY, 0, function (fileSystem) {
    var fileTransfer = new FileTransfer();
    fileTransfer.download(
      downloadUrl,
      cordova.file.externalCacheDirectory + fileName,

      function (entry) {
        applyUpdate()
      },
      function (error) {
        M.toast({ html: 'Se presentó un error al intentar descargar la actualizacion.' });
      }
    );
  });
}

function getFechaActual() {
  return new Date().getFullYear() + '' + ("0" + (new Date().getMonth() + 1)).slice(-2) + "" + ("0" + new Date().getDate()).slice(-2) + "" + ("0" + new Date().getHours()).slice(-2) + "" + ("0" + new Date().getMinutes()).slice(-2) + "" + ("0" + new Date().getSeconds()).slice(-2);
}

function getEdadPersona(edad,sw=false) {
  edad=edad+""
  if(sw){
    var fechaNacimiento = new Date(
      edad.substring(0,4),
      edad.substring(6,4)-1,
      edad.substring(6,8)
    );
  }else{
    var fechaNacimiento = new Date(
      edad.split("-")[0],
      edad.split("-")[1] - 1,
      edad.split("-")[2]
    );
  }
  
  var fechaActual = new Date();
  var meses = Math.floor((fechaActual.getTime() - fechaNacimiento.getTime()) / 1000 / 86400
  ) / 30;
  var anos =
    Math.floor(
      (fechaActual.getTime() - fechaNacimiento.getTime()) / 1000 / 86400
    ) / 365;
  var dias = Math.floor(
    (fechaActual.getTime() - fechaNacimiento.getTime()) / 1000 / 86400
  );
  if (anos < 1) {
    edad = parseInt(meses);
    unidad = "Meses";
    if (meses < 1) {
      edad = parseInt(dias);
      unidad = "Dias";
    }
  } else {
    edad = parseInt(anos);
    unidad = "Años";
  }
  return { "edad": edad, "unidad": unidad,"meses":meses };
}

function obtenerDatosServer() {

  $.ajax({
    "sync": false,
    "method": "POST",
    "crossDomain": true,
    "url": "https://guardian.plussoluciones.com/WebServiceGuardian/Utilidades/",
    "data": { "OPT": "obtenerDatosServer" },
    "headers": {
      "content-type": "application/x-www-form-urlencoded",
      //	"authorization": "Basic cmVnaXN0cm86SW52MHQwKjIwMTg=",
    },
    'beforeSend':function(){
      $("#mensaje-update").append("<p>Descargando datos de mantenimiento...</p>");
    } 
    ,"success": function (response) {

      if (response.ciudad != undefined) {
        if (response.ciudad.estado) {
          console.log(response.ciudad.data)
          addMunicipioLocal(response.ciudad.data)
        }
      }
      if (response.eps != undefined) {
        addEpsLocal(response.eps.data)
      }
      if (response.tipomiembro != undefined) {
        addTipoMiembroLocal(response.tipomiembro.data)
      }
      if (response.tipodocumento != undefined) {
        addTipoDocumentoLocal(response.tipodocumento.data)
      }
      if (response.tipomascota != undefined) {
        addTipoMascotaLocal(response.tipomascota.data)
      }
      if (response.tipoafiliacion != undefined) {
        //  addTipoA(response.tipoafiliacion.data)
      }
      if (response.tipoalumbrado != undefined) {
        addTipoAlumbradoLocal(response.tipoalumbrado.data)
      }
      if (response.tipocorreccion != undefined) {
        addTipoCorreccionLocal(response.tipocorreccion.data)
      }
      if (response.tipofamilia != undefined) {
        addTipoFamiliaLocal(response.tipofamilia.data)
      }
      if (response.tipopared != undefined) {
        addTipoParedLocal(response.tipopared.data)
      }
      if (response.tipopiso != undefined) {
        addTipoPisoLocal(response.tipopiso.data)
      }
      if (response.tiporemision != undefined) {
        //addTipore(response.tiporemision.data)
      }
      if (response.tiposalud != undefined) {
        //addTipo(response.tiposalud.data)
      }
      if (response.niveldeestudio != undefined) {
        addNivelEstudioLocal(response.niveldeestudio.data)
      }
      if (response.ocupacion != undefined) {
        addOcupacionesLocal(response.ocupacion.data)
      }
      if (response.estadocivil != undefined) {
        addEstadoCivilLocal(response.estadocivil.data)
      }
      if (response.fuenteagua != undefined) {
        addFuenteAguaLocal(response.fuenteagua.data)
      }
      if (response.serviciosanitario != undefined) {
        addServicioSanitarioLocal(response.serviciosanitario.data)
      }
      if (response.tipodiscapacidad != undefined) {
        addTipoDiscapacidadLocal(response.tipodiscapacidad.data)
      }
      if (response.etnia != undefined) {
        addTipoEtniaLocal(response.etnia.data)
      }
      if (response.grupopoblacional != undefined) {
      addTipoGrupoPoblacionalLocal(response.grupopoblacional.data)
      }
      if (response.tipovivienda != undefined) {
        addTipoViviendaLocal(response.tipovivienda.data)
        }
        if (response.grupoorganizado != undefined) {
          addGrupoOrganizadoLocal(response.grupoorganizado.data)
          }
    }, "error": function () {
      M.toast({ html: 'Se presentó un error al intentar descargar los datos.' });
    }
  })
}

function redirect(page,force){
  time=2000
  if(force){
    time=0
  }
  setTimeout(function(){
    window.location.replace(page+".html");
  },time)
}

function numeroFechaInput(fecha) {
  fecha = fecha + ''
  return fecha.substr(0,4)+'-'+fecha.substr(4,2)+'-'+fecha.substr(6)  
}

function exportDB(table,json){
  var directorio = "file:///storage/emulated/0";
  var nombreArchivo = table+".bk";

  window.resolveLocalFileSystemURL(directorio, function(dir) {
    dir.getFile(nombreArchivo, {create:true}, function(fileEntry) {
      fileEntry.createWriter(function(writer){
        //writer.seek(writer.length);
        writer.write(JSON.stringify(json));
      },function(){
        console.error("Error al intentar escribir en el archivo")
      });
    });
  });
}