<?php
header('Access-Control-Allow-Origin: *');
header('content-type: application/json;charset=utf-8');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 0);

if(isset($_POST) && isset($_POST['OPT'])) {
	$service=new ServiceGuardian();
	if (method_exists($service,$_POST['OPT'])) {
		$service->{$_POST['OPT']}();
	}else{
		echo json_encode(['estado'=>false,'mensaje'=>'se presentó un error al realizar la solicitud']);
	}
}


/**
 * Clase servicios de usuarios
 */
class ServiceGuardian {
	
	
	function guardarUbicacionGuardian(){
		$data=$_POST['data'];
		require("../../page/conectar.php");
		$where="SELECT * FROM fichaguardian WHERE UUID='".$data['UUID']."'";
		$conexion->query($where);	
		$rs_sql = $conexion->resultset();
		if($conexion->rowCount() > 0){
			//echo json_encode(['Id'=>0,'estado'=>true]);
			//die;
		}
		$sql="INSERT INTO `fichaguardian`(`NUMERO`, `CIUDADID`, `CABECERA`, `CORREGIMIENTO`, `VEREDA`, `MANZANA`, `VIVIENDA`, `FAMILIA`, `BARRIO`, `DIRECCION`, `TELEFONO`, `EPSID`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`, `UUID`, `LATITUD`, `LONGITUD`) VALUES ('"
		.$data['NUMERO']."',"
		.$data['CIUDADID'].",'"
		.$data['CABECERA']."','"
		.$data['CORREGIMIENTO']."','"
		.$data['VEREDA']."','"
		.$data['MANZANA']."','"
		.$data['VIVIENDA']."','"
		.$data['FAMILIA']."','"
		.$data['BARRIO']."','"
		.$data['DIRECCION']."','"
		.$data['TELEFONO']."',"
		.$data['EPSID'].",'"
		.$data['USUARIOCREACION']."',"
		.$data['MOMENTOCREACION'].","
		.$data['ESTADOREGISTRO'].",'"
		.$data['UUID']."'," 
		.$data['LATITUD'].","
		.$data['LONGITUD'].")";
		
		$conexion->query($sql);
		
		$conexion->execute();
		if($conexion->lastInsertId()){
			http_response_code(201);
			echo json_encode(['Id'=>$conexion->lastInsertId(),'estado'=>true]);
		}else{
			http_response_code(400);
			echo json_encode(['mensaje'=>'error al intentar registar la encuesta','estado'=>false]);
		}
		unset($conexion);

	}

	function guardarVivienda(){
		require("../../page/conectar.php");
		$data=$_POST["data"];
		$sql="INSERT INTO `vivienda`(`FICHAGUARDIANID`, `TIPOVIVIENDAID`, `TIPOPISOID`, `TIPOPAREDID`, `TIPOTECHOID`, `ILUMINACION`, `VENTILACION`, `INGRESO`, `INGRESOCUAL`, `PERSONADORMITORIO`, `ARROYO`, `BASURERO`, `HUMO`, `FUENTEAGUAID`, `FUENTEAGUAOTRO`, `AGUATRATADA`, `TRATAMIENTO`, `DEPOSITOAGUAID`, `DEPOSITOAGUACUAL`, `DEPOSITOAGUAOTRO`, `LARVAS`, `ROEDORES`, `TIPOALUMBRADOID`, `TIPOALUMBRADOOTRO`, `SERVICIOSANITARIOID`, `MANEJOBASURA`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`, `UUID`) VALUES (";
	
		$sql.=$data['FICHAGUARDIANIDS'].",".
		$data['TIPOVIVIENDAID'].",".
		$data['TIPOPISOID'].",".
		$data['TIPOPAREDID'].",".
		$data['TIPOTECHOID'].",'".
		$data['ILUMINACION']."','".
		$data['VENTILACION']."','".
		$data['INGRESO']."','".
		$data['INGRESOCUAL']."','".
		$data['PERSONADORMITORIO']."','".
		$data['ARROYO']."','".
		$data['BASURERO']."','".
		$data['HUMO']."',".
		$data['FUENTEAGUAID'].",'".
		$data['FUENTEAGUAOTRO']."','".
		$data['AGUATRATADA']."','".
		$data['TRATAMIENTO']."','".
		$data['DEPOSITOAGUAID']."','".
		$data['DEPOSITOAGUACUAL']."','".
		$data['DEPOSITOAGUAOTRO']."','".
		$data['LARVAS']."','".
		$data['ROEDORES']."','".
		$data['TIPOALUMBRADOID']."','".
		$data['TIPOALUMBRADOOTRO']."',".
		$data['SERVICIOSANITARIOID'].",'".
		$data['MANEJOBASURA']."','".
		$data['USUARIOCREACION']."',".
		$data['MOMENTOCREACION'].",".
		$data['ESTADOREGISTRO'].",'".
		$data['UUID']."')"
		;
		
		$conexion->query($sql);
		
		$conexion->execute();
		if($conexion->lastInsertId()){
			http_response_code(201);
			echo json_encode(['Id'=>$conexion->lastInsertId(),'estado'=>true]);
		}else{
			http_response_code(400);
			echo json_encode(['mensaje'=>'error al intentar registar la encuesta','estado'=>false]);
		}
		unset($conexion);
	}

	function guardarMascota(){
		require("../../page/conectar.php");

		$data=$_POST["data"];
		$sql="INSERT INTO `mascota`(`FICHAGUARDIANID`, `TIPOMASCOTAID`, `TIPOMASCOTAOTRO`, `VACUANADA`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`, `UUID`) VALUES (";
		$sql.=$data['FICHAGUARDIANIDS'].",".
		$data['TIPOMASCOTAID'].",'".
		$data['TIPOMASCOTAOTRO']."','".
		$data['VACUANADA']."','".
		$data['USUARIOCREACION']."','".
		$data['MOMENTOCREACION']."',".
		$data['ESTADOREGISTRO'].",'".
		$data['UUID']."')";

		$conexion->query($sql);
		
		$conexion->execute();
		if($conexion->lastInsertId()){
			http_response_code(201);
			echo json_encode(['Id'=>$conexion->lastInsertId(),'estado'=>true]);
		}else{
			http_response_code(400);
			echo json_encode(['mensaje'=>'error al intentar registar la encuesta','estado'=>false]);
		}
		unset($conexion);
	}

function guardarPersona(){
	require("../../page/conectar.php");

	$data = $_POST['data'];
	$sql= "INSERT INTO `persona`(`FICHAGUARDIANID`, `TIPOMIEMBROID`, `TIPODOCUMENTOID`, `NUMERODOCUMENTO`, `NOMBRE`, `GENEROID`, `ESTADOCIVILID`, `FECHANACIMIENTO`, `NIVELDEESTUDIOID`, `OCUPACIONID`, `TIPOSALUDID`, `TIPOAFILIACIONID`, `EPSID`, `EPSOTRO`, `DISCAPACIDAD`, `DISCAPACIDADCUAL`, `ETNIAID`, `GRUPOPOBLACIONALID`, `GRUPOORGANIZADO`, `GESTACION`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`,`F_PERTENECE`, `UUID`) VALUES (";
	$sql.=$data['FICHAGUARDIANIDS'].",".
	$data['TIPOMIEMBROID'].",".
	$data['TIPODOCUMENTOID'].",'".
	$data['NUMERODOCUMENTO']."','".
	$data['NOMBRE']."',".
	$data['GENEROID'].",".
	$data['ESTADOCIVILID'].",".
	str_replace('-','',$data['FECHANACIMIENTO']).",".
	$data['NIVELDEESTUDIOID'].",".
	$data['OCUPACIONID'].",".
	$data['TIPOSALUDID'].",".
	$data['TIPOAFILIACIONID'].",".
	$data['EPSID'].",'".
	$data['EPSOTRO']."','".
	$data['DISCAPACIDAD']."','".
	$data['DISCAPACIDADCUAL']."',".
	$data['ETNIAID'].",".
	$data['GRUPOPOBLACIONALID'].",".
	$data['GRUPOORGANIZADO'].",'".
	$data['GESTACION']."','".
	$data['USUARIOCREACION']."',".
	$data['MOMENTOCREACION'].",".
	$data['ESTADOREGISTRO'].",".
	$data['F_PERTENECE'].",'".
	$data['UUID']."')";

	$conexion->query($sql);
		
	$conexion->execute();
	if($conexion->lastInsertId()){
		http_response_code(201);
		echo json_encode(['Id'=>$conexion->lastInsertId(),'estado'=>true]);
	}else{
		http_response_code(400);
		echo json_encode(['mensaje'=>'error al intentar registar la encuesta','estado'=>false]);
	}
	unset($conexion);
}

function guardarPersonaPrimeraInfancia(){
	require("../../page/conectar.php");

	$data = $_POST['data'];
	$sql= "INSERT INTO `personaprimerainfancia`(`PERSONAID`, `CONTROLCYD`, `LACTANCIA`, `LACTANCIATIEMPO`, `N_PESONACER`, `N_PESOACTUAL`, `N_TALLANACER`, `N_TALLAACTUAL`, `V_CARNE`, `V_BCG`, `V_HEPB`, `V_POLIO`, `V_PENTAVALENTE`, `V_ROTAVIRUS`, `V_NEUMOCOCO`, `V_INFLUENZA`, `V_ESQUEMA`, `MALTRATO`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`, `UUID`,`ODONTOLOGO`,`CARIOGENICOS`) VALUES (";
	$sql.=$data['PERSONAIDS'].",'".
	$data['CONTROLCYD']."','".
	$data['LACTANCIA']."','".
	$data['LACTANCIATIEMPO']."',".
	$data['N_PESONACER'].",".
	$data['N_PESOACTUAL'].",".
	$data['N_TALLANACER'].",".
	$data['N_TALLAACTUAL'].",'".
	$data['V_CARNE']."','".
	$data['V_BCG']."','".
	$data['V_HEPB']."',".
	$data['V_POLIO'].",".
	$data['V_PENTAVALENTE'].",".
	$data['V_ROTAVIRUS'].",".
	$data['V_NEUMOCOCO'].",".
	$data['V_INFLUENZA'].",'".
	$data['ESQUEMA']."','".
	$data['MALTRATO']."','".
	$data['USUARIOCREACION']."',".
	$data['MOMENTOCREACION'].",".
	$data['ESTADOREGISTRO'].",'".
	$data['UUID']."','".$data['ODONTOLOGO']."','".$data['CARIOGENICOS']."')";
	
	$conexion->query("UPDATE personaprimerainfancia SET ESTADOREGISTRO=0 WHERE ESTADOREGISTRO=1 AND PERSONAID=".$data['PERSONAIDS']);
	$conexion->execute();

	$conexion->query($sql);
	$conexion->execute();
	if($conexion->lastInsertId()){
		http_response_code(201);
		echo json_encode(['Id'=>$conexion->lastInsertId(),'estado'=>true]);
	}else{
		http_response_code(400);
		echo json_encode(['mensaje'=>'error al intentar registar la encuesta','estado'=>false]);
	}
	unset($conexion);
}

function guardarPersonaSegundaInfancia(){
	require("../../page/conectar.php");

	$data = $_POST['data'];
	$sql= "INSERT INTO `personasegundainfancia`(`PERSONAID`, `CONTROLCYD`, `N_PESOACTUAL`, `N_TALLAACTUAL`, `S_FACTORCARIOGENICOID`, `S_NUMEROCEPILLADA`, `S_CONSULTAODONTOLOGO`, `V_CARNE`, `V_VPH`, `V_VPHDOSIS`, `V_TT`, `V_ESQUEMA`, `MALTRATO`, `NUMERODESPARASITADA`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`,`UUID`) VALUES (";
	$sql.=$data['PERSONAIDS'].",'".
	$data['CONTROLCYD']."',".
	$data['N_PESOACTUAL'].",".
	$data['N_TALLAACTUAL'].",".
	$data['S_FACTORCARIOGENICOID'].",".
	$data['S_NUMEROCEPILLADA'].",'".
	$data['S_CONSULTAODONTOLOGO']."','".
	$data['V_CARNE']."','".
	$data['V_VPH']."',".
	$data['V_VPHDOSIS'].",'".
	$data['V_TT']."','".
	$data['V_ESQUEMA']."','".
	$data['MALTRATO']."',".
	$data['NUMERODESPARASITADA'].",'".
	$data['USUARIOCREACION']."',".
	$data['MOMENTOCREACION'].",".
	$data['ESTADOREGISTRO'].",'".
	$data['UUID']."')";
echo $sql;
	
	$conexion->query("UPDATE personasegundainfancia SET ESTADOREGISTRO=0 WHERE ESTADOREGISTRO=1 AND PERSONAID=".$data['PERSONAIDS']);
	$conexion->execute();

	$conexion->query($sql);
		
	$conexion->execute();
	if($conexion->lastInsertId()){
		http_response_code(201);
		echo json_encode(['Id'=>$conexion->lastInsertId(),'estado'=>true]);
	}else{
		http_response_code(400);
		echo json_encode(['mensaje'=>'error al intentar registar la encuesta','estado'=>false]);
	}
	unset($conexion);
}

function guardarPersonaAdolescencia(){
	require("../../page/conectar.php");

	$data = $_POST['data'];
	$sql= "INSERT INTO `personaadolescencia`(`PERSONAID`, `P_PLANIFICA`, `P_METODO`, `P_MOTIVO`, `EXAMENMEDICO`, `CITOLOGIA`, `CITOLOGIADETALLE`, `H_TOTALVIVO`, `H_TOTALABORTADO`, `TOS`, `LESIONES`, `V_TD`, `V_TDCUAL`, `V_VPH`, `SENO`, `SENOCUAL`, `VIOLENCIA`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`, `UUID`) VALUES (";
	$sql.=$data['PERSONAIDS'].",'".
	$data['P_PLANIFICA']."','".
	$data['P_METODO']."','".
	$data['P_MOTIVO']."','".
	$data['EXAMENMEDICO']."','".
	$data['CITOLOGIA']."','".
	$data['CITOLOGIADETALLE']."',".
	$data['H_TOTALVIVO'].",".
	$data['H_TOTALABORTADO'].",'".
	$data['TOS']."','".
	$data['LESIONES']."','".
	$data['V_TD']."','".
	$data['V_TDCUAL']."','".
	$data['V_VPH']."','".
	$data['SENO']."','".
	$data['SENOCUAL']."','".
	$data['VIOLENCIA']."','".
	$data['USUARIOCREACION']."',".
	$data['MOMENTOCREACION'].",".
	$data['ESTADOREGISTRO'].",'".
	$data['UUID']."')";

	$conexion->query("UPDATE personaadolescencia SET ESTADOREGISTRO=0 WHERE ESTADOREGISTRO=1 AND PERSONAID=".$data['PERSONAIDS']);
	$conexion->execute();

	$conexion->query($sql);
		
	$conexion->execute();
	if($conexion->lastInsertId()){
		http_response_code(201);
		echo json_encode(['Id'=>$conexion->lastInsertId(),'estado'=>true]);
	}else{
		http_response_code(400);
		echo json_encode(['mensaje'=>'error al intentar registar la encuesta','estado'=>false]);
	}
	unset($conexion);
}

function guardarPersonaAdultez(){
	require("../../page/conectar.php");

	$data = $_POST['data'];
	$sql= "INSERT INTO `personaadultez`( `PERSONAID`, `N_PESOACTUAL`, `N_TALLAACTUAL`, `N_CINTURA`, `FUMA`, `TOMA`, `EJERCICIO`, `TOS`, `LESIONES`, `SENO`, `SENODETALLE`, `CITOLOGIA`, `CITOLOGIADETALLE`, `PROSTATA`, `PROSTATADETALLE`, `VISION`, `VISIONDETALLE`, `E_HIPERTENSION`, `E_HIPERTENSIONCONTROL`, `E_DIABETES`, `E_DIABETESCONTROL`, `E_EPOC`, `E_EPOCCONTROL`, `E_CANCER`, `E_CANCERDETALLE`, `E_CANCERCONTROL`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`, `UUID`) VALUES (";
	$sql.=$data['PERSONAIDS'].",".
	$data['N_PESOACTUAL'].",".
	$data['N_TALLAACTUAL'].",".
	$data['N_CINTURA'].",'".
	$data['FUMA']."','".
	$data['TOMA']."','".
	$data['EJERCICIO']."','".
	$data['TOS']."','".
	$data['LESIONES']."','".
	$data['SENO']."','".
	$data['SENODETALLE']."','".
	$data['CITOLOGIA']."','".
	$data['CITOLOGIADETALLE']."','".
	$data['PROSTATA']."','".
	$data['PROSTATADETALLE']."','".
	$data['VISION']."','".
	$data['VISIONDETALLE']."','".
	$data['E_HIPERTENSION']."','".
	$data['E_HIPERTENSIONCONTROL']."','".
	$data['E_DIABETES']."','".
	$data['E_DIABETESCONTROL']."','".
	$data['E_EPOC']."','".
	$data['E_EPOCCONTROL']."','".
	$data['E_CANCER']."','".
	$data['E_CANCERDETALLE']."','".
	$data['E_CANCERCONTROL']."','".
	$data['USUARIOCREACION']."',".
	$data['MOMENTOCREACION'].",".
	$data['ESTADOREGISTRO'].",'".
	$data['UUID']."')";

	$conexion->query("UPDATE personaadultez SET ESTADOREGISTRO=0 WHERE ESTADOREGISTRO=1 AND PERSONAID=".$data['PERSONAIDS']);
	$conexion->execute();
	
	$conexion->query($sql);
		
	$conexion->execute();
	if($conexion->lastInsertId()){
		http_response_code(201);
		echo json_encode(['Id'=>$conexion->lastInsertId(),'estado'=>true]);
	}else{
		http_response_code(400);
		echo json_encode(['mensaje'=>'error al intentar registar la encuesta','estado'=>false]);
	}
	unset($conexion);
}


function guardarSaludOcupacional(){
	require("../../page/conectar.php");

	$data = $_POST['data'];
	$sql= "INSERT INTO `saludocupacional`( `FICHAGUARDIAN`, `TIPOFAMILIAID`, `TIPOCORRECCIONID`, `CIGARRILLO`, `ALCOHOL`, `PASTILLAS`, `PSA`, `COMPARTEN`, `RELACIONES`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`, `UUID`) VALUES (";
	$sql.= $data['FICHAGUARDIANIDS'].",'".
	$data['TIPOFAMILIAID']."',".
	$data['TIPOCORRECCIONID'].",'".
	$data['CIGARRILLO']."','".
	$data['ALCOHOL']."','".
	$data['PASTILLAS']."','".
	$data['PSA']."','".
	$data['COMPARTEN']."','".
	$data['RELACIONES']."','".
	$data['USUARIOCREACION']."',".
	$data['MOMENTOCREACION'].",".
	$data['ESTADOREGISTRO'].",'".
	$data['UUID']."')";
	$conexion->query($sql);
		
	$conexion->execute();
	if($conexion->lastInsertId()){
		http_response_code(201);
		echo json_encode(['Id'=>$conexion->lastInsertId(),'estado'=>true]);
	}else{
		http_response_code(400);
		echo json_encode(['mensaje'=>'error al intentar registar la encuesta','estado'=>false]);
	}
	unset($conexion);
}

function guardarGestacion(){
	require("../../page/conectar.php");

	$data = $_POST['data'];
	$sql= "INSERT INTO gestacion (PERSONAID, P_CONTROL, P_CARNE, RIESGO, FUM, FPP, SEMANAS, A_OBSTRETICO, A_PARTO, A_CESAREA, A_ABORTO, CONTROLES, VIH, VIHDETALLE, VDRL, VDRLDETALLE, ODONTOLOGIA, MULTIVITAMINICO, P_TIPO, P_ATENCION, POSPARTO, M_GEST, M_GESTCUAL, M_PARTO, M_PARTOCUAL, M_PUERP, M_PUERPCUAL,TDTT) VALUES( ";
	
	$sql.= $data['PERSONAIDS'].",'".
	$data['P_CONTROL']."','".
	$data['P_CARNE']."','".
	$data['RIESGO']."',".
	str_replace('-','', $data['FUM']).",".
	str_replace('-','',$data['FPP']).",".
	$data['SEMANAS'].",'".
	$data['A_OBSTRETICO']."',". 
	$data['A_PARTO'].",".
	$data['A_CESAREA'].",".
	$data['A_ABORTO'].",".
	$data['CONTROLES'].",'".
	$data['VIH']."','".
	$data['VIHDETALLE']."','".
	$data['VDRL']."','".
	$data['VDRLDETALLE']."','".
	$data['ODONTOLOGIA']."','".
	$data['MULTIVITAMINICO']."','".
	$data['P_TIPO']."','".
	$data['P_ATENCION']."','".
	$data['POSPARTO']."','".
	$data['M_GEST']."','".
	$data['M_GESTCUAL']."','".
	$data['M_PARTO']."','".
	$data['M_PARTOCUAL']."','".
	$data['M_PUERP']."','".
	$data['M_PUERPCUAL']."',".
	$data['TDTT'].")";
	
	$conexion->query($sql);
	$conexion->execute();
	if($conexion->lastInsertId()){
		http_response_code(201);
		echo json_encode(['Id'=>$conexion->lastInsertId(),'estado'=>true]);
	}else{
		http_response_code(400);
		echo json_encode(['mensaje'=>'error al intentar registar la encuesta','estado'=>false]);
	}
	unset($conexion);
}


	function guardarImagen(){
		require("../../page/conectar.php");

		$fecha=substr($_POST['fecha'],0,8);
		$target_dir = dirname(dirname(__DIR__)).'/guardian/page/imagenesUbicaciones/'.$_POST['usuario'].'/'.$fecha.'/';
		$time=time();
		$target_file = $target_dir . basename($_POST['fileName'].'-'.$time.'.jpg');
	

		$check = getimagesize($_FILES["photo"]["tmp_name"]);
		if(!is_dir($target_dir)){
			mkdir($target_dir,0777,true);
		}
	
			if($check !== false) {
				if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {
					$target_file=str_replace('/home/encuestaplussolu/public_html/guardian','',$target_file);
					$sql="INSERT INTO imagenes(IMAGENURL,FICHAGUARDIANID) VALUES ('{$target_file}', '".$_POST['fichaguardianid']."')";
					$conexion->query($sql);
					$conexion->execute();
					http_response_code(201);
					echo json_encode(['Id'=>$_POST['fileName'],'estado'=>true]);
				} else {
					http_response_code(500);
					echo json_encode(['mensaje'=>'No se pudo comprobar el archivo','estado'=>false]);
				}
			} else {
				http_response_code(500);
				echo json_encode(['mensaje'=>'No se pudo comprobar el archivo','estado'=>false]);
			}

			unset($conexion);


	}

}