
function maxLongitud(input){
	
	if($(input).val().length < $(input).prop('maxlength')){
		$(input).addClass('invalid')
	}else{
		$(input).removeClass('invalid')
	}
	if($(input).val().length > $(input).prop('maxlength')){
		$(input).val($(input).val().slice(0,$(input).prop('maxlength')))
		
	}
}



function validarNum(e) {
	tecla = (document.all) ? e.keyCode : e.which;
	if (tecla == 8 || tecla == 0) return true;
	patron = /(^[0-9]$)/;
	te = String.fromCharCode(tecla);
	return patron.test(te);
}


function validar_tel(){
	var tel = $('#EdtTel').val();
	var pos = tel.substring(0, 1);
	if (pos != 3){
        M.toast({html: 'Debe comenzar por 3!'})
		$('#EdtTel').val('');
		return false;
	}
}

function validar_cel(){
	var tel = $('#edtCel').val();
	var pos = tel.substring(0, 1);
	if (pos != 3){
        M.toast({html: 'Debe comenzar por 3!'})
		$('#edtCel').val('');
		return false;
	}
}
