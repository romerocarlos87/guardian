/**
 * Inicio UbicacionGuardian
 */
function guardarUbicacionSeguimiento(datos) {

		conn.transaction(function(tx) {
			tx.executeSql(
				"INSERT OR IGNORE INTO fichaguardian (SEGUIMIENTO,DBSERVERID,NUMERO,CIUDADID,CABECERA,CORREGIMIENTO,VEREDA,MANZANA,VIVIENDA,FAMILIA,BARRIO,DIRECCION,TELEFONO,EPSID,USUARIOCREACION,MOMENTOCREACION,ESTADOREGISTRO,LONGITUD, LATITUD,UUID,INICIOCAPTURA) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
				[ 1,datos.FICHAGUARDIANID,
					datos.NUMERO,
					datos.CIUDADID,
					datos.CABECERA,
					datos.CORREGIMIENTO,
					datos.VEREDA,
					datos.MANZANA,
					datos.VIVIENDA,
					datos.FAMILIA,
					datos.BARRIO,
					datos.DIRECCION,
					datos.TELEFONO,
					datos.EPSID,
					datos.USUARIOCREACION,
					datos.MOMENTOCREACION,
					datos.ESTADOREGISTRO,
					datos.LONGITUD,
					datos.LATITUD,
					datos.UUID,
					datos.INICIOCAPTURA
				],
				function(tx, results) {
					
				},
				function(tx,error){
						console.log(error);
						
				}
			);
		});
	}

	function addPersonaPrimeraInfanciaSeguimiento(data) {

		conn.transaction(function(tx) {
			tx.executeSql("INSERT OR IGNORE INTO `personaprimerainfancia`(DBSERVERID,`PERSONAID`,`CONTROLCYD`, `LACTANCIA`, `LACTANCIATIEMPO`, `N_PESONACER`, `N_PESOACTUAL`, `N_TALLANACER`, `N_TALLAACTUAL`, `V_CARNE`, `V_BCG`, `V_HEPB`, `V_POLIO`, `V_PENTAVALENTE`,`V_ROTAVIRUS`, `V_NEUMOCOCO`, `V_INFLUENZA`, `ESQUEMA`, `MALTRATO`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`,`UUID`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);", 
			[ data.PERSONAPRIMERAINFANCIAID,
				data.PERSONAID,
				data.CONTROLCYD,
				data.LACTANCIA,
				data.LACTANCIATIEMPO,
				data.N_PESONACER,
				data.N_PESOACTUAL,
				data.N_TALLANACER,
				data.N_TALLAACTUAL,
				data.V_CARNE,
				data.V_BCG,
				data.V_HEPB,
				data.V_POLIO,
				data.V_PENTAVALENTE,
				data.V_ROTAVIRUS,
				data.V_NEUMOCOCO,
				data.V_INFLUENZA,
				data.ESQUEMA,
				data.MALTRATO,
				data.USUARIOCREACION,
				data.MOMENTOCREACION,
				data.ESTADOREGISTRO,
				data.UUID
			], function(tx, results) {
			 
			},errorHandler);
		});
	}
	
	function addPersonaSegundaInfanciaSeguimiento(data) {
	
		conn.transaction(function(tx) {
			tx.executeSql("INSERT OR IGNORE INTO `personasegundainfancia`( DBSERVERID,`PERSONAID`, `CONTROLCYD`, `N_PESOACTUAL`, `N_TALLAACTUAL`, `S_FACTORCARIOGENICOID`, `S_NUMEROCEPILLADA`, `S_CONSULTAODONTOLOGO`, `V_CARNE`,`V_VPH`, `V_VPHDOSIS`, `V_TT`, `V_ESQUEMA`, `MALTRATO`, `NUMERODESPARASITADA`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`,`UUID`) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", 
			[ data.PERSONASEGUNDAINFANCIAID,
				data.PERSONAID,
				data.CONTROLCYD,
				data.N_PESOACTUAL,
				data.N_TALLAACTUAL,
				data.S_FACTORCARIOGENICOID,
				data.S_NUMEROCEPILLADA,
				data.S_CONSULTAODONTOLOGO,
				data.V_CARNE,
				data.V_VPH,
				data.V_VPHDOSIS,
				data.V_TT,
				data.V_ESQUEMA,
				data.MALTRATO,
				data.NUMERODESPARASITADA,
				data.USUARIOCREACION,
				data.MOMENTOCREACION,
				data.ESTADOREGISTRO,
				data.UUID,
			], function(tx, results) {
				
			},errorHandler);
		});
	}
	
	function addPersonaAdolescensJuventudSeguimiento(datos) {
	
		conn.transaction(function(tx) {
			tx.executeSql("INSERT OR IGNORE INTO `personaadolescencia`(DBSERVERID,`PERSONAID`, `P_PLANIFICA`, `P_METODO`, `P_MOTIVO`, `EXAMENMEDICO`, `CITOLOGIA`, `CITOLOGIADETALLE`, `H_TOTALVIVO`,`H_TOTALABORTADO`, `TOS`, `LESIONES`, `V_TD`, `V_TDCUAL`, `V_VPH`, `SENO`, `SENOCUAL`, `VIOLENCIA`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`, `UUID`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", 
			[ datos.PERSONAADOLESCENCIAID,
				datos.PERSONAID,
				datos.P_PLANIFICA,
				datos.P_METODO,
				datos.P_MOTIVO,
				datos.EXAMENMEDICO,
				datos.CITOLOGIA,
				datos.CITOLOGIADETALLE,
				datos.H_TOTALVIVO,
				datos.H_TOTALABORTADO,
				datos.TOS,
				datos.LESIONES,
				datos.V_TD,
				datos.V_TDCUAL,
				datos.V_VPH,
				datos.SENO,
				datos.SENOCUAL,
				datos.VIOLENCIA,
				datos.USUARIOCREACION,
				datos.MOMENTOCREACION,
				datos.ESTADOREGISTRO,
				datos.UUID
			], function(tx, results) {
			 
			},errorHandler);
		});
	}
	
	function addPersonaAdultezSeguimiento(datos) {
	
		conn.transaction(function(tx) {
			tx.executeSql("INSERT OR IGNORE INTO `personaadultez`(DBSERVERID, `PERSONAID`, `N_PESOACTUAL`, `N_TALLAACTUAL`, `N_CINTURA`, `FUMA`, `TOMA`, `EJERCICIO`, `TOS`, `LESIONES`, `SENO`, `SENODETALLE`, `CITOLOGIA`,`CITOLOGIADETALLE`, `PROSTATA`, `PROSTATADETALLE`, `VISION`, `VISIONDETALLE`, `E_HIPERTENSION`, `E_HIPERTENSIONCONTROL`, `E_DIABETES`, `E_DIABETESCONTROL`, `E_EPOC`,`E_EPOCCONTROL`, `E_CANCER`, `E_CANCERDETALLE`, `E_CANCERCONTROL`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`,`UUID`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", 
			[ datos.PERSONAADULTEZID,
				datos.PERSONAID,
				datos.N_PESOACTUAL,
				datos.N_TALLAACTUAL,
				datos.N_CINTURA,
				datos.FUMA,
				datos.TOMA,
				datos.EJERCICIO,
				datos.TOS,
				datos.LESIONES,
				datos.SENO,
				datos.SENODETALLE,
				datos.CITOLOGIA,
				datos.CITOLOGIADETALLE,
				datos.PROSTATA,
				datos.PROSTATADETALLE,
				datos.VISION,
				datos.VISIONDETALLE,
				datos.E_HIPERTENSION,
				datos.E_HIPERTENSIONCONTROL,
				datos.E_DIABETES,
				datos.E_DIABETESCONTROL,
				datos.E_EPOC,
				datos.E_EPOCCONTROL,
				datos.E_CANCER,
				datos.E_CANCERDETALLE,
				datos.E_CANCERCONTROL,
				datos.USUARIOCREACION,
				datos.MOMENTOCREACION,
				datos.ESTADOREGISTRO,
				datos.UUID
			], function(tx, results) {
			 
			},errorHandler);
		});
	}

function addGestacionSeguimiento(datos) {
		conn.transaction(function(tx) {
			tx.executeSql(
				"INSERT OR IGNORE INTO gestacion (DBSERVERID,PERSONAID, P_CONTROL, P_CARNE, RIESGO, FUM, FPP, SEMANAS, A_OBSTRETICO, A_PARTO, A_CESAREA, A_ABORTO, CONTROLES, VIH, VIHDETALLE, VDRL, VDRLDETALLE, ODONTOLOGIA, MULTIVITAMINICO, P_TIPO, P_ATENCION, POSPARTO, M_GEST, M_GESTCUAL, M_PARTO, M_PARTOCUAL, M_PUERP, M_PUERPCUAL,UUID,USUARIOCREACION,P_GESTANTE,TDTT) VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?)",
				[ datos.GESTACIONID,
					datos.PERSONAID,
					datos.P_CONTROL,
					datos.P_CARNE,
					datos.RIESGO,
					datos.FUM,
					datos.FPP,
					datos.SEMANAS,
					datos.A_OBSTRETICO,
					datos.A_PARTO,
					datos.A_CESAREA,
					datos.A_ABORTO,
					datos.CONTROLES,
					datos.VIH,
					datos.VIHDETALLE,
					datos.VDRL,
					datos.VDRLDETALLE,
					datos.ODONTOLOGIA,
					datos.MULTIVITAMINICO,
					datos.P_TIPO,
					datos.P_ATENCION,
					datos.POSPARTO,
					datos.M_GEST,
					datos.M_GESTCUAL,
					datos.M_PARTO,
					datos.M_PARTOCUAL,
					datos.M_PUERP,
					datos.M_PUERPCUAL,
					datos.UUID,
					datos.USUARIOCREACION,
					datos.P_GESTANTE,
					datos.TDTT
				],
				function(tx, results) {
				},
				errorHandler
			);
		});
	}

function guardarPersonaSeguimiento(datos) {
		conn.transaction(function(tx) {
			tx.executeSql(
				"INSERT OR IGNORE INTO `persona`(DBSERVERID,`FICHAGUARDIANID`, `TIPOMIEMBROID`, `TIPODOCUMENTOID`, `NUMERODOCUMENTO`, `NOMBRE`, `GENEROID`, `ESTADOCIVILID`, `FECHANACIMIENTO`, `NIVELDEESTUDIOID`, `OCUPACIONID`, `TIPOSALUDID`, `TIPOAFILIACIONID`, `EPSID`, `EPSOTRO`, `DISCAPACIDAD`, `DISCAPACIDADCUAL`, `ETNIAID`, `GRUPOPOBLACIONALID`, `GRUPOORGANIZADO`, `GESTACION`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`,UUID,F_PERTENECE) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
				[ datos.PERSONAID,
					datos.FICHAGUARDIANID,
					datos.TIPOMIEMBROID,
					datos.TIPODOCUMENTOID,
					datos.NUMERODOCUMENTO,
					datos.NOMBRE,
					datos.GENEROID,
					datos.ESTADOCIVILID,
					datos.FECHANACIMIENTO,
					datos.NIVELDEESTUDIOID,
					datos.OCUPACIONID,
					datos.TIPOSALUDID,
					datos.TIPOAFILIACIONID,
					datos.EPSID,
					datos.EPSOTRO,
					datos.DISCAPACIDAD,
					datos.DISCAPACIDADCUAL,
					datos.ETNIAID,
					datos.GRUPOPOBLACIONALID,
					datos.GRUPOORGANIZADO,
					datos.GESTACION,
					datos.USUARIOCREACION,
					datos.MOMENTOCREACION,
					datos.ESTADOREGISTRO,
					datos.UUID,
					datos.F_PERTENECE
				],
				function(tx, results) {
					},
				function(x,error){
						console.log(error);
						
				}
			);
		});
	}

function addSaludOcupacionalSeguimiento(data) {
		conn.transaction(function(tx) {
			tx.executeSql("INSERT OR IGNORE INTO `saludocupacional`(DBSERVERID,`FICHAGUARDIAN`, `TIPOFAMILIAID`, `TIPOCORRECCIONID`, `CIGARRILLO`, `ALCOHOL`, `PASTILLAS`, `PSA`, `COMPARTEN`, `RELACIONES`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`,`UUID`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
				[ data.SALUDOCUPACIONALID,
					data.FICHAGUARDIAN,
					data.TIPOFAMILIAID,
					data.TIPOCORRECCIONID,
					data.CIGARRILLO,
					data.ALCOHOL,
					data.PASTILLAS,
					data.PSA,
					data.COMPARTEN,
					data.RELACIONES,
					data.USUARIOCREACION,
					data.MOMENTOCREACION,
					data.ESTADOREGISTRO,
					data.UUID
				],
				function(tx, results) {

				},
				errorHandler
			);
		});
	}

function guardarViviendaSeguimiento(data) {
		conn.transaction(function(tx) {
			tx.executeSql("INSERT OR IGNORE INTO `vivienda`(DBSERVERID,`FICHAGUARDIANID`, `TIPOVIVIENDAID`, `TIPOPISOID`, `TIPOPAREDID`, `TIPOTECHOID`, `ILUMINACION`, `VENTILACION`, `INGRESO`, `INGRESOCUAL`, `PERSONADORMITORIO`, `ARROYO`, `BASURERO`, `HUMO`, `FUENTEAGUAID`, `FUENTEAGUAOTRO`, `AGUATRATADA`, `TRATAMIENTO`, `DEPOSITOAGUAID`, `DEPOSITOAGUACUAL`, `DEPOSITOAGUAOTRO`, `LARVAS`, `ROEDORES`, `TIPOALUMBRADOID`, `TIPOALUMBRADOOTRO`, `SERVICIOSANITARIOID`, `MANEJOBASURA`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`, `UUID`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
				[ data.VIVIENDAID,
					data.FICHAGUARDIANID,
					data.TIPOVIVIENDAID,
					data.TIPOPISOID,
					data.TIPOPAREDID,
					data.TIPOTECHOID,
					data.ILUMINACION,
					data.VENTILACION,
					data.INGRESO,
					data.INGRESOCUAL,
					data.PERSONADORMITORIO,
					data.ARROYO,
					data.BASURERO,
					data.HUMO,
					data.FUENTEAGUAID,
					data.FUENTEAGUAOTRO,
					data.AGUATRATADA,
					data.TRATAMIENTO,
					data.DEPOSITOAGUAID,
					data.DEPOSITOAGUACUAL,
					data.DEPOSITOAGUAOTRO,
					data.LARVAS,
					data.ROEDORES,
					data.TIPOALUMBRADOID,
					data.TIPOALUMBRADOOTRO,
					data.SERVICIOSANITARIOID,
					data.MANEJOBASURA,
					data.USUARIOCREACION,
					data.MOMENTOCREACION,
					data.ESTADOREGISTRO,
					data.UUID
				],
				function(tx, results) {
				 
				},
				errorHandler
			);
		});
	}
	function guardarMascotaSeguimiento(data) {
		conn.transaction(function(tx) {
			tx.executeSql("INSERT OR IGNORE INTO `mascota` (DBSERVERID,`FICHAGUARDIANID`, `TIPOMASCOTAID`, `TIPOMASCOTAOTRO`, `VACUANADA`, `USUARIOCREACION`, `MOMENTOCREACION`, `ESTADOREGISTRO`,`UUID`) VALUES (?,?,?,?,?,?,?,?,?)",
				[ data.MASCOTAID,
					data.FICHAGUARDIANID,
					data.TIPOMASCOTAID,
					data.TIPOMASCOTAOTRO,
					data.VACUANADA,
					data.USUARIOCREACION,
					data.MOMENTOCREACION,
					data.ESTADOREGISTRO,
					data.UUID
				],
				function(tx, results) {
					
				},
				errorHandler
			);
		});
	}  

	function getSeguimientoGuardian() {
		conn.transaction(function(tx) {
				tx.executeSql(
					"SELECT F1.*,F2.MUNICIPIONOMBRE FROM fichaguardian f1 inner join municipios f2 on f1.CIUDADID=F2.MUNICIPIOID where seguimiento=1",
					[],
					function(tx, results) {
						$("#seguimientos .collapsible").html("")
						$.each(results.rows,function (i,row) {
							console.log(row.DBSERVERID);
							
								$("#seguimientos .collapsible").append(
								'<li>'+
								'<div class="collapsible-header">'+
									'<i class="material-icons">place</i>'
									+row.DIRECCION+
									'<span class="badge"></span></div>'+
								'<div class="collapsible-body">'+
								'<p><b>Dirección: </b>'+row.DIRECCION+'</p>'+
								'<p><b>Barrio: </b>'+row.BARRIO+'</p>'+
								'<p><b>Municipio: </b>'+row.MUNICIPIONOMBRE+'</p>'+
								'  <button class="waves-effect waves-light btn-small" onclick="visitar('+row.DBSERVERID+')" type="submit" name="action">Visitar'+
								'<i class="material-icons right">send</i>'+
							'</button>'+
								'</div>'+
							'</li>')

						})
					},
					errorHandler
				);
			});
	}
	function listarPersonaSeguimiento() {
		conn.transaction(function(tx) {
			tx.executeSql(				
				"SELECT f1.`PERSONAID`,NOMBRE,TIPOMIEMBRODESCRIPCION,FECHANACIMIENTO,GENEROID,GESTACION,ESCOLARIZADO FROM "+
				"`persona` f1 "+ 
				"INNER JOIN tipomiembro f2 on f1.`TIPOMIEMBROID`=f2.TIPOMIEMBROID WHERE FICHAGUARDIANID=?",
				[localStorage.getItem('FICHAGUARDIANID')],
				function(tx, results) {
					$("#tablaMiembros").html("");

					$.each(results.rows, function(i, persona) {
					 data=getEdadPersona(persona.FECHANACIMIENTO,true)
					 optMenu = '<li class="verMiembro" data-id="' + persona.PERSONAID + '" data-nf="' + persona.FAMILIA + '" ><a href="#!">Ver</a></li>'

					 
					 if (persona.GESTACION=='SI') {
						optMenu += '<li class="tipoPersona" data-tipo="gestacion" data-id="' + persona.PERSONAID + '" ><a href="#!">Gestación</a></li>'
					 }
					
					if (data.unidad=='Años') {
						if (data.edad >= 1 && data.edad < 6) {
							optMenu += '<li class="tipoPersona" data-tipo="primeraInfancia16" data-id="' + persona.PERSONAID + '" ><a href="#!">Primera Infancia</a></li>'
						}
						if (data.edad >= 6 && data.edad < 12) {
							optMenu += '<li class="tipoPersona" data-tipo="segundainfancia" data-id="' + persona.PERSONAID + '" ><a href="#!">Segunda Infancia</a></li>'
						}
						if (data.edad >= 12 && data.edad < 18) {
							optMenu += '<li class="tipoPersona" data-tipo="adolesenciajuventud" data-id="' + persona.PERSONAID + '" ><a href="#!">Adolescencia Juventud</a></li>'

						}
						if (data.edad >= 18 && data.edad < 29) {
							optMenu += '<li class="tipoPersona" data-tipo="adolesenciajuventud" data-id="' + persona.PERSONAID + '" ><a href="#!">Adolescencia Juventud</a></li>'

						}
						if (data.edad >= 29 && data.edad < 60) {
							optMenu += '<li class="tipoPersona" data-tipo="adultezvejez" data-id="' + persona.PERSONAID + '" ><a href="#!">Adultez Vejéz</a></li>'
						}
						if (data.edad >= 60) {
							optMenu += '<li class="tipoPersona" data-tipo="adultezvejez" data-id="' + persona.PERSONAID + '" ><a href="#!">Adultez Vejéz</a></li>'
						}
					}else{
						if(data.meses >=0 && data.meses <= 11){
							optMenu += '<li class="tipoPersona" data-tipo="primeraInfancia01" data-id="' + persona.PERSONAID + '" ><a href="#!">Primera Infancia</a></li>'
						 }
					}
					/*if (data.unidad=='Años') {
						if((data.edad>=1 && data.edad<=5) ||
						 (data.edad>=6 && data.edad<=11)){
							optMenu += '<li class="tipoPersona" data-tipo="segundainfancia" data-id="' + persona.PERSONAID + '" ><a href="#!">Segunda Infancia</a></li>'
						}else if((data.edad>=12 && data.edad<=17) || 
							(data.edad>=18 && data.edad<=28) ){
							optMenu = '<li class="tipoPersona" data-tipo="adolesenciajuventud" data-id="' + persona.PERSONAID + '" ><a href="#!">Adolescencia Juventud</a></li>'
						}else if(data.edad>=29 ){
							optMenu += '<li class="tipoPersona" data-tipo="adultezvejez" data-id="' + persona.PERSONAID + '" ><a href="#!">Adultez Vejéz</a></li>'
						}
					}else{
						if(data.meses >=0 && data.meses <= 11){
							optMenu += '<li class="tipoPersona" data-tipo="primeraInfancia" data-id="' + persona.PERSONAID + '" ><a href="#!">Primera Infancia</a></li>'
						}   
					}*/

						i++;
						$("#tablaMiembros").append(
							"<tr><td>" +
								i +
								"</td><td>" +
								persona.TIPOMIEMBRODESCRIPCION +
								"</td><td>" +
								persona.NOMBRE +
								"</td><td>" +
								data.edad +
								"</td>" +
								"<td>" +
								'<a class="dropdown-trigger btn-floating grey" href="#" data-target="dropdown' +
								i +
								'"><i class="material-icons">build</i></a>' +
								'<ul id="dropdown' +
								i +
								'" class="dropdown-content">' +
								optMenu+
								'<li class="eliminarPersona" data-id="' + persona.PERSONAID + '" ><a href="#!">Eliminar</a></li>' +
								"</ul>" +
								"</td>" +
								"</tr>"
						);
						$(".dropdown-trigger").dropdown();
					});
					$(".tipoPersona").click(function(){
						switch ($(this).data("tipo")) {
							case 'primeraInfancia01':
								$.get("primerainfancia01.html",(html)=>{
								  $("#modal-complemento-persona").html(html);
								 
								});
							  break;
							  case 'primeraInfancia16':
								$.get("primerainfancia16.html",(html)=>{
								  $("#modal-complemento-persona").html(html);
								 
								});
							  break;
							  case 'adultezvejez':
								$.get("adultez-vejez.html",(html)=>{
								  $("#modal-complemento-persona").html(html);
								});
							  break;
							  case 'adolesenciajuventud':
								$.get("adolesencia-juventud.html",(html)=>{
								  $("#modal-complemento-persona").html(html);
								});
							  break;
							  case 'segundainfancia':
								$.get("segundainfancia.html",(html)=>{
								  $("#modal-complemento-persona").html(html);
							  	});
							  break;
							  case 'gestacion':
								$.get("gestacion.html",(html)=>{
								  $("#modal-complemento-persona").html(html);
								});
							  break;
						
						  }
					
				
					})
					$(".eliminarPersona > a").click(function() {
						console.log(this);
						deletePersonaLocal(
							$(this)
								.parent()
								.data("id")
						);
					});
	
					$(".verMiembro").click(function() {
						localStorage.setItem("NF",$(this).data("id"))
						verMiembro($(this).data("id"))
					});
				},
				errorHandler
			);
		});
	}
	function visitar(id){
		localStorage.setItem('FICHAGUARDIANID',id)
		localStorage.setItem('seguimiento',true)
		redirect('miembro',true)
	}

	function eliminarSeguimientos() {
		conn.transaction(function(tx) {
			tx.executeSql(
				"DELETE FROM fichaguardian where seguimiento=1",
				[ 
				],
				function(tx, results) {},
				function(tx,error){console.log(error);}
			);
		});
	}


	function verMiembro(id) {
		conn.transaction(function(tx) {
			tx.executeSql(
				"select f1.*,f2.FAMILIA from persona f1 inner join fichaguardian f2 on f1.FICHAGUARDIANID=f2.DBSERVERID where f1.DBSERVERID=?",
				[ id ],
				function(tx, results) {
					data=results.rows[0]
					localStorage.setItem('NF',data.FAMILIA)
					$("#F_PERTENECE").val(data.F_PERTENECE)
					$("#F_PERTENECE").formSelect();
					$("#NOMBRE").val(data.NOMBRE)
					$("#GENEROID").val(data.GENEROID)
					$("#GENEROID").formSelect();
					$("#FECHANACIMIENTO").val(data.FECHANACIMIENTO)
					$("#TIPODOCUMENTOID").val(data.TIPODOCUMENTOID)
					$('#TIPODOCUMENTOID').select2().trigger('change');
					$("#NUMERODOCUMENTO").val(data.NUMERODOCUMENTO)
					$("#TIPOMIEMBROID").val(data.TIPOMIEMBROID)
					$('#TIPOMIEMBROID').select2().trigger('change');
					
					$("#NIVELDEESTUDIOID").val(data.NIVELDEESTUDIOID)
					$('#NIVELDEESTUDIOID').select2().trigger('change');

					$("#OCUPACIONID").val(data.OCUPACIONID)
					$('#OCUPACIONID').select2().trigger('change');

					$("#TIPOSALUDID").val(data.TIPOSALUDID)
					$("#TIPOSALUDID").formSelect();
					
					$("#TIPOAFILIACIONID").val(data.TIPOAFILIACIONID)
					$("#TIPOAFILIACIONID").formSelect();
					
					$("#EPSID").val(data.EPSID)
					$('#EPSID').select2().trigger('change');

					$("#ESTADOCIVILID").val(data.ESTADOCIVILID)
					$('#ESTADOCIVILID').select2().trigger('change');

					$("#DISCAPACIDAD").val(data.DISCAPACIDAD)
					
					$("#DISCAPACIDAD").formSelect();

					$("#ETNIAID").val(data.ETNIAID)
					$("#ETNIAID").formSelect();
					$("#GRUPOPOBLACIONALID").val(data.GRUPOPOBLACIONALID)
					$("#GRUPOORGANIZADOQ").val(data.GRUPOORGANIZADOQ)
					$("#GRUPOORGANIZADOQ").formSelect();
					$("#TIPOAFILIACIONID").val(data.TIPOAFILIACIONID)
					$("#GESTACION").val(data.GESTACION)
					M.updateTextFields();
					console.log(data);
				},
				function(tx,error){console.log(error);}
			);
		});
	}

	function verPersonaAdultez(id) {
		conn.transaction(function(tx) {
			tx.executeSql(
				"SELECT * FROM personaadultez where PERSONAID=?",
				[ id ],
				function(tx, results) {
					if (results.rows.length) {
						$.each(Object.keys(data=results.rows[0]),function (i,key) {
							$("#"+key).val(results.rows[0][key])
						})
						M.updateTextFields();      
						$(".modal-content .select").formSelect()  
					}
				},
				function(tx,error){console.log(error);}
			);
		});
	}
	function verGestacion(id) {
		console.log(id);
		
		conn.transaction(function(tx) {
			tx.executeSql(
				"SELECT * FROM gestacion where PERSONAID=?",
				[ id ],
				function(tx, results) {
					if (results.rows.length) {
						$.each(Object.keys(data=results.rows[0]),function (i,key) {
							$("#"+key).val(results.rows[0][key])
						})
						M.updateTextFields();      
						$(".modal-content .select").formSelect()  
					}
				},
				function(tx,error){console.log(error);}
			);
		});
	}

	function verAdolescenciaJuventud(id) {
		console.log(id);
		
		conn.transaction(function(tx) {
			tx.executeSql(
				"SELECT * FROM personaadolescencia where PERSONAID=?",
				[ id ],
				function(tx, results) {
					if (results.rows.length) {
						$.each(Object.keys(data=results.rows[0]),function (i,key) {
							$("#"+key).val(results.rows[0][key])
						})
						M.updateTextFields();      
						$(".modal-content .select").formSelect()  
					}
				},
				function(tx,error){console.log(error);}
			);
		});
	}