    function getStatusSinc() {
        conn.transaction(function(tx) {
        tx.executeSql(
            "SELECT 0 as n, count(DBSERVERID) sinc,count(FICHAGUARDIANID) total ,(count(FICHAGUARDIANID)-count(DBSERVERID)) nosinc FROM fichaguardian union "+
            "SELECT 1 as n, count(v.DBSERVERID) sinc,count(v.VIVIENDAID)total ,(count(v.VIVIENDAID)-count(v.DBSERVERID))nosinc FROM vivienda v INNER JOIN fichaguardian f on v.FICHAGUARDIANID=f.FICHAGUARDIANID union "+
            "SELECT 2 as n, count(m.DBSERVERID) sinc,count(m.MASCOTAID)total ,(count(m.MASCOTAID)-count(m.DBSERVERID))nosinc FROM mascota m INNER JOIN fichaguardian f on m.FICHAGUARDIANID=f.FICHAGUARDIANID  union "+
            "SELECT 3 as n, count(f1.DBSERVERID) sinc,count(f1.PERSONAID)total ,(count(f1.PERSONAID)-count(f1.DBSERVERID))nosinc FROM persona f1 INNER JOIN fichaguardian f2 on f1.FICHAGUARDIANID=f2.FICHAGUARDIANID  union "+
            "SELECT 4 as n, count(f1.DBSERVERID) sinc,count(f1.PERSONAID)total ,(count(f1.PERSONAID)-count(f1.DBSERVERID))nosinc FROM personaprimerainfancia f1 INNER JOIN  persona f2 on f1.PERSONAID=f2.PERSONAID union "+
            "SELECT 5 as n, count(f1.DBSERVERID) sinc,count(f1.PERSONAID)total ,(count(f1.PERSONAID)-count(f1.DBSERVERID))nosinc FROM personasegundainfancia f1 INNER JOIN  persona f2 on f1.PERSONAID=f2.PERSONAID union "+
            "SELECT 6 as n, count(f1.DBSERVERID) sinc,count(f1.PERSONAID)total ,(count(f1.PERSONAID)-count(f1.DBSERVERID))nosinc FROM personaadolescencia f1 INNER JOIN  persona f2 on f1.PERSONAID=f2.PERSONAID union " +
            "SELECT 7 as n, count(f1.DBSERVERID) sinc,count(f1.PERSONAID)total ,(count(f1.PERSONAID)-count(f1.DBSERVERID))nosinc FROM personaadultez f1 INNER JOIN  persona f2 on f1.PERSONAID=f2.PERSONAID union "+
            "SELECT 8 as n, count(f1.DBSERVERID) sinc,count(f1.saludocupacionalid)total ,(count(f1.saludocupacionalid)-count(f1.DBSERVERID))nosinc FROM saludocupacional f1 INNER JOIN fichaguardian f2 on f1.FICHAGUARDIAN=f2.FICHAGUARDIANID union " +
            "SELECT 9 as n, count(f1.DBSERVERID) sinc,count(f1.IMAGENID)total ,(count(f1.IMAGENID)-count(f1.DBSERVERID))nosinc FROM imagenes f1 INNER JOIN fichaguardian f2 on f1.UBICACIONUUID=f2.UUID union " +
            "SELECT 10 as n, count(f1.DBSERVERID) sinc,count(f1.GESTACIONID)total ,(count(f1.GESTACIONID)-count(f1.DBSERVERID))nosinc FROM GESTACION f1 union " +
            "SELECT 11 as n, count(f1.DBSERVERID) sinc,count(f1.ESCOLARID)total ,(count(f1.ESCOLARID)-count(f1.DBSERVERID))nosinc FROM ESCOLAR f1 INNER JOIN  persona f2 on f1.PERSONAID=f2.PERSONAID  " +

            " order by(1)", [], function(tx, results) {
            //Estado sincronizacion ubicaciones
            $("#ubicacionTotal").html(results.rows.item(0).total);
            $("#ubicacionSincronizados").html(results.rows.item(0).sinc);
            $("#ubicacionnoSincronizados").html(results.rows.item(0).nosinc);
            $("#p-ubicaciones").css({width: (((results.rows.item(0).sinc)/(results.rows.item(0).total))*100)+"%"})
            //Estado sincronizacion viviendas
            $("#viviendaTotal").html(results.rows.item(1).total);
            $("#viviendaSincronizados").html(results.rows.item(1).sinc);
            $("#viviendanoSincronizados").html(results.rows.item(1).nosinc);
            $("#p-viviendas").css({width: (((results.rows.item(1).sinc)/(results.rows.item(1).total))*100)+"%"})
            //Estado sincronizacion viviendas
            $("#mascotaTotal").html(results.rows.item(2).total);
            $("#mascotaSincronizados").html(results.rows.item(2).sinc);
            $("#mascotanoSincronizados").html(results.rows.item(2).nosinc);
            $("#p-mascotas").css({width: (((results.rows.item(2).sinc)/(results.rows.item(2).total))*100)+"%"})

            //Estado sincronizacion viviendas
            $("#personaTotal").html(results.rows.item(3).total);
            $("#personaSincronizados").html(results.rows.item(3).sinc);
            $("#personanoSincronizados").html(results.rows.item(3).nosinc);
            $("#p-personas").css({width: (((results.rows.item(3).sinc)/(results.rows.item(3).total))*100)+"%"})
            //Estado sincronizacion primerainfancia
            $("#personaPrimeraInfanciaTotal").html(results.rows.item(4).total);
            $("#personaPrimeraInfanciaSincronizados").html(results.rows.item(4).sinc);
            $("#personaPrimeraInfancianoSincronizados").html(results.rows.item(4).nosinc);
            $("#p-personaPrimeraInfancia").css({width: (((results.rows.item(4).sinc)/(results.rows.item(4).total))*100)+"%"})

            //Estado sincronizacion segundainfancia
            $("#personaSegundaInfanciaTotal").html(results.rows.item(5).total);
            $("#personaSegundaInfanciaSincronizados").html(results.rows.item(5).sinc);
            $("#personaSegundaInfancianoSincronizados").html(results.rows.item(5).nosinc);
            $("#p-personaSegundaInfancia").css({width: (((results.rows.item(5).sinc)/(results.rows.item(5).total))*100)+"%"})

            //Estado sincronizacion adolescencia
            $("#personaAdolescenciaTotal").html(results.rows.item(6).total);
            $("#personaAdolescenciaSincronizados").html(results.rows.item(6).sinc);
            $("#personaAdolescencianoSincronizados").html(results.rows.item(6).nosinc);
            $("#p-personaAdolescencia").css({width: (((results.rows.item(6).sinc)/(results.rows.item(6).total))*100)+"%"})

            //Estado sincronizacion adultez
            $("#personaAdultezTotal").html(results.rows.item(7).total);
            $("#personaAdultezSincronizados").html(results.rows.item(7).sinc);
            $("#personaAdulteznoSincronizados").html(results.rows.item(7).nosinc);
            $("#p-personaAdultez").css({width: (((results.rows.item(7).sinc)/(results.rows.item(7).total))*100)+"%"})

            //Estado sincronizacion Salud ocupacional
            $("#saludOcupacionalTotal").html(results.rows.item(8).total);
            $("#saludOcupacionalSincronizados").html(results.rows.item(8).sinc);
            $("#saludOcupacionalnoSincronizados").html(results.rows.item(8).nosinc);
            $("#p-saludOcupacional").css({width: (((results.rows.item(8).sinc)/(results.rows.item(8).total))*100)+"%"})
        
            //Estado sincronizacion Imagenes
            $("#fotoTotal").html(results.rows.item(9).total);
            $("#fotoSincronizados").html(results.rows.item(9).sinc);
            $("#fotonoSincronizados").html(results.rows.item(9).nosinc);
            $("#p-fotos").css({width: (((results.rows.item(9).sinc)/(results.rows.item(9).total))*100)+"%"})
            
            //Estado sincronizacion gestacion
            $("#gestacionTotal").html(results.rows.item(10).total);
            $("#gestacionSincronizados").html(results.rows.item(10).sinc);
            $("#gestacionnoSincronizados").html(results.rows.item(10).nosinc);
            $("#p-gestacion").css({width: (((results.rows.item(10).sinc)/(results.rows.item(10).total))*100)+"%"})

            //Estado sincronizacion gestacion
            $("#escolarTotal").html(results.rows.item(11).total);
            $("#escolarSincronizados").html(results.rows.item(11).sinc);
            $("#escolarnoSincronizados").html(results.rows.item(11).nosinc);
            $("#p-escolar").css({width: (((results.rows.item(11).sinc)/(results.rows.item(11).total))*100)+"%"})

            
            
        },errorHandler);
        });
    }
  
    function sincronizarFichaGuardian() {
    conn.transaction(function(tx) {
      tx.executeSql("SELECT * FROM fichaguardian where USUARIOCREACION IS NOT NULL AND DBSERVERID IS NULL", [], function(tx, results) {

        localStorage.setItem("totalFichaGuardian",results.rows.length);
        localStorage.setItem("enviadoFichaGuardian",0);
        progress("p-ubicaciones",localStorage.getItem("totalFichaGuardian"),localStorage.getItem("enviadoFichaGuardian"));
        exportDB('fichaguardian',results.rows)

        $.each(results.rows, function(i, element) {
            guardarUbicacionGuardianServer(element, element.FICHAGUARDIANID, true);  
        });

    });
    },errorHandler);
    }

    function sincronizarGestacion() {
        conn.transaction(function(tx) {
          tx.executeSql("SELECT *,f2.DBSERVERID PERSONAIDS FROM gestacion F1 INNER JOIN persona f2 on f1.PERSONAID=f2.PERSONAID where f1.USUARIOCREACION IS NOT NULL AND f1.DBSERVERID IS NULL", [], function(tx, results) {
    
            localStorage.setItem("totalGestacion",results.rows.length);
            localStorage.setItem("enviadoGestacion",0);
            progress("p-gestacion",localStorage.getItem("totalGestacion"),localStorage.getItem("enviadoGestacion"));
            exportDB('gestacion',results.rows)
            $.each(results.rows, function(i, element) {
             
                guardarGestacionServer(element, element.GESTACIONID, true);
                
                
            });
    
        });
        },errorHandler);
        }

    function sincronizarImagenes() {
        conn.transaction(function(tx) {
          tx.executeSql("SELECT f1.*,f2.DBSERVERID FICHAGUARDIANID FROM imagenes f1 inner join fichaguardian f2 on f1.UBICACIONUUID=f2.UUID where  f1.DBSERVERID IS NULL", [], function(tx, results) {
    
            localStorage.setItem("totalFoto",results.rows.length);
            localStorage.setItem("enviadoFoto",0);
            progress("p-fotos",localStorage.getItem("totalFoto"),localStorage.getItem("enviadoFoto"));
            exportDB('imagenes',results.rows)
            $.each(results.rows, function(i, element) {
                setTimeout(function () {
                    guardarImagenServer(element, element.IMAGENID, true);
                },300)
                
            });
    
        });
        },errorHandler);
        }

    function sincronizarVivienda() {
    conn.transaction(function(tx) {
      tx.executeSql("SELECT v.*,f.DBSERVERID FICHAGUARDIANIDS FROM vivienda v INNER JOIN fichaguardian f on v.FICHAGUARDIANID=f.FICHAGUARDIANID WHERE f.DBSERVERID IS NOT NULL AND V.USUARIOCREACION IS NOT NULL and v.DBSERVERID IS NULL ", [], function(tx, results) {

        localStorage.setItem("totalVivienda",results.rows.length);
        localStorage.setItem("enviadoVivienda",0);
        progress("p-viviendas",localStorage.getItem("totalVivienda"),localStorage.getItem("enviadoVivienda"));
        exportDB('vivienda',results.rows)
        $.each(results.rows, function(i, element) {
            setTimeout(function () {
                guardarViviendaServer(element, element.VIVIENDAID, true);
            },300)
            
        });

    });
    },errorHandler);
    }

    function sincronizarMascotas() {
        conn.transaction(function(tx) {
        tx.executeSql("SELECT m.*,f.DBSERVERID FICHAGUARDIANIDS FROM mascota m INNER JOIN fichaguardian f on m.FICHAGUARDIANID=f.FICHAGUARDIANID WHERE f.DBSERVERID IS NOT NULL AND m.USUARIOCREACION IS NOT NULL and m.DBSERVERID IS NULL ", [], function(tx, results) {

            localStorage.setItem("totalMascota",results.rows.length);
            localStorage.setItem("enviadoMascota",0);
            progress("p-mascotas",localStorage.getItem("totalMascota"),localStorage.getItem("enviadoMascota"));
            exportDB('mascota',results.rows)
            $.each(results.rows, function(i, element) {
                setTimeout(function () {
                    guardarMascotaServer(element, element.MASCOTAID, true);
                },300)
                
            });

        });
        },errorHandler);
    }

    function sincronizarPersonas() {
        conn.transaction(function(tx) {
        tx.executeSql("SELECT p.*,f.DBSERVERID FICHAGUARDIANIDS FROM persona p INNER JOIN fichaguardian f on p.FICHAGUARDIANID=f.FICHAGUARDIANID WHERE f.DBSERVERID IS NOT NULL AND p.USUARIOCREACION IS NOT NULL AND p.DBSERVERID IS NULL", [], function(tx, results) {

            localStorage.setItem("totalPersona",results.rows.length);
            localStorage.setItem("enviadoPersona",0);
            progress("p-personas",localStorage.getItem("totalPersona"),localStorage.getItem("enviadoPersona"));
            exportDB('persona',results.rows)
            $.each(results.rows, function(i, element) {
                guardarPersonaServer(element, element.PERSONAID, true);
                
            });

        });
        },errorHandler);
    }

    function sincronizarPersonaPrimeraInfancia() {
        conn.transaction(function(tx) {
        tx.executeSql("SELECT ppi.*,p.DBSERVERID PERSONAIDS FROM persona p INNER JOIN personaprimerainfancia ppi on ppi.PERSONAID=p.PERSONAID WHERE p.DBSERVERID IS NOT NULL AND ppi.USUARIOCREACION IS NOT NULL and ppi.DBSERVERID IS NULL ", [], function(tx, results) {

            localStorage.setItem("totalPersonaPrimeraInfancia",results.rows.length);
            localStorage.setItem("enviadoPersonaPrimeraInfancia",0);
            progress("p-personaPrimeraInfancia",localStorage.getItem("totalPersonaPrimeraInfancia"),localStorage.getItem("enviadoPersonaPrimeraInfancia"));
            exportDB('personaprimerainfancia',results.rows)

            $.each(results.rows, function(i, element) {
                guardarPersonaPrimeraInfanciaServer(element, element.PERSONAPRIMERAINFANCIAID, true);
                
            });

        });
        },errorHandler);
    }

    function sincronizarPersonaSegundaInfancia() {
        conn.transaction(function(tx) {
        tx.executeSql("SELECT ppi.*,p.DBSERVERID PERSONAIDS FROM persona p INNER JOIN personasegundainfancia ppi on ppi.PERSONAID=p.PERSONAID WHERE p.DBSERVERID IS NOT NULL AND ppi.USUARIOCREACION IS NOT NULL and ppi.DBSERVERID IS NULL ", [], function(tx, results) {

            localStorage.setItem("totalPersonaSegundaInfancia",results.rows.length);
            localStorage.setItem("enviadoPersonaSegundaInfancia",0);
            progress("p-personaSegundaInfancia",localStorage.getItem("totalPersonaSegundaInfancia"),localStorage.getItem("enviadoPersonaSegundaInfancia"));
            exportDB('personasegundainfancia',results.rows)

            $.each(results.rows, function(i, element) {
                guardarPersonaSegundaInfanciaServer(element, element.PERSONASEGUNDAINFANCIAID, true);
            });

        });
        },errorHandler);
    }

    function sincronizarPersonaAdolescencia() {
        conn.transaction(function(tx) {
        tx.executeSql("SELECT ppi.*,p.DBSERVERID PERSONAIDS FROM persona p INNER JOIN personaadolescencia ppi on ppi.PERSONAID=p.PERSONAID WHERE p.DBSERVERID IS NOT NULL AND ppi.USUARIOCREACION IS NOT NULL and ppi.DBSERVERID IS NULL ", [], function(tx, results) {

            localStorage.setItem("totalPersonaAdolescencia",results.rows.length);
            localStorage.setItem("enviadoPersonaAdolescencia",0);
            progress("p-personaAdolescencia",localStorage.getItem("totalPersonaAdolescencia"),localStorage.getItem("enviadoPersonaAdolescencia"));
            exportDB('personaadolescencia',results.rows)

            $.each(results.rows, function(i, element) {
                guardarPersonaAdolescenciaServer(element, element.PERSONAADOLESCENCIAID, true);
            });

        });
        },errorHandler);
    }

    function sincronizarSaludOcupacional() {
        conn.transaction(function(tx) {
        tx.executeSql("SELECT ppi.*,p.DBSERVERID FICHAGUARDIANIDS FROM fichaguardian p INNER JOIN saludocupacional ppi on ppi.FICHAGUARDIAN=p.FICHAGUARDIANID WHERE p.DBSERVERID IS NOT NULL AND ppi.USUARIOCREACION IS NOT NULL and ppi.DBSERVERID IS NULL ", [], function(tx, results) {

            localStorage.setItem("totalSaludOcupacional",results.rows.length);
            localStorage.setItem("enviadoSaludOcupacional",0);
            progress("p-saludOcupacional",localStorage.getItem("totalSaludOcupacional"),localStorage.getItem("enviadoSaludOcupacional"));
            exportDB('saludocupacional',results.rows)

            $.each(results.rows, function(i, element) {
                guardarSaludOcupacionalServer(element, element.SALUDOCUPACIONALID, true);
            });

        });
        },errorHandler);
    }


    function sincronizarPersonaAdultez() {
        conn.transaction(function(tx) {
        tx.executeSql("SELECT ppi.*,p.DBSERVERID PERSONAIDS FROM persona p INNER JOIN personaadultez ppi on ppi.PERSONAID=p.PERSONAID WHERE p.DBSERVERID IS NOT NULL AND ppi.USUARIOCREACION IS NOT NULL and ppi.DBSERVERID IS NULL ", [], function(tx, results) {

            localStorage.setItem("totalPersonaAdultez",results.rows.length);
            localStorage.setItem("enviadoPersonaAdultez",0);
            progress("p-personaAdultez",localStorage.getItem("totalPersonaAdultez"),localStorage.getItem("enviadoPersonaAdultez"));
            exportDB('personaadultez',results.rows)

            $.each(results.rows, function(i, element) {
                guardarPersonaAdultezServer(element, element.PERSONAADULTEZID, true);
            });

        });
        },errorHandler);
    }


    function sincronizarUbicaciones() {
        conn.transaction(function(tx) {
        tx.executeSql("SELECT * FROM ubicaciones where usuario is not null ", [], function(tx, results) {
            exportDB('ubicaciones',results.rows)
            
            $.each(results.rows, function(i, element) {
                enviarUbicacionServer(element, element.ID,true);
            });
        });
        });
    }


    function sincronizarEscolar() {
        conn.transaction(function(tx) {
        tx.executeSql("SELECT ppi.*,p.DBSERVERID PERSONAIDS FROM persona p INNER JOIN ESCOLAR ppi on ppi.PERSONAID=p.PERSONAID WHERE p.DBSERVERID IS NOT NULL AND ppi.USUARIOCREACION IS NOT NULL and ppi.DBSERVERID IS NULL ", [], function(tx, results) {

            localStorage.setItem("totalEscolar",results.rows.length);
            localStorage.setItem("enviadoEscolar",0);
            progress("p-personaAdultez",localStorage.getItem("totalEscolar"),localStorage.getItem("enviadoEscolar"));
            exportDB('ESCOLAR',results.rows)

            $.each(results.rows, function(i, element) {
                guardarEscolarServer(element, element.ESCOLARID, true);
            });

        });
        },errorHandler);
    }